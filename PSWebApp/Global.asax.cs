﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using PSWebApp.App_Start;
using PSWebApp.Controllers;

namespace PSWebApp
{
    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new System.Web.Mvc.AuthorizeAttribute());
        }
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            // routes.MapRoute(
            //     name: "Default",
            //     url:"{controller}.mvc/{action}/{id}",
            //     defaults: new { action = "Index", id = "" }
            // );
            // routes.MapRoute(
            //  name: "Root",
            //  url: "",
            //  defaults: new { controller = "Home", action = "Index", id = "" }
            //);
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);
            BootStrapBundleConfig.RegisterBundles(BundleTable.Bundles);
            ControllerBuilder.Current.SetControllerFactory(typeof(CustomControllerFactory));
        }

        protected void Application_BeginRequest()
        {
        }

        protected void Application_EndRequest()
        {
            if (Context.Response.StatusCode == 404)
            {
                Response.Clear();

                var rd = new RouteData();
                rd.Values["controller"] = "Home";
                rd.Values["action"] = "NotFound";

                IController c = new HomeController();
                c.Execute(new RequestContext(new HttpContextWrapper(Context), rd));
            }
        }

        protected void Session_OnEnd(object sender, EventArgs e)
        {
            var _sessionRepository = new Repository.SessionRepository();
            //_sessionRepository.RemoveUserSession(Session.SessionID);
            System.Diagnostics.Debug.WriteLine("Session_OnEnd fired!! - " + Session.SessionID);
        }
        protected void Session_Start(object sender, EventArgs e)
        {
            string sessionId = Session.SessionID;
        }
    }
}
