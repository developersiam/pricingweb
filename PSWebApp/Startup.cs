﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PSWebApp.Startup))]
namespace PSWebApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
