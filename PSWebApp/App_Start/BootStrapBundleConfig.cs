﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace PSWebApp.App_Start
{
    public class BootStrapBundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region scripts
            bundles.Add(new ScriptBundle("~/bundles/scripts/process").Include(
                "~/Scripts/Views/process/process.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/scripts/layout").Include(
                "~/Scripts/spin.min.js",
                "~/Scripts/bootbox.min.js",
                "~/Scripts/jquery/jquery-2.0.0.min.js",
                "~/Scripts/jquery-1.10.0.js",
                "~/Scripts/jquery/jquery.validate*",
                "~/Scripts/jquery/jquery.unobtrusive-ajax.min.js",
                "~/Scripts/jquery.mousewheel.min.js",
                "~/Scripts/jquery.mCustomScrollbar.js",
                "~/Scripts/umd/popper.js",
                "~/Scripts/bootstrap/js/bootstrap.js",
                "~/Scripts/jquery.multifile.js",
                "~/Scripts/views/shared/layout.js",
                "~/Scripts/views/shared/slide-menu.js"
                ));
            bundles.Add(new ScriptBundle("~/bundles/scripts/home").Include(
                "~/Scripts/Views/home/home.js",
                "~/Scripts/Views/home/scroll-index.js"
                ));
            bundles.Add(new ScriptBundle("~/bundles/scripts/logon").Include(
                "~/Scripts/Views/account/logon.js"
                ));
            bundles.Add(new ScriptBundle("~/bundles/scripts/report").Include(
                "~/Scripts/Views/report/report.js"
                ));
            bundles.Add(new ScriptBundle("~/bundles/scripts/bootstrap-datepicker").Include(
               "~/Scripts/datepicker/js/bootstrap-datepicker.js"
               ));
            bundles.Add(new ScriptBundle("~/bundles/scripts/receivingA").Include(
                "~/Scripts/Views/greenleaf/receivingA.js"
                ));
            bundles.Add(new ScriptBundle("~/bundles/scripts/receivingM").Include(
                "~/Scripts/Views/greenleaf/receivingM.js"
                ));
            bundles.Add(new ScriptBundle("~/bundles/scripts/receivingD").Include(
                "~/Scripts/Views/greenleaf/receiving.js"
                ));
            bundles.Add(new ScriptBundle("~/bundles/scripts/greenTranM").Include(
                "~/Scripts/Views/greenleaf/greenTranM.js"));
            bundles.Add(new ScriptBundle("~/bundles/scripts/greenTranD").Include(
                "~/Scripts/Views/greenleaf/greenTran.js"
                ));
            bundles.Add(new ScriptBundle("~/bundles/scripts/handStripM").Include(
                "~/Scripts/Views/greenleaf/handStripM.js"));
            bundles.Add(new ScriptBundle("~/bundles/scripts/handStripD").Include(
                "~/Scripts/Views/greenleaf/handStrip.js"
                ));
            bundles.Add(new ScriptBundle("~/bundles/scripts/regradeM").Include(
                "~/Scripts/Views/greenleaf/regradeM.js"));
            bundles.Add(new ScriptBundle("~/bundles/scripts/regradeD").Include(
                "~/Scripts/Views/greenleaf/regrade.js"
                ));
            bundles.Add(new ScriptBundle("~/bundles/scripts/redry").Include(
                "~/Scripts/Views/setup/redryCharge.js"
                ));
            bundles.Add(new ScriptBundle("~/bundles/scripts/redryCode").Include(
                "~/Scripts/Views/setup/redryCodeM.js"));
            bundles.Add(new ScriptBundle("~/bundles/scripts/redryCodeD").Include(
                "~/Scripts/Views/setup/redryCode.js"));
            bundles.Add(new ScriptBundle("~/bundles/scripts/packing").Include(
                "~/Scripts/Views/setup/packingMat.js"
                ));
            bundles.Add(new ScriptBundle("~/bundles/scripts/exchange").Include(
                "~/Scripts/Views/setup/exchangeRate.js"
                ));
            bundles.Add(new ScriptBundle("~/bundles/scripts/processReady").Include(
                "~/Scripts/Views/process/processReady.js"
                ));
            bundles.Add(new ScriptBundle("~/bundles/scripts/processReadyD").Include(
                "~/Scripts/Views/process/processReadyD.js"
                ));
            bundles.Add(new ScriptBundle("~/bundles/scripts/processProduct").Include(
                "~/Scripts/Views/process/processProduct.js"
                ));
            bundles.Add(new ScriptBundle("~/bundles/scripts/processProductD").Include(
                "~/Scripts/Views/process/processProductD.js"
                ));
            bundles.Add(new ScriptBundle("~/bundles/scripts/shippingP").Include(
                 "~/Scripts/Views/shipping/shippingProduct.js"
                ));
            bundles.Add(new ScriptBundle("~/bundles/scripts/shippingD").Include(
                 "~/Scripts/Views/shipping/shippingProductD.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/scripts/export").Include(
                 "~/Scripts/Views/manage/exportM.js"
                ));
            bundles.Add(new ScriptBundle("~/bundles/scripts/exportD").Include(
                 "~/Scripts/Views/manage/export.js"
                ));

            #endregion

            #region css
            bundles.Add(new StyleBundle("~/Content/css/bootstrap").Include(
                "~/Scripts/bootstrap/css/bootstrap.css",
                "~/Scripts/bootstrap/css/sticky-footer-navbar.css"
                ));

            bundles.Add(new StyleBundle("~/Content/bootstrap-table").Include(
                "~/Content/bootstrap-table.css",
                "~/Content/bootstrap-table-partial.css"
                ));

            bundles.Add(new StyleBundle("~/Content/layout").Include(
                "~/Content/slide-menu.css",
                "~/Content/css/bootstrap-colorselector.css",
                "~/Content/css/stec-theme.css"
                ));

            bundles.Add(new StyleBundle("~/Content/css/prototype-layout").Include(
                "~/Content/css/bootstrap.css",
                "~/Content/css/bootstrap-colorselector.css",
                "~/Content/css/font-awesome.css",
                "~/Content/css/style.css"
                ));

            bundles.Add(new StyleBundle("~/Content/views/home").Include(
                "~/Content/views/home.css"
                ));

            bundles.Add(new StyleBundle("~/Content/views/logon").Include(
                "~/Content/views/logon.css"
                ));

            bundles.Add(new StyleBundle("~/Content/css/validation").Include(
                "~/Content/css/validation.css"
            ));

            bundles.Add(new StyleBundle("~/Content/views/receiving").Include(
                "~/Content/views/receiving.css"
                ));

            bundles.Add(new StyleBundle("~/Content/views/setup").Include(
                "~/Content/views/setup.css"
                ));

            #endregion
        }
    }
}