﻿var table;
var ByProduct = "Product";
$(document).ready(function () {
    $('input[name="byProductOptions"]:radio').click(function () {
        //alert("Clicked");
        if ($(this).attr('checkstate') == 'true') {
            $(this).attr('checked', false);
            $(this).attr('checkstate', false);
            showRedry();
        }
        else {
            $(this).attr('checked', true);
            $(this).attr('checkstate', true);
            HideRedry();
        }
    });
    $('input[name="byProductOptions"]').removeAttr('checked');
});

$('#TCodes').change(function () {
    $('#TCodes-Charge option:first').attr('selected', 'selected');
    $('#TCodes-Charge').find('option[trans-Code]').hide()
    $('#TCodes-Charge').find('option[trans-Code="' + $(this).val() + '"]').show();

});

$('#RCodes').change(function () {
    $('#RCodes-Charge').attr('selected', 'selected');
    $('#RCodes-Charge').find('option[redry-Code]').hide()
    $('#RCodes-Charge').find('option[redry-Code="' + $(this).val() + '"]').show();
});

function adjust_receiving_details_table() {
    var height = $('.headerBar div').outerHeight(true) + $('.fixed-table-header').outerHeight(true) + $('.clearfix').height() + $('.form-group fieldset-row').height(true);
    height = $(window).height() - height - 130;
    $('.fixed-table-body').height(height);
}

function adjust_table_by_size() {
    var screen_width = $(window).width();
    if (screen_width < 768) {
        //extra small device
        adjust_table_column("xs");
    }
    else if (screen_width < 992) {
        //small device
        adjust_table_column("sm");
    }
    else if (screen_width < 1200) {
        //medium device
        adjust_table_column("md");
    }
}

function adjust_table_column(size) {
    var hide_column = [];
    if (size == "md") {
        hide_column = [];
    }
    else if (size == "sm") {
        hide_column = [];
    }
    else if (size == "xs") {
        hide_column = ["weight", "unit price"];
    }
    for (var i = 0; i < hide_column.length; i++) {
        table.bootstrapTable('hideColumn', hide_column[i]);
    }
}

function ShowRedry() {
    document.getElementById('redryP').removeClass('hidden');
    document.getElementById('redryC').removeClass('hidden');
}

function HideRedry() {
    document.getElementById('redryP').addClass('hidden');
    document.getElementById('redryC').addClass('hidden');
}

//$('#btn-delete-redryCode').click(function () {
//    var TFDocNo = $('#DocumentNo').val();
//    var frm_delete = $('<form action="/RedryCode/DeleteRedryCode" method="post"/>');
//    $('body').append(frm_delete);
//    frm_delete.append($('<input name="TFDocNo" type="hidden" value="' + TFDocNo + '" />'));
//    frm_delete.submit();
//});

$('#frm-redryCode-details').submit(function (e) {
    $(this).validate().form();

    var is_valid = true;
    var error_count = $('[data-valmsg-for]:has(>span)').length;

    if (error_count > 0)
        is_valid = false;

    if (is_valid) {
        bootbox.dialog({
            message: '<h4 class="text-center"><i class="fa fa-refresh fa-spin fa-fw"></i></i>&nbsp;Submitting Information</h4>',
            closeButton: false
        });
    }
});