﻿$(function () {
    $('a[href*=#]:not([href=#])').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {

            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });

    $("#toastBox").fadeIn();

    setTimeout(function () { $("#toastBox").fadeOut(); }, 3000);

    $(window).scroll(function () {
        if ($(window).scrollTop() > 100) {
            $("#scrolldownarrow").fadeOut("fast");
        }
    });

    $(window).scroll(function () {
        if ($(window).scrollTop() < 100) {
            $("#scrolldownarrow").fadeIn("fast");
        }
    });
});

function showBasicDiv() {
    document.getElementById('internetDiv').style.display = "none";
    document.getElementById('basicDiv').style.display = "block";
    document.getElementById('basicLink').className = "selected";
    document.getElementById('internetLink').className = "";
};

function showInternetDiv() {
    document.getElementById('internetDiv').style.display = "block";
    document.getElementById('basicDiv').style.display = "none";
    document.getElementById('internetLink').className = "selected";
    document.getElementById('basicLink').className = "";
};