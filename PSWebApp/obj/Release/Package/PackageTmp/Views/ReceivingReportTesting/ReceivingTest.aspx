﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReceivingTest.aspx.cs" Inherits="PSWebApp.Views.ReceivingReportTesting.ReceivingTest" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.3500.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Button ID="Show" runat="server" Text="Show" />
        <CR:CrystalReportPartsViewer ID="CrystalReportPartsViewer1" runat="server" AutoDataBind="true" />
    </div>
    </form>
</body>
</html>
