﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using PSWebApp.Models;
using PSWebApp.Repository;
using System.Web.Script.Serialization;
using System.Web.Configuration;

namespace PSWebApp.Controllers
{
    [Serializable]
    public class RouteInfo
    {
        public string Controller { get; set; }
        public string Action { get; set; }
    }
    public class CommonControllerBase : Controller
    {
        private DateTime _startRequestTime;
        public UserInfo CurrentUser
        {
            set { Session["CurrentUser"] = value; }
            get
            {
                if (Session == null)
                    return null;
                return (UserInfo)Session["CurrentUser"];
            }
        }
        public RouteInfo LastRoute
        {
            set { Session["LastRoute"] = value; }
            get { return (RouteInfo)Session["LastRoute"]; }
        }

        public CommonControllerBase()
        {
            CultureInfo culture = (CultureInfo)CultureInfo.CurrentCulture.Clone();
            culture = new CultureInfo("en-US", true);
            culture.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy";
            Thread.CurrentThread.CurrentCulture = culture;         
        }
        protected override void OnException(ExceptionContext filterContext)
        {
            var action = filterContext.RouteData.Values["action"].ToString();
            var controller = filterContext.RouteData.Values["controller"].ToString();
            filterContext.ExceptionHandled = true;
            if (filterContext.HttpContext.Request.IsAjaxRequest())
                filterContext.Result = this.RedirectToAction("JsonError", "Home", new { status = ErrorStatus.Default });
            else
                filterContext.Result = this.RedirectToAction("Error", "Home", new { status = ErrorStatus.Default });
            base.OnException(filterContext);
        }
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var action = filterContext.RouteData.Values["action"].ToString();
            var controller = filterContext.RouteData.Values["controller"].ToString();
            var param = filterContext.ActionParameters;
            var currentController = (CommonControllerBase)filterContext.Controller;
            var isLogoff = false;
            if (currentController.CurrentUser == null)
                isLogoff = true;
            bool skipAuthorization = filterContext.ActionDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true)
                         || filterContext.ActionDescriptor.ControllerDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true);
            if (isLogoff && !skipAuthorization)
            {
                var routeValues = new RouteValueDictionary(new
                {
                    action = "LogOff",
                    controller = "Account"
                });
                filterContext.Result = new RedirectToRouteResult(routeValues);
            }
            filterContext.Controller.ViewBag.CurrentUser = currentController.CurrentUser;
            _startRequestTime = DateTime.Now;
            base.OnActionExecuting(filterContext);
        }


    }
}