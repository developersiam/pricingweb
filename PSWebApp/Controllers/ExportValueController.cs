﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using PSWebApp.Repository;
using System.Web.Script.Serialization;
using PSWebApp.Models;
using PSWebApp.Models.JsonModels;
using PSWebApp.Model;

namespace PSWebApp.Controllers
{
    public class ExportValueController : CommonControllerBase
    {
        private ExportRepository _exportRepository;

        public ExportValueController()
        {
            _exportRepository = new ExportRepository();
        }


        //[Authorize]
        //[HttpGet]
        //private ActionResult GetPackedGrade(Dictionary<string, string> param, string sortName = "packedgrade", string sortOrder = "asc", int offset = 0, int limit = 10)
        //{
        //    int crop = Int16.Parse(param["scrop"]);
        //    ViewBag.PackedGrade = _exportRepository.GetPackedGrade(crop).Select(p => new PackedInfo()
        //    {
        //        PackedGrade = p.PackedGrade
        //    }).ToList();
        //    return View("Index");
        //}

        [Authorize]
        [HttpGet]
        public ActionResult Index()
        {
            LastRoute = new RouteInfo() { Controller = "ExportValue", Action = "Index" };
            return View("Index");
        }

        [Authorize]
        public ActionResult JsonPackedGrade(Dictionary<string, string> param, string sortName = "packedgrade", string sortOrder = "asc", int offset = 0, int limit = 10)
        {
            string crop = param["scrop"];
            if (String.IsNullOrWhiteSpace(crop))
                return Json(new { success = false, message = "Please input crop for get details" }, JsonRequestBehavior.AllowGet);

            var PackedGrade = _exportRepository.GetJsonPackedExportValue(crop).Select(a => new JsonExportValueModels()
            {
                Packed_Grade = a.PackedGrade,
                Customer = a.Customer,
                Customer_Grade = a.CustomerGrade,
                Crop = a.Crop.ToString(),             
                Export_Value = a.Value.ToString("#,##0.00")
            });
            var serializer = new JavaScriptSerializer() { MaxJsonLength = Int32.MaxValue };
            return Json(new { success= true, total = PackedGrade.Count(), rows = serializer.Deserialize<object>(PackedGrade.Any() ? "[" + String.Join(",", PackedGrade.Select(a => a.ToString())) + "]" : "[]") }, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult JsonClassifiedGrade(Dictionary<string, string> param, string sortName = "packedgrade", string sortOrder = "asc", int offset = 0, int limit = 10)
        {
            string crop = param["scrop"];
            if (String.IsNullOrWhiteSpace(crop))
                return Json(new { success = false, message = "Please input crop for get details" }, JsonRequestBehavior.AllowGet);

            var ClassifiedGrade = _exportRepository.GetJsonExportValue(crop).Select(a => new JsonClassifiedExportValueModels()
            {
                Classified_Grade = a.ClassifiedGrade,
                Crop = a.Crop.ToString(),
                Export_Value = a.Value.ToString("#,##0.00")
            });
            var serializer = new JavaScriptSerializer() { MaxJsonLength = Int32.MaxValue };
            return Json(new { success = true, total = ClassifiedGrade.Count(), rows = serializer.Deserialize<object>(ClassifiedGrade.Any() ? "[" + String.Join(",", ClassifiedGrade.Select(a => a.ToString())) + "]" : "[]") }, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult PackedGrade(int Crop)
        {
            LastRoute = new RouteInfo() { Controller = "ExportValue", Action = "PackedGrade" };
            if (Crop == 0)
                return null;

            ViewBag.Crop = Crop;
            ViewBag.PackedGrade = _exportRepository.GetPackedGrade(Crop).Select(p => new PackedInfo()
            {
                PackedGrade = p.PackedGrade
            }).ToList();
            var packed = _exportRepository.GetAllPackedGradeCustomer(Crop).Select(a => new PackedGradeCustomerInfo()
            {
                PackedGrade = a.PackedGrade,
                CustomerName = a.CustomerName,
                CustomerGrade = a.CustomerGrade
                //UnitPrice = a.UnitPrice,
                //AvgWeight = a.AvgWeight
            }).ToList();
            return View("PackedGrade", packed);
        }

        [Authorize]
        public ActionResult JsonUnitAndWeight(int crop, string packed, string customer, string customerPacked)
        {
             LastRoute = new RouteInfo() { Controller = "ExportValue", Action = "PackedGrade" };
            
            PackedGradeUnitWeightInfo rate = _exportRepository.GetJsonPackedGradeUnitWeight(crop, packed, customer, customerPacked);
            if (rate == null)
                return View("_ExportValuePartial", new PackedGradeUnitWeightModels());

            PackedGradeUnitWeightModels UnitAndWeight = new PackedGradeUnitWeightModels()
            {
                UnitPrice = rate.UnitPrice,
                AvgWeight = rate.AvgWeight
            };

            return View("_ExportValuePartial", UnitAndWeight);
        }

        [Authorize]
        public ActionResult SubmitExportValue(int Crop, string Customer, string PackedGrade, string CustomerPacked, decimal ExportValue)
        {
            //Check duplicate data
            bool duplicate = _exportRepository.ExistedPackedExportByCropAndPacked(Crop, Customer, PackedGrade, CustomerPacked);

            if (duplicate)
                return View("DuplicateData");

            _exportRepository.AddNewExportValue(
                Crop,
                Customer,
                PackedGrade,
                CustomerPacked,
                ExportValue,
                CurrentUser.C_NAME
                );
            return View("UpdateSuccess");
        }
        //---------------- Delete  -------------------------------------
        [Authorize]
        public ActionResult DeleteExportValue(int Crop, string Customer, string CustomerPacked, string PackedGrade)
        {
            if (_exportRepository.DeletePackedGrade(Crop, Customer, CustomerPacked, PackedGrade))
            {
                return View("UpdateSuccess");
            }
            else
            {
                return View("NotFoundInfo");
            }
        }

    }
}