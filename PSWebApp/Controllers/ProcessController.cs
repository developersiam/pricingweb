﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.Mvc;
using PSWebApp.Models;
using PSWebApp.Repository;
using System.Web.Script.Serialization;
using PSWebApp.Models.JsonModels;


namespace PSWebApp.Controllers
{
    public class ProcessController : CommonControllerBase
    {
        private ProcessRepository _processRepository;
        public ProcessController()
        {
            _processRepository = new ProcessRepository();
        }

        public ActionResult Index()
        {
            return View("Index");
        }

        public ActionResult JsonProcesses(string sortName = "C_PROCESS_NO", string sortOrder = "asc", int offset = 0, int limit = 10)
        {
            int total = 0;
            var processes = _processRepository.GetProcess_M().Select(a => new JsonProcessesModel()
            {
                C_PROG_ID = a.C_PROG_ID,
                C_PROCESS_NO = a.C_PROCESS_NO,
                C_PROCESS_DOC_NO = a.C_PROCESS_DOC_NO,
                D_PROCESS_DOC_DATE = a.D_PROCESS_DOC_DATE,
                N_CROP = a.N_CROP,
                C_TYPE = a.C_TYPE,
                C_SEASON = a.C_SEASON,
                C_PACKED_GRADE = a.C_PACKED_GRADE,
                C_PACK_MAT_CODE = a.C_PACK_MAT_CODE,
                N_PACK_MAT_CHARGE = a.N_PACK_MAT_CHARGE,
                C_TRAN_CHARGE_CODE = a.C_TRAN_CHARGE_CODE,
                N_TRAN_CHARGE = a.N_TRAN_CHARGE,
                C_REDRY_CHARGE_CODE = a.C_REDRY_CHARGE_CODE,
                N_REDRY_CHARGE = a.N_REDRY_CHARGE,
                N_CASE_WEIGHT = a.N_CASE_WEIGHT,
                N_PACKED_WEIGHT = a.N_PACKED_WEIGHT,
                C_CR_BY = a.C_CR_BY,
                D_CR_DATE = a.D_CR_DATE,
                C_UPD_BY = a.C_UPD_BY,
                D_UPD_DATE = a.D_UPD_DATE

            });
            var serializer = new JavaScriptSerializer() { MaxJsonLength = Int32.MaxValue };
            return Json(new { total = total, rows = serializer.Deserialize<object>(processes.Any() ? "[" + String.Join(",", processes.Select(a => a.ToString())) + "]" : "[]") }, JsonRequestBehavior.AllowGet);
        }


        // GET: Process/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Process/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Process/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Process/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Process/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Process/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Process/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
