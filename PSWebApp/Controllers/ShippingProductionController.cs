﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using PSWebApp.Repository;
using PSWebApp.Models;
using PSWebApp.Models.JsonModels;
using PSWebApp.Model;
using System.Web.Script.Serialization;

namespace PSWebApp.Controllers
{
    public class ShippingProductionController : CommonControllerBase
    {
        private ShippingProductionRepository _shippingProductionRepository;

        public ShippingProductionController()
        {
            _shippingProductionRepository = new ShippingProductionRepository();
        }

        private List<TypeInfo> TypeInfo
        {
            get
            {
                var types = new List<TypeInfo>();
                types = _shippingProductionRepository.GetTypeName();
                return types;

            }
        }
        private List<TypeRBInfo> TypeRBInfo
        {
            get
            {
                var typesRB = new List<TypeRBInfo>();
                var typesR = new TypeRBInfo { TypeName = "R", Description = "Product" };
                var typesB = new TypeRBInfo { TypeName = "B", Description = "By-Product" };
                typesRB.Add(typesR);
                typesRB.Add(typesB);

                return typesRB;
            }
        }
        public ActionResult Index()
        {
            LastRoute = new RouteInfo() { Controller = "ShippingProduction", Action = "Index" };
            ViewBag.Crop = CropProduction;
            return View();
        }
        private List<CropReceiving> CropProduction
        {
            get
            {
                List<CropReceiving> crop = _shippingProductionRepository.GetProcessingCorp();
                return crop;
            }
        }
        //--------------------- Show all Shipping Production by Crop  --------------------------------------------
        [Authorize]
        public ActionResult JsonShippingProduct(Dictionary<string, string> param, string sortName = "shipping_no", string sortOrder = "asc", int offset = 0, int limit = 10)
        {
            int scrop = Int16.Parse(param["scrop"]);
            var ShippingProduction = _shippingProductionRepository.GetJsonShippingProduct(scrop).Select(a => new JsonShippingProductionMasterModel()
            {
                DocNo = a.Doc_No,
                Shipping_no = a.Shipping_No,
                Doc_Date = a.Doc_Date.ToString(),
                Crop = a.Crop.ToString(),
                Type = a.Type
            });
            var serializer = new JavaScriptSerializer() { MaxJsonLength = Int32.MaxValue };
            return Json(new { total = ShippingProduction.Count(), rows = serializer.Deserialize<object>(ShippingProduction.Any() ? "[" + String.Join(",", ShippingProduction.Select(a => a.ToString())) + "]" : "[]") }, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult JsonAddingShippingProduct(Dictionary<string, string> param, string sortName = "shipping_no", string sortOrder = "asc", int offset = 0, int limit = 10)
        {
            int scrop = Int16.Parse(param["scrop"]);
            var ShippingProduction = _shippingProductionRepository.GetNewJsonShippingProduct();
            var filterCrop = ShippingProduction.Where(a => a.Crop == scrop).OrderBy(a => a.Shipping_No).ToList();
            var ShippingProductionList = filterCrop.Select(a => new JsonShippingProductionMasterModel()
            {
                DocNo = a.Doc_No,
                Shipping_no = a.Shipping_No,
                Doc_Date = a.Doc_Date.ToString(),
                Crop = a.Crop.ToString(),
                Type = a.Type
            });
            var serializer = new JavaScriptSerializer() { MaxJsonLength = Int32.MaxValue };
            return Json(new { total = ShippingProductionList.Count(), rows = serializer.Deserialize<object>(ShippingProductionList.Any() ? "[" + String.Join(",", ShippingProductionList.Select(a => a.ToString())) + "]" : "[]") }, JsonRequestBehavior.AllowGet);
        }

        //--------------------------------------- Edit Shipping Product --------------------------------------------------------
        [Authorize]
        public ActionResult EditShippingProduct(string Shippingno)
        {
            var ShippingType = TypeInfo;
            var TypeRB = TypeRBInfo;
            LastRoute = new RouteInfo() { Controller = "ShippingProduction", Action = "EditShippingProduct" };
            if (Shippingno == "")
                return null;

            ShippingProductionMasterInfo ShippingProduct = _shippingProductionRepository.GetShippingProductCalculate(Shippingno);

            ShippingProductModels existedShipping = new ShippingProductModels()
            {
                Crop = ShippingProduct.Crop,
                Doc_No = ShippingProduct.Doc_No,
                Shipping_No = ShippingProduct.Shipping_No,
                Doc_Date = ShippingProduct.Doc_Date,
                Type = ShippingProduct.Type,
                TypeRB = ShippingProduct.TypeRB,
                Total_Amt = ShippingProduct.Total_Cost.ToString("#,##0.00"),
                Total_Weight = ShippingProduct.Total_Weight.ToString("#,##0.00"),
                Average_Price = ShippingProduct.Average_Price.ToString("#,##0.00")
            };

            ViewBag.typeShipping = ShippingType;
            ViewBag.typeRB = TypeRB;
            return View("EditShippingProduct", existedShipping);
        }

        [Authorize]
        [HttpPost]
        public ActionResult EditShippingProduct(ShippingProductModels ShippingModel, string SelectedType, string SelectedTypeRB)
        {
            string NewDocumentNo;
            var ShippingType = TypeInfo;
            ShippingModel.Type = SelectedType;
            
            //Generate Document No
            NewDocumentNo = _shippingProductionRepository.GetDocumentNo(ShippingModel.Crop);
            if (!String.IsNullOrEmpty(NewDocumentNo))
                ShippingModel.Doc_No = NewDocumentNo;
            if (SelectedTypeRB != null)
                ShippingModel.TypeRB = SelectedTypeRB;

            _shippingProductionRepository.AddShippingProduct(ShippingModel.Doc_No,
                ShippingModel.Doc_Date,
                ShippingModel.Shipping_No,
                ShippingModel.Crop,
                ShippingModel.Type,
                ShippingModel.TypeRB,
                CurrentUser.C_NAME);
            return View("UpdateSuccess");
        }

        [Authorize]
        public ActionResult JsonShippingProductDetails(Dictionary<string, string> param, string sortName = "packed_grade", string sortOrder = "asc", int offset = 0, int limit = 10)
        {
            int total = 0;
            string ShippingNo = param["ShippingNo"];

            List<ShippingProductDetails> ShippingDetails = new List<ShippingProductDetails>();

            var JsonShippingDetails = new List<JsonShippingProductionDetailsModel>();
            var serializer = new JavaScriptSerializer() { MaxJsonLength = Int32.MaxValue };

            ShippingDetails = _shippingProductionRepository.GetJsonShippingDetails(ShippingNo);

            foreach (var r in ShippingDetails.OrderBy(r => r.PackedGrade))
            {

                JsonShippingDetails.Add(new JsonShippingProductionDetailsModel()
                {
                    Company = r.Company,
                    Packed_grade = r.PackedGrade,
                    Case_no = r.CaseNo,
                    Weight = Double.Parse(r.Weight).ToString("#,##0.00"),
                    Unit_cost = Double.Parse(r.UnitCost).ToString("#,##0.00"),
                    Amount = Double.Parse(r.Amount).ToString("#,##0.00")
                });
            }
            return Json(new { total = total, rows = serializer.Deserialize<object>(JsonShippingDetails.Any() ? "[" + String.Join(",", JsonShippingDetails.Select(a => a.ToString())) + "]" : "[]") }, JsonRequestBehavior.AllowGet);
        }

    }
}