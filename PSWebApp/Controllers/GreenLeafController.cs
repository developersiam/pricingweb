﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using PSWebApp.Repository;
using PSWebApp.Models;
using PSWebApp.Models.JsonModels;

namespace PSWebApp.Controllers
{
    public class GreenLeafController : CommonControllerBase
    {
        private ReceivingRepository _receivingRepository;

        public GreenLeafController()
        {
            _receivingRepository = new ReceivingRepository();
        }
        public ActionResult Index()
        {
            LastRoute = new RouteInfo() { Controller = "GreenLeaf", Action = "Index" };
            ViewBag.Crop = CropReceiving;
            return View();
        }
        private List<CropReceiving> CropReceiving
        {
            get
            {
                List<CropReceiving> crop = _receivingRepository.GetReceivingCorp();
                return crop;
            }
        }

        [Authorize]
        public ActionResult JsonReceivingMaster(Dictionary<string,string> param, string sortName = "rec_doc_no", string sortOrder = "asc", int offset = 0, int limit = 10)
        {

            //Dictionary<string, SORT_USER_BY> ordersBy = new Dictionary<string, SORT_USER_BY>{
            //    {"username", SORT_USER_BY.USERNAME },
            //    {"status", SORT_USER_BY.STATUS },
            //    {"first_name", SORT_USER_BY.FIRST_NAME},
            //    {"last_name", SORT_USER_BY.LAST_NAME},
            //    {"email", SORT_USER_BY.EMAIL},
            //    {"organization", SORT_USER_BY.ORGANIZATION},
            //    {"account_type", SORT_USER_BY.ACCOUNT_TYPE},
            //    {"expiry_date", SORT_USER_BY.EXPIRY_DATE},
            //    {"signup_date", SORT_USER_BY.SIGNUP_DATE}
            //};
            //Dictionary<string, SORT_ORDER> orders = new Dictionary<string, SORT_ORDER> { { "asc", SORT_ORDER.ASCENDING }, { "desc", SORT_ORDER.DESCENDING } };
            //var orderBy = ordersBy[sortName];
            //var order = orders[sortOrder];

            string scrop = param["scrop"];
            var ReceivingMaster = _receivingRepository.GetJsonReceivingMaster(scrop).Select(a => new JsonReceivingMasterModels()
            {
                C_REC_DOC_NO = a.C_REC_DOC_NO,
                C_REC_NO = a.C_REC_NO,
                N_CROP = Convert.ToString(a.N_CROP),
                D_REC_DOC_DATE = Convert.ToString(a.D_REC_DOC_DATE),
                C_BUY_DOC_NO = a.C_BUY_DOC_NO,
                C_COMPANY = a.C_COMPANY,
                C_TYPE = a.C_TYPE,
                C_SEASON = a.C_SEASON,
                C_CURER_CODE = a.C_CURER_CODE,
                C_CURER_NAME = a.C_CURER_NAME,
                C_AREA = a.C_AREA,
                C_GREEN_PRICE_CODE = a.C_GREEN_PRICE_CODE,
                C_REC_TYPE_RB = a.C_REC_TYPE_RB,
                C_PROG_ID = a.C_PROG_ID
            });
            var serializer = new JavaScriptSerializer() { MaxJsonLength = Int32.MaxValue };
            return Json(new { total = ReceivingMaster.Count() , rows = serializer.Deserialize<object>(ReceivingMaster.Any() ? "[" + String.Join(",", ReceivingMaster.Select(a => a.ToString())) + "]" : "[]") }, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult JsonReceivingDetails(Dictionary<string, string> param, string sortName = "bale_no", string sortOrder = "asc", int offset = 0, int limit = 10)
        {
            int total = 0;
            string RecDocno = param["recDocNo"];
            int RecNo = int.Parse(param["recNo"]);
            int Crop = int.Parse(param["crop"]);
            List<ReceivingDetails> receivingDetails = new List<ReceivingDetails>();
            var JsonRecDetails = new List<JsonReceivingDetailsModels>();
            var serializer = new JavaScriptSerializer() { MaxJsonLength = Int32.MaxValue };
            //if (RecDocno != "")
            //{ receivingDetails = _receivingRepository.GetJsonReceivingDetails(RecDocno);}
            //else
            //{ receivingDetails = _receivingRepository.GetJsonAddReceivingDetails(RecNo, Crop); }

            receivingDetails = _receivingRepository.GetJsonAddReceivingDetails(RecNo, Crop);

            foreach (var r in receivingDetails.OrderBy(r => Int32.Parse(r.BaleNo)))
            {
                JsonRecDetails.Add(new JsonReceivingDetailsModels()
                {
                    BaleNo = r.BaleNo,
                    GreenGrade = r.GreenGrade,
                    ClassifiedGrade = r.ClassifiedGrade,
                    BuyWeight = r.BuyWeight,
                    Weight = r.Weight,
                    UnitPrice = r.UnitPrice,
                    TCharge = r.TCharge,
                    TotalUnitPrice = r.TotalUnitPrice,
                    Amount = r.Amount,
                    FCVClass = r.FCVClass
                });
            }
            return Json(new { total = total, rows = serializer.Deserialize<object>(JsonRecDetails.Any() ? "[" + String.Join(",", JsonRecDetails.Select(a => a.ToString())) + "]" : "[]") }, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult EditReceiving(string RecNo)
        {
            LastRoute = new RouteInfo() { Controller = "GreenLeaf", Action = "EditReceiving" };
            if (RecNo == "")
                return null;

            MatReceivingInfo receivingMat = _receivingRepository.GetReceivingCalculate(RecNo);

            ReceivingModels existedReceiving = new ReceivingModels()
            {
                ReceivingNo = receivingMat.ReceivingNo,
                DocumentNo = receivingMat.DocumentNo,
                BuyingDocNo = receivingMat.BuyingDocNo,
                DocumentDate = receivingMat.DocumentDate,
                CompanyCode = receivingMat.CompanyCode,
                Crop = receivingMat.Crop,
                Type = receivingMat.Type,
                Season = receivingMat.Season,
                CurerCode = receivingMat.CurerCode,
                CurerName = receivingMat.CurerName,
                Area = receivingMat.Area,
                RecordType = receivingMat.RecordType,
                GreenPriceCode = receivingMat.GreenPriceCode,
                WeightBuy = receivingMat.WeightBuy.ToString("#,##0.00"),
                WeightReceived = receivingMat.WeightReceived.ToString("#,##0.00"),
                AveragePriceBuy = receivingMat.AveragePriceBuy.ToString("#,##0.00"),
                AveragePriceRec = receivingMat.AveragePriceRec.ToString("#,##0.00"),
                TotalAmount = receivingMat.TotalAmount.ToString("#,##0.00")
            };
            return View("EditReceiving", existedReceiving);
        }

        [Authorize]
        [HttpPost]
        public ActionResult EditReceiving(ReceivingModels recModel)
        {
            string NewDocumentNo;
            //Generate Receiving Document No
            NewDocumentNo = _receivingRepository.GetDocumentNo(recModel.Crop);
            if (!String.IsNullOrEmpty(NewDocumentNo))
                recModel.DocumentNo = NewDocumentNo;

            if (String.IsNullOrEmpty(recModel.RecordType))
                recModel.RecordType = "B";
            //Tricker generate Buying no
            recModel.BuyingDocNo = "";

            _receivingRepository.AddReceiving(
                recModel.DocumentNo,
                recModel.DocumentDate,
                recModel.ReceivingNo,
                recModel.BuyingDocNo,
                recModel.Crop,
                recModel.CompanyCode,
                recModel.Type,
                recModel.Season,
                recModel.CurerCode,
                recModel.CurerName,
                recModel.Area,
                recModel.GreenPriceCode,
                recModel.RecordType,
                CurrentUser.C_NAME
                );
            return View("UpdateSuccess");
        }

        [Authorize]
        public ActionResult JsonAddingReceivingMaster(string sortName = "rec_doc_no", string sortOrder = "asc", int offset = 0, int limit = 10)
        {
            int total = 0;
            var ReceivingMaster = _receivingRepository.GetJsonAddReceivingMaster().Select(a => new JsonReceivingMasterModels()
            {
                C_REC_DOC_NO = a.C_REC_DOC_NO,
                C_REC_NO = a.C_REC_NO,
                N_CROP = Convert.ToString(a.N_CROP),
                D_REC_DOC_DATE = Convert.ToString(a.D_REC_DOC_DATE),
                C_BUY_DOC_NO = a.C_BUY_DOC_NO,
                C_COMPANY = a.C_COMPANY,
                C_TYPE = a.C_TYPE,
                C_SEASON = a.C_SEASON,
                C_CURER_CODE = a.C_CURER_CODE,
                C_CURER_NAME = a.C_CURER_NAME,
                C_AREA = a.C_AREA,
                C_GREEN_PRICE_CODE = a.C_GREEN_PRICE_CODE,
                C_REC_TYPE_RB = a.C_REC_TYPE_RB,
                C_PROG_ID = a.C_PROG_ID
            });
            var serializer = new JavaScriptSerializer() { MaxJsonLength = Int32.MaxValue };
            return Json(new { total = total, rows = serializer.Deserialize<object>(ReceivingMaster.Any() ? "[" + String.Join(",", ReceivingMaster.Select(r => r.ToString())) + "]" : "[]") }, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult AddReceiving(ReceivingModels recModel)
        {
            _receivingRepository.AddReceiving(
                recModel.DocumentNo,
                recModel.DocumentDate,
                recModel.ReceivingNo,
                recModel.BuyingDocNo,
                recModel.Crop,
                recModel.CompanyCode,
                recModel.Type,
                recModel.Season,
                recModel.CurerCode,
                recModel.CurerName,
                recModel.Area,
                recModel.GreenPriceCode,
                recModel.RecordType,
                CurrentUser.C_NAME
                );
            return View("UpdateSuccess");
        }

        [Authorize]
        public ActionResult DeleteReceiving(string RecDocNo)
        {
            _receivingRepository.DeleteReceiving(RecDocNo);
            return View("UpdateSuccess");        
        }
        //public ActionResult ShowReceivingForAdding(int recDocNo)
        //{
        //    LastRoute = new RouteInfo() { Controller = "GreenLeaf", Action = "AddReceiving" };
        //    ViewBag.ReceivingNotDone = _receivingRepository.GetAllNotDoneReceiving().Select(rd => new ReceivingListNotDone()
        //    {
        //        DocNo = rd.DocNo,
        //        Crop = rd.Crop
        //    }).ToList();

        //    MatReceivingInfo receivingMat = _receivingRepository.GetReceivingCalculateForAdding(recDocNo);
        //    MatReceivingModels existedReceiving = new MatReceivingModels()
        //    {
        //        ReceivingNo = receivingMat.ReceivingNo,
        //        DocumentNo = "123456",
        //        BuyingDocNo = "123456",
        //        DocumentDate = DateTime.Today,
        //        CompanyCode = receivingMat.CompanyCode,
        //        Crop = receivingMat.Crop,
        //        Type = receivingMat.Type,
        //        Season = receivingMat.Season,
        //        CurerCode = receivingMat.CurerCode,
        //        CurerName = receivingMat.CurerName,
        //        Area = receivingMat.Area,
        //        RecordType = receivingMat.RecordType,
        //        GreenPriceCode = receivingMat.GreenPriceCode,
        //        WeightBuy = receivingMat.WeightBuy,
        //        WeightReceived = receivingMat.WeightReceived,
        //        AveragePriceBuy = receivingMat.AveragePriceBuy,
        //        AveragePriceRec = receivingMat.AveragePriceRec,
        //        TotalAmount = receivingMat.TotalAmount
        //    };

        //    return View("_ReceivingPartial", existedReceiving);
        //}

        //[Authorize]
        //public ActionResult JsonAddReceivingDetails(Dictionary<string, int> param, string sortName = "BaleNo", string sortOrder = "asc", int offset = 0, int limit = 10)
        //{
        //    int total = 0;
        //    int RecDocno = param["recDocNo"];
        //    int Crop = param["crop"];
        //    var ReceivingDetails = _receivingRepository.GetJsonAddReceivingDetails(RecDocno, Crop).Select(r => new JsonReceivingDetailsModels()
        //    {
        //        BaleNo = r.BaleNo,
        //        GreenGrade = r.GreenGrade,
        //        ClassifiedGrade = r.ClassifiedGrade,
        //        BuyWeight = r.BuyWeight,
        //        Weight = r.Weight,
        //        UnitPrice = r.UnitPrice,
        //        TCharge = r.TCharge,
        //        TotalUnitPrice = r.TotalUnitPrice,
        //        Amount = r.Amount,
        //        FCVClass = r.FCVClass

        //    });
        //    var serializer = new JavaScriptSerializer() { MaxJsonLength = Int32.MaxValue };
        //    return Json(new { total = total, rows = serializer.Deserialize<object>(ReceivingDetails.Any() ? "[" + String.Join(",", ReceivingDetails.Select(a => a.ToString())) + "]" : "[]") }, JsonRequestBehavior.AllowGet);
        //}
        //public ActionResult AddReceiving(int recDocNo)
        //{
        //    //List only Receiving not done yet
        //    ViewBag.ReceivingNotDone = _receivingRepository.GetAllNotDoneReceiving().Select(rd => new ReceivingListNotDone()
        //    {
        //        DocNo = rd.DocNo,
        //        Crop = rd.Crop
        //    }).ToList();


        //        MatReceivingInfo receivingMat = _receivingRepository.GetReceivingCalculateForAdding(recDocNo);
        //        MatReceivingModels existedReceiving = new MatReceivingModels()
        //        {
        //            ReceivingNo = receivingMat.ReceivingNo,
        //            DocumentNo = "",
        //            BuyingDocNo = "",
        //            DocumentDate = DateTime.Today,
        //            CompanyCode = receivingMat.CompanyCode,
        //            Crop = receivingMat.Crop,
        //            Type = receivingMat.Type,
        //            Season = receivingMat.Season,
        //            CurerCode = receivingMat.CurerCode,
        //            CurerName = receivingMat.CurerName,
        //            Area = receivingMat.Area,
        //            RecordType = receivingMat.RecordType,
        //            GreenPriceCode = receivingMat.GreenPriceCode,
        //            WeightBuy = receivingMat.WeightBuy,
        //            WeightReceived = receivingMat.WeightReceived,
        //            AveragePriceBuy = receivingMat.AveragePriceBuy,
        //            AveragePriceRec = receivingMat.AveragePriceRec,
        //            TotalAmount = receivingMat.TotalAmount
        //        };

        //        return View("AddReceiving", existedReceiving);
        //}

    }
}