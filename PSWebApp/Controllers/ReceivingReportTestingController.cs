﻿using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PSWebApp.Reports.RPT;

namespace PSWebApp.Controllers
{
    public class ReceivingReportTestingController : Controller
    {
        // GET: ReceivingReportTesting
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ShowAllReceiving()
        {
            StecDBMSEntities db = new StecDBMSEntities();
            var c = (from b in db.PS_RECEIVING_M select b).ToList();

            Receiving rpt = new Receiving();

            rpt.Load();
            rpt.SetDataSource(c);
            Stream s = rpt.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);

            return File(s, "application/pdf");

        }

        public ActionResult Views()
        {
            StecDBMSEntities db = new StecDBMSEntities();
            var c = (from b in db.PS_RECEIVING_M select b).ToList();

            Receiving rpt = new Receiving();

            rpt.Load();
            rpt.SetDataSource(c);
            Stream s = rpt.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);

            return File(s, "application/pdf");
        }
    }
}