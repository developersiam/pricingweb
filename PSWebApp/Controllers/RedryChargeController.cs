﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using PSWebApp.Repository;
using PSWebApp.Models;
using PSWebApp.Models.JsonModels;
using PSWebApp.Model;


namespace PSWebApp.Controllers
{
    public class RedryChargeController : CommonControllerBase
    {
        private RedryChargeRepository _redrychargeRepository;

        public RedryChargeController()
        {
            _redrychargeRepository = new RedryChargeRepository();
        }

        public ActionResult Index()
        {
            LastRoute = new RouteInfo() { Controller = "RedryCharge", Action = "Index" };
            return View();
        }
        private List<CropReceiving> CropRegrade
        {
            get
            {
                List<CropReceiving> crop = _redrychargeRepository.GetCorp();
                return crop;
            }
        }
        //------------------- Show Redry Charge details ----------------
        [Authorize]
        public ActionResult JsonShowRedryCharge(Dictionary<string, string> param, string sortName = "redry_trans_charge_code", string sortOrder = "asc", int offset = 0, int limit = 10)
        {
            int scrop = Int16.Parse(param["scrop"]);
            var ReDryMaster = _redrychargeRepository.GetRedry_M(scrop).Select(a => new JsonRedryTransCharge()
            {
                Crop = a.Crop.ToString(),
                Redry_Trans_Charge_Code = a.Redry_Charge_Code,
                Redry_Trans_Charge_Desc = a.Redry_Charge_Description,
                Charge = a.Charge.ToString()
            });
            var serializer = new JavaScriptSerializer() { MaxJsonLength = Int32.MaxValue };
            return Json(new { total = ReDryMaster.Count(), rows = serializer.Deserialize<object>(ReDryMaster.Any() ? "[" + String.Join(",", ReDryMaster.Select(a => a.ToString())) + "]" : "[]") }, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult JsonShowTransCharge(Dictionary<string, string> param, string sortName = "redry_trans_charge_code", string sortOrder = "asc", int offset = 0, int limit = 10)
        {
            int scrop = Int16.Parse(param["scrop"]);
            var ReDryMaster = _redrychargeRepository.GetTransport_M(scrop).Select(a => new JsonRedryTransCharge()
            {
                Crop = a.Crop.ToString(),
                Redry_Trans_Charge_Code = a.Trans_Charge_Code,
                Redry_Trans_Charge_Desc = a.Trans_Charge_Description,
                Charge = a.Charge.ToString()
            });
            var serializer = new JavaScriptSerializer() { MaxJsonLength = Int32.MaxValue };
            return Json(new { total = ReDryMaster.Count(), rows = serializer.Deserialize<object>(ReDryMaster.Any() ? "[" + String.Join(",", ReDryMaster.Select(a => a.ToString())) + "]" : "[]") }, JsonRequestBehavior.AllowGet);
        }
        //---------------- Delete Redry & Transportation Charge -------------------------------------
        [Authorize]
        public ActionResult DeleteRedryCharge(int Crop, string Code, string type)
        {
            if (_redrychargeRepository.DeleteRedryCharge(Crop, Code, type))
            {
                return View("UpdateSuccess");
            }
            else
            {
                return View("NotFoundInfo");
            }         
        }       
        [Authorize]
        public ActionResult SubmitRedryCharge(int Crop, string Code, string type, string Desc, decimal Cost)
        {
            _redrychargeRepository.SubmitRedryTransport(
                Crop, 
                Code, 
                type, 
                Desc, 
                Cost, 
                CurrentUser.C_NAME);
            return View("UpdateSuccess");
        }
    }
}