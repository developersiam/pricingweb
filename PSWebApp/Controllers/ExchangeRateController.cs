﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using PSWebApp.Repository;
using PSWebApp.Models;
using PSWebApp.Models.JsonModels;
using PSWebApp.Model;
using System.Web.Script.Serialization;

namespace PSWebApp.Controllers
{
    public class ExchangeRateController : CommonControllerBase
    {
        private ExchangeRateRepository _exchangerateRepository;

        public ExchangeRateController()
        {
            _exchangerateRepository = new ExchangeRateRepository();
        }

        [Authorize]
        public ActionResult Index()
        {
            LastRoute = new RouteInfo() { Controller = "ExchangeRate", Action = "Index" };
            return View();
        }
        //------------------- Show Exchange Rate ----------------
        [Authorize]
        public ActionResult JsonShowExchangeRate(Dictionary<string, string> param, string sortName = "eff_date", string sortOrder = "asc", int offset = 0, int limit = 10)
        {
            string sEffDate = param["sEffDate"];
            DateTime EffDate = ExchangeRateRepository.DateStringToDate(sEffDate);

            var ExchangeRate = _exchangerateRepository.GetExchangeRate(EffDate).Select(a => new JsonExchangeRateModels()
            {
                Effective_Date = a.EffectiveDate.ToString(),
                Exchange_Rate = a.ExchangeRate.ToString("#,##0.0000")
            });
            var serializer = new JavaScriptSerializer() { MaxJsonLength = Int32.MaxValue };
            return Json(new { total = ExchangeRate.Count(), rows = serializer.Deserialize<object>(ExchangeRate.Any() ? "[" + String.Join(",", ExchangeRate.Select(a => a.ToString())) + "]" : "[]") }, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult SubmitExchangeRate(string EffDate, decimal Rate)
        {
            DateTime sEffDate = ExchangeRateRepository.DateStringToDate(EffDate);
            _exchangerateRepository.SubmitExchangeRate(
                sEffDate,
                Rate,
                CurrentUser.C_NAME);
            return View("UpdateSuccess");
        }
        //---------------- Delete Packing Material -------------------------------------
        [Authorize]
        public ActionResult DeleteExchangeRate(string EffDate, decimal Rate)
        {
            DateTime sEffDate = ExchangeRateRepository.DateStringToDate(EffDate);
            if (_exchangerateRepository.DeleteExchangeRate(sEffDate, Rate))
            {
                return View("UpdateSuccess");
            }
            else
            {
                return View("NotFoundInfo");
            }
        }

    }
}