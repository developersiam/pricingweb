﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using PSWebApp.Repository;
using PSWebApp.Models;
using PSWebApp.Models.JsonModels;
using PSWebApp.Model;
using System.Web.Script.Serialization;

namespace PSWebApp.Controllers
{
    public class PackingMatController : CommonControllerBase
    {
        private PackingMatRepository _packingMatrepository;

        public PackingMatController()
        {
            _packingMatrepository = new PackingMatRepository();
        }

        [Authorize]
        public ActionResult Index()
        {
            LastRoute = new RouteInfo() { Controller = "PackingMat", Action = "Index" };
            return View();
        }
        //------------------- Show Packing Materail Cost ----------------
        [Authorize]
        public ActionResult JsonShowPackingMat(Dictionary<string, string> param, string sortName = "packing_mat", string sortOrder = "asc", int offset = 0, int limit = 10)
        {
            int scrop = Int16.Parse(param["scrop"]);
            var Packing = _packingMatrepository.GetPackingMat(scrop).Select(a => new JsonPackingMatModels()
            {
                Crop = a.Crop.ToString(),
                Packing_Mat = a.Packing_Mat,
                Price = a.Price.ToString("#,##0.00")
            });
            var serializer = new JavaScriptSerializer() { MaxJsonLength = Int32.MaxValue };
            return Json(new { total = Packing.Count(), rows = serializer.Deserialize<object>(Packing.Any() ? "[" + String.Join(",", Packing.Select(a => a.ToString())) + "]" : "[]") }, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult SubmitPackingPrice(int Crop, string Packing, decimal Price)
        {
            _packingMatrepository.SubmitPackingMat(
                Crop,
                Packing,
                Price);
            return View("UpdateSuccess");
        }
        //---------------- Delete Packing Material -------------------------------------
        [Authorize]
        public ActionResult DeletePackingPrice(int Crop, string Packing, decimal Price)
        {
            if (_packingMatrepository.DeletePackingMat(Crop, Packing, Price))
            {
                return View("UpdateSuccess");
            }
            else
            {
                return View("NotFoundInfo");
            }
        }

    }
}