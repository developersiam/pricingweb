﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CrystalDecisions.CrystalReports.Engine;


namespace PSWebApp.Controllers
{
    public class ReportController : CommonControllerBase
    {
        // GET: Report
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult reportView(string id)
        {
            //Session["ReportSource"] = GetdataFromDB();
            Response.Redirect("/Report/ReportForm.aspx");
            return View();
        }


    }
}