﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using PSWebApp.Repository;
using PSWebApp.Models;
using PSWebApp.Models.JsonModels;
using PSWebApp.Model;

namespace PSWebApp.Controllers
{
    public class HandStripController : CommonControllerBase
    {
        private HandStripRepository _handStripRepository;

        public HandStripController()
        {
            _handStripRepository = new HandStripRepository();
        }
        public ActionResult Index()
        {
            LastRoute = new RouteInfo() { Controller = "HandStrip", Action = "Index" };
            ViewBag.Crop = CropGreenTransfer;
            return View();
        }
        private List<CropReceiving> CropGreenTransfer
        {
            get
            {
                List<CropReceiving> crop = _handStripRepository.GetCorp();
                return crop;
            }
        }

        //--------------------- Show all Hand Strip by Crop on Index --------------------------------------------
        [Authorize]
        public ActionResult JsonHandStripMaster(Dictionary<string, string> param, string sortName = "hand_doc_no", string sortOrder = "asc", int offset = 0, int limit = 10)
        {
            string scrop = param["scrop"];
            var HandStripMaster = _handStripRepository.GetJsonHandStripMaster(scrop).Select(a => new JsonHandStripMasterModels()
            {
                Hand_Doc_No = a.HandStrip_Doc_No,
                Hand_No = a.HandStrip_No,
                Hand_Doc_Date = Convert.ToString(a.HandStrip_Doc_Date),
                Crop = Convert.ToString(a.Crop),
                Type = a.Type,
                Season = a.Season,
                Company = a.Company
            });
            var serializer = new JavaScriptSerializer() { MaxJsonLength = Int32.MaxValue };
            return Json(new { total = HandStripMaster.Count(), rows = serializer.Deserialize<object>(HandStripMaster.Any() ? "[" + String.Join(",", HandStripMaster.Select(a => a.ToString())) + "]" : "[]") }, JsonRequestBehavior.AllowGet);
        }

        //Show only New Hand Strip
        [Authorize]
        public ActionResult JsonAddingHandStripMaster(Dictionary<string, string> param, string sortName = "hand_doc_no", string sortOrder = "asc", int offset = 0, int limit = 10)
        {
            int total = 0;
            int scrop = Int16.Parse(param["scrop"]);
            var HandStripMaster = _handStripRepository.GetJsonAddHandSMaster(scrop);
            var filterCrop = HandStripMaster.Where(a => a.Crop == scrop).OrderBy(a => a.HandStrip_No).ToList();
            var HandStriplist = filterCrop.Select(a => new JsonHandStripMasterModels()
            {
                Hand_Doc_No = a.HandStrip_Doc_No,
                Hand_No = a.HandStrip_No,
                Hand_Doc_Date = Convert.ToString(a.HandStrip_Doc_Date),
                Crop = Convert.ToString(a.Crop),
                Type = a.Type,
                Season = a.Season,
                Company = a.Company
            });
            var serializer = new JavaScriptSerializer() { MaxJsonLength = Int32.MaxValue };
            return Json(new { total = total, rows = serializer.Deserialize<object>(HandStriplist.Any() ? "[" + String.Join(",", HandStriplist.Select(r => r.ToString())) + "]" : "[]") }, JsonRequestBehavior.AllowGet);
        }

        //--------------------------------------- Edit Hand Strip --------------------------------------------------------
        [Authorize]
        public ActionResult EditHandStrip(string HSNo)
        {
            LastRoute = new RouteInfo() { Controller = "HandStrip", Action = "EditHandStrip" };
            if (HSNo == "")
                return null;

            MatIsHandStripInfo handSMatIS = _handStripRepository.GetHandSCalculate(HSNo);
            if (handSMatIS == null)
                return View("NotFoundInfo");

            HandStripModels existedHandS = new HandStripModels()
            {
                HandStripNo = handSMatIS.HandStripNo,
                DocumentNo = handSMatIS.DocumentNo,
                DocumentDate = handSMatIS.DocumentDate,
                CompanyCode = handSMatIS.CompanyCode,
                Crop = handSMatIS.Crop,
                Type = handSMatIS.Type,
                Season = handSMatIS.Season,
                TotalWeight = handSMatIS.TotalWeight,
                AveragePrice = handSMatIS.AveragePrice,
                TotalAmount = handSMatIS.TotalAmount
            };
            return View("EditHandStrip", existedHandS);
        }

        [Authorize]
        public ActionResult JsonHandStripDetails(Dictionary<string, string> param, string sortName = "bale_no", string sortOrder = "asc", int offset = 0, int limit = 10)
        {
            int total = 0;
            string HSNo = param["HSNo"];
            List<HandStripDetails> HandStripDetails = new List<HandStripDetails>();
            var JsonHandSDetails = new List<JsonHandStripDetailsModels>();
            var serializer = new JavaScriptSerializer() { MaxJsonLength = Int32.MaxValue };

            HandStripDetails = _handStripRepository.GetJsonHandSDetails(HSNo);

            foreach (var r in HandStripDetails.OrderBy(r => Int32.Parse(r.BaleNo)))
            {
                JsonHandSDetails.Add(new JsonHandStripDetailsModels()
                {
                    BaleNo = r.BaleNo,
                    Curer = r.Curer,
                    GreenGrade = r.GreenGrade,
                    Classified = r.Classified,
                    FlagIO = r.FlagIO,
                    Weight = Double.Parse(r.Weight).ToString("#,##0.00"),
                    UnitPrice = Double.Parse(r.UnitPrice).ToString("#,##0.00"),
                    Amount = Double.Parse(r.Amount).ToString("#,##0.00")
                });
            }
            return Json(new { total = total, rows = serializer.Deserialize<object>(JsonHandSDetails.Any() ? "[" + String.Join(",", JsonHandSDetails.Select(a => a.ToString())) + "]" : "[]") }, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DeleteHandStrip(string HSDocNo)
        {
            _handStripRepository.DeleteHandS(HSDocNo);
            return View("UpdateSuccess");
        }

        [Authorize]
        [HttpPost]
        public ActionResult EditHandStrip(HandStripModels HSModel)
        {
            string NewDocumentNo;
            //Generate Document No
            NewDocumentNo = _handStripRepository.GetDocumentNo(HSModel.Crop);

            if (!String.IsNullOrEmpty(NewDocumentNo))
                HSModel.DocumentNo = NewDocumentNo;

            _handStripRepository.AddHandStrip(
                HSModel.DocumentNo,
                HSModel.DocumentDate,
                HSModel.HandStripNo,
                HSModel.Crop,
                HSModel.CompanyCode,
                HSModel.Type,
                HSModel.Season,
                CurrentUser.C_NAME
                );
            return View("UpdateSuccess");
        }

    }
}