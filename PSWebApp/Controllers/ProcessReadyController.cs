﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using PSWebApp.Repository;
using PSWebApp.Models;
using PSWebApp.Models.JsonModels;
using PSWebApp.Model;
using System.Web.Script.Serialization;

namespace PSWebApp.Controllers
{
    public class ProcessReadyController : CommonControllerBase
    {
        private ProcessReadyRepository _processreadyRepository;

        public ProcessReadyController()
        {
            _processreadyRepository = new ProcessReadyRepository();
        }
        public ActionResult Index()
        {
            LastRoute = new RouteInfo() { Controller = "ProcessReady", Action = "Index" };
            ViewBag.Crop = CropProduction;
            return View();
        }
        private List<CropReceiving> CropProduction
        {
            get
            {
                List<CropReceiving> crop = _processreadyRepository.GetProcessingCorp();
                return crop;
            }
        }
        //--------------------- Show all Processing Product by Crop on Index --------------------------------------------
        [Authorize]
        public ActionResult JsonProcessReady(Dictionary<string, string> param, string sortName = "processing_no", string sortOrder = "asc", int offset = 0, int limit = 10)
        {
            int scrop = Int16.Parse(param["scrop"]);
            var ProcessReadyMaster = _processreadyRepository.GetJsonProcessReady(scrop).Select(a => new JsonProcessReadyMasterModel()
            {
                DocNo = a.Doc_No,
                ProcessingR_No = a.ProcessingR_No,
                Doc_Date = a.Doc_Date.ToString(),
                Crop = a.Crop.ToString(),
                Type = a.Type,
                Season = a.Season,
                Packed_Grade = a.Packed_Grade,
                Packed_Weight = a.Packed_Weight.ToString("#,##0.00"),
                Pack_Mat = a.Pack_Mat.ToString("#,##0.00"),
                Weight = a.Weight.ToString("#,##0.00"),
                Redry_Charge = a.Redry_Charge.ToString("#,##0.00"),
                Transportation = a.Transportation.ToString("#,##0.00")

            });
            var serializer = new JavaScriptSerializer() { MaxJsonLength = Int32.MaxValue };
            return Json(new { total = ProcessReadyMaster.Count(), rows = serializer.Deserialize<object>(ProcessReadyMaster.Any() ? "[" + String.Join(",", ProcessReadyMaster.Select(a => a.ToString())) + "]" : "[]") }, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult JsonAddingProcessReady(Dictionary<string,string> param, string sortName = "processing_no", string sortOrder = "asc", int offset = 0, int limit=10)
        {
            int scrop = Int16.Parse(param["scrop"]);
            var ProcessReadyMaster = _processreadyRepository.GetNewJsonProcessReady();
            var filterCrop = ProcessReadyMaster.Where(a => a.Crop == scrop).OrderBy(a => a.ProcessingR_No).ToList();
            var ProcessReadyList = filterCrop.Select(a => new JsonProcessReadyMasterModel()
            {
                DocNo = a.Doc_No,
                ProcessingR_No = a.ProcessingR_No,
                Doc_Date = a.Doc_Date.ToString(),
                Crop = a.Crop.ToString(),
                Type = a.Type,
                Season = a.Season,
                Packed_Grade = a.Packed_Grade,
                Packed_Weight = a.Packed_Weight.ToString("#,##0.00"),
                Pack_Mat = a.Pack_Mat.ToString("#,##0.00"),
                Weight = a.Weight.ToString("#,##0.00"),
                Redry_Charge = a.Redry_Charge.ToString("#,##0.00"),
                Transportation = a.Transportation.ToString("#,##0.00")
            });
            var serializer = new JavaScriptSerializer() { MaxJsonLength = Int32.MaxValue };
            return Json(new { total = ProcessReadyList.Count(), rows = serializer.Deserialize<object>(ProcessReadyList.Any() ? "[" + String.Join(",", ProcessReadyList.Select(a => a.ToString())) + "]" : "[]") }, JsonRequestBehavior.AllowGet);
        }

        //--------------------------------------- Edit Process Ready --------------------------------------------------------
        [Authorize]
        public ActionResult EditProcessReady(string ProcessingR_no)
        {
            LastRoute = new RouteInfo() { Controller = "ProcessReady", Action = "EditProcessReady" };
            if (ProcessingR_no == "")
                return null;

            PDProcessReadyMasterInfo ProcessReadyPD = _processreadyRepository.GetProcessReadyCalculate(ProcessingR_no);

            ProcessReadyModels existedProcessReady = new ProcessReadyModels()
            {
                Crop = ProcessReadyPD.Crop,
                Doc_No = ProcessReadyPD.Doc_No,
                ProcessingR_No = ProcessReadyPD.ProcessingR_No,
                Doc_Date = ProcessReadyPD.Doc_Date,
                Packed_Grade = ProcessReadyPD.Packed_Grade,
                Type = ProcessReadyPD.Type,
                Season = ProcessReadyPD.Season,
                Packed_Weight = Decimal.Parse(ProcessReadyPD.Packed_Weight.ToString("#,##0.00")),
                Weight = Decimal.Parse(ProcessReadyPD.Weight.ToString("#,##0.00")),
                Redry_Charge = Decimal.Parse(ProcessReadyPD.Redry_Charge.ToString("#,##0.00")),
                Pack_Mat = Decimal.Parse(ProcessReadyPD.Pack_Mat.ToString("#,##0.00")),
                Transportation = Decimal.Parse(ProcessReadyPD.Transportation.ToString("#,##0.00")),
                Total_Amt = (ProcessReadyPD.Total_Amt).ToString("#,##0.00"),
                Total_Weight = (ProcessReadyPD.Total_Weight).ToString("#,##0.00"),
                Average_Price = (ProcessReadyPD.Average_Price).ToString("#,##0.00")
            };
            return View("EditProcessReady", existedProcessReady);
        }

        [Authorize]
        [HttpPost]
        public ActionResult EditProcessReady(ProcessReadyModels PRModel)
        {
            string NewDocumentNo;
            //Generate Receiving Document No
            NewDocumentNo = _processreadyRepository.GetDocumentNo(PRModel.Crop);
            if (!String.IsNullOrEmpty(NewDocumentNo))
                PRModel.Doc_No = NewDocumentNo;

            _processreadyRepository.AddProcessReady(PRModel.Doc_No,
                PRModel.Doc_Date,
                PRModel.ProcessingR_No,
                PRModel.Crop,
                PRModel.Type,
                PRModel.Season,
                PRModel.Packed_Grade,
                "", PRModel.Pack_Mat,
                "", PRModel.Transportation, 
                "", PRModel.Redry_Charge,
                PRModel.Weight,
                PRModel.Packed_Weight,
                CurrentUser.C_NAME);
            return View("UpdateSuccess");
        }

        [Authorize]
        public ActionResult JsonProcessReadyDetails(Dictionary<string, string> param, string sortName = "bale_no", string sortOrder = "asc", int offset = 0, int limit = 10)
        {
            int total = 0;
            string ProcessNo = param["ProcessNo"];

            List<ProcessReadyDetails> PRDetails = new List<ProcessReadyDetails>();

            var JsonPRDetails = new List<JsonProcessReadyDetailsModel>();
            var serializer = new JavaScriptSerializer() { MaxJsonLength = Int32.MaxValue };

            PRDetails = _processreadyRepository.GetJsonProcessRDetails(ProcessNo);

            foreach (var r in PRDetails.OrderBy(r => Int32.Parse(r.BaleNo)))
            {
                JsonPRDetails.Add(new JsonProcessReadyDetailsModel()
                {
                    BaleNo = r.BaleNo,
                    GreenGrade = r.GreenGrade,
                    Packed_grade = r.PackedGrade,
                    Weight = Double.Parse(r.Weight).ToString("#,##0.00"),
                    UnitPrice = Double.Parse(r.UnitPrice).ToString("#,##0.00"),
                    Gep = r.GEP,
                    Gep_per = r.GEP_Per,
                    Amount = Double.Parse(r.Amount).ToString("#,##0.00"),
                    Total_unit = Double.Parse(r.ToTalUnit).ToString("#,##0.00"),
                    Flag = r.Flag
                });
            }
            return Json(new { total = total, rows = serializer.Deserialize<object>(JsonPRDetails.Any() ? "[" + String.Join(",", JsonPRDetails.Select(a => a.ToString())) + "]" : "[]") }, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DeleteProcessReady(string PRDocNo)
        {
            _processreadyRepository.DeleteProcessReady(PRDocNo);
            return View("UpdateSuccess");
        }



    }
}