﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using PSWebApp.Repository;
using PSWebApp.Models;
using PSWebApp.Models.JsonModels;
using PSWebApp.Model;

namespace PSWebApp.Controllers
{
    public class GreenTransferController : CommonControllerBase
    {
        private GreenTransferRepository _greenTransferRepository;

        public GreenTransferController()
        {
            _greenTransferRepository = new GreenTransferRepository();
        }
        public ActionResult Index()
        {
            LastRoute = new RouteInfo() { Controller = "GreenTransfer", Action = "Index" };
            ViewBag.Crop = CropGreenTransfer;
            return View();
        }
        private List<CropReceiving> CropGreenTransfer
        {
            get
            {
                List<CropReceiving> crop = _greenTransferRepository.GetCorp();
                return crop;
            }
        }

        //--------------------- Show all Green Transfer by Crop on Index --------------------------------------------
        [Authorize]
        public ActionResult JsonGreenTMaster(Dictionary<string, string> param, string sortName = "transfer_doc_no", string sortOrder = "asc", int offset = 0, int limit = 10)
        {
            string scrop = param["scrop"];
            var GreenTransferMaster = _greenTransferRepository.GetJsonGreenTMaster(scrop).Select(a => new JsonGreenTransferMasterModels()
            {
                Transfer_Doc_No = a.Transfer_Doc_No,
                Transfer_No = a.Transfer_No,
                Transfer_Doc_Date = Convert.ToString(a.Transfer_Doc_Date),
                Crop = Convert.ToString(a.Crop),
                Type = a.Type,
                Season = a.Season,
                Company = a.Company
            });
            var serializer = new JavaScriptSerializer() { MaxJsonLength = Int32.MaxValue };
            return Json(new { total = GreenTransferMaster.Count(), rows = serializer.Deserialize<object>(GreenTransferMaster.Any() ? "[" + String.Join(",", GreenTransferMaster.Select(a => a.ToString())) + "]" : "[]") }, JsonRequestBehavior.AllowGet);
        }

        //Show only New Green Transfer
        [Authorize]
        public ActionResult JsonAddingGreenTMaster(Dictionary<string, string> param,string sortName = "transfer_doc_no", string sortOrder = "asc", int offset = 0, int limit = 10)
        {
            int total = 0;
            int scrop = Int16.Parse(param["scrop"]);
            var GreenTransferMaster = _greenTransferRepository.GetJsonAddGreenTMaster(scrop);
            var filterCrop = GreenTransferMaster.Where(a => a.Crop == scrop).OrderBy(a => a.Transfer_No).ToList();
            var GreenTransferlist = filterCrop.Select(a => new JsonGreenTransferMasterModels()
            {
                Transfer_Doc_No = a.Transfer_Doc_No,
                Transfer_No = a.Transfer_No,
                Transfer_Doc_Date = Convert.ToString(a.Transfer_Doc_Date),
                Crop = Convert.ToString(a.Crop),
                Type = a.Type,
                Season = a.Season,
                Company = a.Company
            });
            var serializer = new JavaScriptSerializer() { MaxJsonLength = Int32.MaxValue };
            return Json(new { total = total, rows = serializer.Deserialize<object>(GreenTransferlist.Any() ? "[" + String.Join(",", GreenTransferlist.Select(r => r.ToString())) + "]" : "[]") }, JsonRequestBehavior.AllowGet);
        }

        //--------------------------------------- Edit Green --------------------------------------------------------
        [Authorize]
        public ActionResult EditGreenTransfer(string TFNo)
        {
            LastRoute = new RouteInfo() { Controller = "GreenTransfer", Action = "EditGreenTransfer" };
            if (TFNo == "")
                return null;

            MatIsGreenTransferInfo greenTMatIS = _greenTransferRepository.GetGreenTCalculate(TFNo);
            if (greenTMatIS == null)
                return View("NotFoundInfo");

            GreenTransferModels existedGreenT = new GreenTransferModels()
            {
                TransferNo = greenTMatIS.TransferNo,
                DocumentNo = greenTMatIS.DocumentNo,
                DocumentDate = greenTMatIS.DocumentDate,
                CompanyCode = greenTMatIS.CompanyCode,
                Crop = greenTMatIS.Crop,
                Type = greenTMatIS.Type,
                Season = greenTMatIS.Season,
                TotalWeight = greenTMatIS.TotalWeight,
                AveragePrice = greenTMatIS.AveragePrice,
                TotalAmount = greenTMatIS.TotalAmount
            };
            return View("EditGreenTransfer", existedGreenT);
        }

        [Authorize]
        public ActionResult JsonGreenTransferDetails(Dictionary<string, string> param, string sortName = "bale_no", string sortOrder = "asc", int offset = 0, int limit = 10)
        {
            int total = 0;
            string TFNo = param["TFNo"];
            List<GreenTransferDetails> GreenTransferDetails = new List<GreenTransferDetails>();
            var JsonGreenTDetails = new List<JsonGreenTransferDetailsModels>();
            var serializer = new JavaScriptSerializer() { MaxJsonLength = Int32.MaxValue };

            GreenTransferDetails = _greenTransferRepository.GetJsonGreenTDetails(TFNo);

            foreach (var r in GreenTransferDetails.OrderBy(r => Int32.Parse(r.BaleNo)))
            {
                JsonGreenTDetails.Add(new JsonGreenTransferDetailsModels()
                {
                    BaleNo = r.BaleNo,
                    Supplier = r.Supplier,
                    GreenGrade = r.GreenGrade,
                    FromClassified = r.FromClassify,
                    ToClassified = r.ToClassify,
                    Weight = Double.Parse(r.Weight).ToString("#,##0.00"),
                    UnitPrice = Double.Parse(r.UnitPrice).ToString("#,##0.00"),
                    Amount = Double.Parse(r.Amount).ToString("#,##0.00")
                });
            }
            return Json(new { total = total, rows = serializer.Deserialize<object>(JsonGreenTDetails.Any() ? "[" + String.Join(",", JsonGreenTDetails.Select(a => a.ToString())) + "]" : "[]") }, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DeleteGreenTransfer(string TFDocNo)
        {
            _greenTransferRepository.DeleteGreenT(TFDocNo);
            return View("UpdateSuccess");
        }

        [Authorize]
        [HttpPost]
        public ActionResult EditGreenTransfer(GreenTransferModels GTModel)
        {
            string NewDocumentNo;
            //Generate Document No
            NewDocumentNo = _greenTransferRepository.GetDocumentNo(GTModel.Crop);

            if (!String.IsNullOrEmpty(NewDocumentNo))
                GTModel.DocumentNo = NewDocumentNo;

            _greenTransferRepository.AddGreenTransfer(
                GTModel.DocumentNo,
                GTModel.DocumentDate,
                GTModel.TransferNo,
                GTModel.Crop,
                GTModel.CompanyCode,
                GTModel.Type,
                GTModel.Season,
                CurrentUser.C_NAME
                );
            return View("UpdateSuccess");
        }

    }
}