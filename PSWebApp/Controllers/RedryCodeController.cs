﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using PSWebApp.Repository;
using PSWebApp.Models;
using PSWebApp.Models.JsonModels;
using PSWebApp.Model;

namespace PSWebApp.Controllers
{
    public class RedryCodeController : CommonControllerBase
    {
        private RedryCodeRepository _redryCodeRepository;
        public RedryCodeController()
        {
            _redryCodeRepository = new RedryCodeRepository();
        }
        // GET: RedryCode
        [Authorize]
        [HttpGet]
        public ActionResult Index()
        {
            LastRoute = new RouteInfo() { Controller = "RedryCode", Action = "Index" };
            ViewBag.Crop = CropRegrade;
            return View();
        }
        private List<CropReceiving> CropRegrade
        {
            get
            {
                List<CropReceiving> crop = _redryCodeRepository.GetCorp();
                return crop;
            }
        }

        //--------------------- Show all Regrade by Crop on Index --------------------------------------------
        [Authorize]
        public ActionResult JsonRedryCode(Dictionary<string, string> param, string sortName = "packedgrade", string sortOrder = "asc", int offset = 0, int limit = 10)
        {
            string scrop = param["scrop"];
            var RedryCodeMaster = _redryCodeRepository.GetJsonRedryCodeMaster(scrop).Select(a => new JsonRedryTransCodeModel()
            {
                PackedGrade = a.PackedGrade,
                Type = a.Type,
                Customer = a.Customer,
                Form = a.Form,
                NetDef = a.Netdef.ToString("#,##0"),
                TareDef = a.Taredef.ToString("#,##0.00"),
                GrossDef = a.Grossdef.ToString("#,##0.00"),
                Gep = a.Gep.ToString(),
                GepBaht = a.GepBaht.ToString("#,##0.0000"),
                RedryCode = a.RedryCode,
                RedryCharge = a.RedryCharge.ToString("#,##0.00"),
                Transport = a.Transport.ToString("#,##0.00"),
                Crop = a.Crop.ToString(),
                TransportCode = a.TranspostCode,
                Price = a.Price.ToString("#,##0.00")
            });
            var serializer = new JavaScriptSerializer() { MaxJsonLength = Int32.MaxValue };
            return Json(new { total = RedryCodeMaster.Count(), rows = serializer.Deserialize<object>(RedryCodeMaster.Any() ? "[" + String.Join(",", RedryCodeMaster.Select(a => a.ToString())) + "]" : "[]") }, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult JsonRedryCodeByProduct(Dictionary<string, string> param, string sortName = "packedgrade", string sortOrder = "asc", int offset = 0, int limit = 10)
        {
            string scrop = param["scrop"];
            var RedryCodeByProduct = _redryCodeRepository.GetJsonRedryCodeByProduct(scrop).Select(a => new JsonRedryTransCodeModel()
            {
                PackedGrade = a.PackedGrade,
                Type = a.Type,
                Customer = a.Customer,
                Form = a.Form,
                NetDef = a.Netdef.ToString("#,##0"),
                TareDef = a.Taredef.ToString("#,##0.00"),
                GrossDef = a.Grossdef.ToString("#,##0.00"),
                Gep = a.Gep.ToString(),
                GepBaht = a.GepBaht.ToString("#,##0.0000"),
                RedryCode = a.RedryCode,
                RedryCharge = a.RedryCharge.ToString("#,##0.00"),
                Transport = a.Transport.ToString("#,##0.00"),
                Crop = a.Crop.ToString(),
                TransportCode = a.TranspostCode,
                Price = a.Price.ToString("#,##0.00")
            });
            var serializer = new JavaScriptSerializer() { MaxJsonLength = Int32.MaxValue };
            return Json(new { total = RedryCodeByProduct.Count(), rows = serializer.Deserialize<object>(RedryCodeByProduct.Any() ? "[" + String.Join(",", RedryCodeByProduct.Select(a => a.ToString())) + "]" : "[]") }, JsonRequestBehavior.AllowGet);
        }

        //--------------------------------------- Edit Redry Code --------------------------------------------------------
        [Authorize]
        public ActionResult EditRedryCode(int Crop, string PackGrade)
        {
            LastRoute = new RouteInfo() { Controller = "RedryCode", Action = "EditRedryCode" };
            if (PackGrade == "")
                return null;

            RedryCodeInfo redryCode = _redryCodeRepository.GetRedryCodeExisted(Crop, PackGrade);
            if (redryCode == null)
                return View("NotFoundInfo");
            //Get all Transport and Redry Code
            ViewBag.TransportCode = _redryCodeRepository.GetTransportation(Crop);
            ViewBag.RedryCode = _redryCodeRepository.GetRedrying(Crop);

            RedryCodeModels existedGreenT = new RedryCodeModels()
            {
                Packedgrade = redryCode.PackedGrade,
                Type = redryCode.Type,
                Customer = redryCode.Customer,
                From = redryCode.Form,
                NetDef = Decimal.Parse(redryCode.Netdef.ToString("#,##0")),
                TareDef = Decimal.Parse(redryCode.Taredef.ToString("#,##0.00")),
                GrossDef = Decimal.Parse(redryCode.Grossdef.ToString("#,##0.00")),
                Gep = redryCode.Gep,
                GepBaht = Decimal.Parse(redryCode.GepBaht.ToString("#,##0.0000")),
                RedryCode = redryCode.RedryCode,
                RedryCharge = Decimal.Parse(redryCode.RedryCharge.ToString("#,##0.00")),
                Transport = Decimal.Parse(redryCode.Transport.ToString("#,##0.00")),
                Crop = redryCode.Crop,
                TransportCode = redryCode.TranspostCode,
                Price = Decimal.Parse(redryCode.Price.ToString("#,##0.00"))
            };
            return View("EditRedryCode", existedGreenT);
        }

        [Authorize]
        [HttpPost]
        public ActionResult EditRedryCode(RedryCodeModels redryCodeModel)
        {
            //string NewDocumentNo;
            ////Generate Receiving Document No
            //NewDocumentNo = _receivingRepository.GetDocumentNo(recModel.Crop);
            //if (!String.IsNullOrEmpty(NewDocumentNo))
            //    recModel.DocumentNo = NewDocumentNo;

            //if (String.IsNullOrEmpty(recModel.RecordType))
            //    recModel.RecordType = "B";
            ////Tricker generate Buying no
            //recModel.BuyingDocNo = "";

            //_receivingRepository.AddReceiving(
            //    recModel.DocumentNo,
            //    recModel.DocumentDate,
            //    recModel.ReceivingNo,
            //    recModel.BuyingDocNo,
            //    recModel.Crop,
            //    recModel.CompanyCode,
            //    recModel.Type,
            //    recModel.Season,
            //    recModel.CurerCode,
            //    recModel.CurerName,
            //    recModel.Area,
            //    recModel.GreenPriceCode,
            //    recModel.RecordType,
            //    CurrentUser.C_NAME
            //    );
            return View("UpdateSuccess");
        }
    }
}