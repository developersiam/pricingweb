﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using PSWebApp.Repository;
using PSWebApp.Models;
using PSWebApp.Models.JsonModels;
using PSWebApp.Model;

namespace PSWebApp.Controllers
{
    public class RegradeController : CommonControllerBase
    {
        private RegradeRepository _regradeRepository;

        public RegradeController()
        {
            _regradeRepository = new RegradeRepository();
        }
        // GET: Regrade
        public ActionResult Index()
        {
            LastRoute = new RouteInfo() { Controller = "Regrade", Action = "Index" };
            ViewBag.Crop = CropRegrade;
            return View();
        }
        private List<CropReceiving> CropRegrade
        {
            get
            {
                List<CropReceiving> crop = _regradeRepository.GetCorp();
                return crop;
            }
        }
        //--------------------- Show all Regrade by Crop on Index --------------------------------------------
        [Authorize]
        public ActionResult JsonRegradeMaster(Dictionary<string, string> param, string sortName = "regrade_doc_no", string sortOrder = "asc", int offset = 0, int limit = 10)
        {
            string scrop = param["scrop"];
            var RegradeMaster = _regradeRepository.GetJsonRegradeMaster(scrop).Select(a => new JsonRegradeMasterModels()
            {
                Regrade_Doc_No = a.Regrade_Doc_No,
                Regrade_No = a.Regrade_No,
                Regrade_Doc_Date = Convert.ToString(a.Regrade_Doc_Date),
                Crop = Convert.ToString(a.Crop),
                Type = a.Type,
                Season = a.Season,
                Company = a.Company
            });
            var serializer = new JavaScriptSerializer() { MaxJsonLength = Int32.MaxValue };
            return Json(new { total = RegradeMaster.Count(), rows = serializer.Deserialize<object>(RegradeMaster.Any() ? "[" + String.Join(",", RegradeMaster.Select(a => a.ToString())) + "]" : "[]") }, JsonRequestBehavior.AllowGet);
        }

        //Show only New Hand Strip
        [Authorize]
        public ActionResult JsonAddingRegradeMaster(Dictionary<string, string> param, string sortName = "hand_doc_no", string sortOrder = "asc", int offset = 0, int limit = 10)
        {
            int total = 0;
            int scrop = Int16.Parse(param["scrop"]);
            var RegradeMaster = _regradeRepository.GetJsonAddRegradeMaster(scrop);
            var filterCrop = RegradeMaster.Where(a => a.Crop == scrop).OrderBy(a => a.Regrade_No).ToList();
            var Regradelist = filterCrop.Select(a => new JsonRegradeMasterModels()
            {
                Regrade_Doc_No = a.Regrade_Doc_No,
                Regrade_No = a.Regrade_No,
                Regrade_Doc_Date = Convert.ToString(a.Regrade_Doc_Date),
                Crop = Convert.ToString(a.Crop),
                Type = a.Type,
                Season = a.Season,
                Company = a.Company
            });
            var serializer = new JavaScriptSerializer() { MaxJsonLength = Int32.MaxValue };
            return Json(new { total = total, rows = serializer.Deserialize<object>(Regradelist.Any() ? "[" + String.Join(",", Regradelist.Select(r => r.ToString())) + "]" : "[]") }, JsonRequestBehavior.AllowGet);
        }

        //--------------------------------------- Edit Regrade --------------------------------------------------------
        [Authorize]
        public ActionResult EditRegrade(string RGNo)
        {
            LastRoute = new RouteInfo() { Controller = "Regrade", Action = "EditRegrade" };
            if (RGNo == "")
                return null;

            MatIsRegradeInfo regradeMatIS = _regradeRepository.GetRegradeCalculate(RGNo);
            if (regradeMatIS == null)
                return View("NotFoundInfo");

            RegradeModels existedReg = new RegradeModels()
            {
                RegradeNo = regradeMatIS.RegradeNo,
                DocumentNo = regradeMatIS.DocumentNo,
                DocumentDate = regradeMatIS.DocumentDate,
                CompanyCode = regradeMatIS.CompanyCode,
                Crop = regradeMatIS.Crop,
                Type = regradeMatIS.Type,
                Season = regradeMatIS.Season,
                TotalWeight = regradeMatIS.TotalWeight,
                AveragePrice = regradeMatIS.AveragePrice,
                TotalAmount = regradeMatIS.TotalAmount
            };
            return View("EditRegrade", existedReg);
        }

        [Authorize]
        public ActionResult JsonRegradeDetails(Dictionary<string, string> param, string sortName = "bale_no", string sortOrder = "asc", int offset = 0, int limit = 10)
        {
            int total = 0;
            string RGNo = param["RGNo"];
            List<RegradeDetails> RegradeDetails = new List<RegradeDetails>();
            var JsonRegDetails = new List<JsonRegradeDetailsModels>();
            var serializer = new JavaScriptSerializer() { MaxJsonLength = Int32.MaxValue };

            RegradeDetails = _regradeRepository.GetJsonRegradeDetails(RGNo);

            foreach (var r in RegradeDetails.OrderBy(r => Int32.Parse(r.BaleNo)))
            {
                JsonRegDetails.Add(new JsonRegradeDetailsModels()
                {
                    BaleNo = r.BaleNo,
                    GreenGrade = r.GreenGrade,
                    Classified = r.Classified,
                    FlagIO = r.FlagIO,
                    Weight = Double.Parse(r.Weight).ToString("#,##0.00"),
                    UnitPrice = Double.Parse(r.UnitPrice).ToString("#,##0.00"),
                    Amount = Double.Parse(r.Amount).ToString("#,##0.00")
                });
            }
            return Json(new { total = total, rows = serializer.Deserialize<object>(JsonRegDetails.Any() ? "[" + String.Join(",", JsonRegDetails.Select(a => a.ToString())) + "]" : "[]") }, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DeleteRegrade(string RGDocNo)
        {
            _regradeRepository.DeleteRegrade(RGDocNo);
            return View("UpdateSuccess");
        }

        [Authorize]
        [HttpPost]
        public ActionResult EditRegrade(RegradeModels RGModel)
        {
            string NewDocumentNo;
            //Generate Document No
            NewDocumentNo = _regradeRepository.GetDocumentNo(RGModel.Crop);

            if (!String.IsNullOrEmpty(NewDocumentNo))
                RGModel.DocumentNo = NewDocumentNo;

            _regradeRepository.AddRegrade(
                RGModel.DocumentNo,
                RGModel.DocumentDate,
                RGModel.RegradeNo,
                RGModel.Crop,
                RGModel.CompanyCode,
                RGModel.Type,
                RGModel.Season,
                CurrentUser.C_NAME
                );
            return View("UpdateSuccess");
        }
    }
}