﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using PSWebApp.Repository;
using PSWebApp.Models;
using PSWebApp.Models.JsonModels;
using PSWebApp.Model;
using System.Web.Script.Serialization;

namespace PSWebApp.Controllers
{
    public class ProcessProductController : CommonControllerBase
    {
        private ProcessProductRepository _processproductRepository;

        public ProcessProductController()
        {
            _processproductRepository = new ProcessProductRepository();
        }
        public ActionResult Index()
        {
            LastRoute = new RouteInfo() { Controller = "ProcessProduct", Action = "Index" };
            ViewBag.Crop = CropProduction;
            return View();
        }
        private List<CropReceiving> CropProduction
        {
            get
            {
                List<CropReceiving> crop = _processproductRepository.GetProcessingCorp();
                return crop;
            }
        }
        //--------------------- Show all Processing Product by Crop on Index --------------------------------------------
        [Authorize]
        public ActionResult JsonProcessProduct(Dictionary<string, string> param, string sortName = "processing_no", string sortOrder = "asc", int offset = 0, int limit = 10)
        {
            int scrop = Int16.Parse(param["scrop"]);
            var ProcessProductMaster = _processproductRepository.GetJsonProcessProduct(scrop).Select(a => new JsonProcessProductMasterModel()
            {
                DocNo = a.Doc_No,
                ProcessingP_No = a.ProcessingP_No,
                Doc_Date = a.Doc_Date.ToString(),
                Crop = a.Crop.ToString(),
                Type = a.Type,
                Season = a.Season,
                Company = a.Company

            });
            var serializer = new JavaScriptSerializer() { MaxJsonLength = Int32.MaxValue };
            return Json(new { total = ProcessProductMaster.Count(), rows = serializer.Deserialize<object>(ProcessProductMaster.Any() ? "[" + String.Join(",", ProcessProductMaster.Select(a => a.ToString())) + "]" : "[]") }, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult JsonAddingProcessProduct(Dictionary<string, string> param, string sortName = "processingP_no", string sortOrder = "asc", int offset = 0, int limit = 10)
        {
            int scrop = Int16.Parse(param["scrop"]);
            var ProcessProductMaster = _processproductRepository.GetNewJsonProcessProduct();
            var filterCrop = ProcessProductMaster.Where(a => a.Crop == scrop).OrderBy(a => a.ProcessingP_No).ToList();
            var ProcessProductList = filterCrop.Select(a => new JsonProcessProductMasterModel()
            {
                DocNo = a.Doc_No,
                ProcessingP_No = a.ProcessingP_No,
                Doc_Date = a.Doc_Date.ToString(),
                Crop = a.Crop.ToString(),
                Type = a.Type,
                Season = a.Season,
                Company = a.Company
            });
            var serializer = new JavaScriptSerializer() { MaxJsonLength = Int32.MaxValue };
            return Json(new { total = ProcessProductList.Count(), rows = serializer.Deserialize<object>(ProcessProductList.Any() ? "[" + String.Join(",", ProcessProductList.Select(a => a.ToString())) + "]" : "[]") }, JsonRequestBehavior.AllowGet);
        }
        //--------------------------------------- Edit Process Product --------------------------------------------------------
        [Authorize]
        public ActionResult EditProcessProduct(string ProcessingP_no)
        {
            LastRoute = new RouteInfo() { Controller = "ProcessProduct", Action = "EditProcessProduct" };
            if (ProcessingP_no == "")
                return null;

            PDProcessProductMasterInfo ProcessProductPD = _processproductRepository.GetProcessProductCalculate(ProcessingP_no);

            ProcessProductModels existedProcessProduct = new ProcessProductModels()
            {
                Crop = ProcessProductPD.Crop,
                Doc_No = ProcessProductPD.Doc_No,
                ProcessingP_No = ProcessProductPD.ProcessingR_No,
                Doc_Date = ProcessProductPD.Doc_Date,
                Company = ProcessProductPD.Company,
                Type = ProcessProductPD.Type,
                Season = ProcessProductPD.Season,
                Total_Amt = ProcessProductPD.Total_Cost.ToString("#,##0.00"),
                Total_Weight = ProcessProductPD.Total_Weight.ToString("#,##0.00"),
                Average_Price = ProcessProductPD.Average_Price.ToString("#,##0.00")
            };
            return View("EditProcessProduct", existedProcessProduct);
        }

        [Authorize]
        [HttpPost]
        public ActionResult EditProcessProduct(ProcessProductModels PPModel)
        {
            string NewDocumentNo;
            //Generate Receiving Document No
            NewDocumentNo = _processproductRepository.GetDocumentNo(PPModel.Crop);
            if (!String.IsNullOrEmpty(NewDocumentNo))
                PPModel.Doc_No = NewDocumentNo;

            _processproductRepository.AddProcessProduct(PPModel.Doc_No,
                PPModel.Doc_Date,
                PPModel.ProcessingP_No,
                PPModel.Crop,
                PPModel.Company,
                PPModel.Type,
                PPModel.Season,
                CurrentUser.C_NAME);
            return View("UpdateSuccess");
        }

        [Authorize]
        public ActionResult JsonProcessingProductDetails(Dictionary<string, string> param, string sortName = "packed_grade", string sortOrder = "asc", int offset = 0, int limit = 10)
        {
            int total = 0;
            string unit_p = "";
            string ProcessNo = param["ProcessNo"];

            List<ProcessProductDetails> PPDetails = new List<ProcessProductDetails>();

            var JsonPPDetails = new List<JsonProcessProductDetailsModel>();
            var serializer = new JavaScriptSerializer() { MaxJsonLength = Int32.MaxValue };

            PPDetails = _processproductRepository.GetJsonProcessPDetails(ProcessNo);

            foreach (var r in PPDetails.OrderBy(r => r.PackedGrade))
            {
                if (r.UnitPrice != "")
                    unit_p = Double.Parse(r.UnitPrice).ToString("#,##0.00");
                else
                    unit_p = Double.Parse("0").ToString("#,##0.00");

                JsonPPDetails.Add(new JsonProcessProductDetailsModel()
                {
                    Packed_grade = r.PackedGrade,
                    Weight = Double.Parse(r.Weight).ToString("#,##0.00"),
                    UnitPrice = unit_p,
                    Amount = Double.Parse(r.Amount).ToString("#,##0.00"),
                    Packing_mat = r.PackingMaterial,
                    Trans_charge = Double.Parse(r.Trans_Charge).ToString("#,##0.00"),
                    Total_cost = Double.Parse(r.ToTalCost).ToString("#,##0.00")
                });
            }
            return Json(new { total = total, rows = serializer.Deserialize<object>(JsonPPDetails.Any() ? "[" + String.Join(",", JsonPPDetails.Select(a => a.ToString())) + "]" : "[]") }, JsonRequestBehavior.AllowGet);
        }
        [Authorize]
        public ActionResult DeleteProcessProduct(string PPDocNo)
        {
            _processproductRepository.DeleteProcessProduct(PPDocNo);
            return View("UpdateSuccess");
        }


    }
}