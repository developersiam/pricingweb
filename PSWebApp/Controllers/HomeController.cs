﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PSWebApp.Controllers
{
    public class HomeController : CommonControllerBase
    {
        [Authorize]
        public ActionResult Index()
        {
            LastRoute = new RouteInfo() { Controller = "Home", Action = "Index" };
            return View("Index", CurrentUser);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [Authorize]
        [HttpGet]
        public JsonResult ContinueSession()
        {
            return null;
        }
    }
}