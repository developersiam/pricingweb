﻿using System;

namespace PSWebApp.Models
{
    [Serializable]
    public class RedryChargeModels
    {
        public int Crop { get; set; }
        public string Type { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string UnitCost { get; set; }
    }
}