﻿using System;

namespace PSWebApp.Models
{
    [Serializable]
    public class ShippingProductModels
    {
        public string Doc_No { get; set; }
        public string Shipping_No { get; set; }
        public int Crop { get; set; }
        public DateTime Doc_Date { get; set; }
        public string Type { get; set; }
        public string TypeRB { get; set; }
        public string Total_Amt { get; set; }
        public string Total_Weight { get; set; }
        public string Average_Price { get; set; }
    }
}