﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSWebApp.Models
{
    [Serializable]
    public class ProcessProductModels
    {
        public string Doc_No { get; set; }
        public string ProcessingP_No { get; set; }
        public int Crop { get; set; }
        public DateTime Doc_Date { get; set; }
        public string Company { get; set; }
        public string Type { get; set; }
        public string Season { get; set; }
        public string Total_Amt { get; set; }
        public string Total_Weight { get; set; }
        public string Average_Price { get; set; }
    }
}