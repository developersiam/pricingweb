﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSWebApp.Models
{
    [Serializable]
    public class GreenTransferModels
    {
        public string TransferNo { get; set; }
        public string DocumentNo { get; set; }
        public DateTime DocumentDate { get; set; }
        public string CompanyCode { get; set; }
        public int Crop { get; set; }
        public string Type { get; set; }
        public string Season { get; set; }
        public string TotalWeight { get; set; }
        public string AveragePrice { get; set; }
        public string TotalAmount { get; set; }
    }
}