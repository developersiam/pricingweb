﻿using System;

namespace PSWebApp.Models
{
    [Serializable]
    public class ExchangeRateModels
    {
        public DateTime EffectiveDate { get; set; }
        public decimal ExchangeRate { get; set; }
    }
}