﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSWebApp.Models
{
    [Serializable]
    public class ProcessReadyModels
    {
        public string Doc_No { get; set; }
        public string ProcessingR_No { get; set; }
        public int Crop { get; set; }
        public DateTime Doc_Date { get; set; }
        public string Packed_Grade { get; set; }
        public string Type { get; set; }
        public string Season { get; set; }
        public decimal Packed_Weight { get; set; }
        public decimal Weight { get; set; }
        public decimal Redry_Charge { get; set; }
        public decimal Pack_Mat { get; set; }
        public decimal Transportation { get; set; }
        public string Total_Amt { get; set; }
        public string Total_Weight { get; set; }
        public string Average_Price { get; set; }
    }
}