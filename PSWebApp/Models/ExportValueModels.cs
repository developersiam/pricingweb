﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSWebApp.Models
{
    [Serializable]
    public class ExportValueModels
    {
        public int Crop { get; set; }
        public string PackedGrade { get; set; }
        public string Customer { get; set; }
        public string CustomerGrade { get; set; }
        public decimal Value { get; set; }
    }
    [Serializable]
    public class PackedGradeCustomerModels
    {
        public string PackedGrade { get; set; }
        public string CustomerName { get; set; }
        public string CustomerGrade { get; set; }
    }

    [Serializable]
    public class PackedGradeUnitWeightModels
    {
        public string UnitPrice { get; set; }
        public string AvgWeight { get; set; }
    }
}