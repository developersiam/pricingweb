﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSWebApp.Models
{
    [Serializable]
    public class RedryCodeModels
    {
        public string Packedgrade { get; set; }
        public string Type { get; set; }
        public string Customer { get; set; }
        public string From { get; set; }
        public decimal NetDef { get; set; }
        public int Crop { get; set; }
        public decimal TareDef { get; set; }
        public decimal GrossDef { get; set; }
        public decimal Gep { get; set; }
        public decimal GepBaht { get; set; }
        public string RedryCode { get; set; }
        public decimal RedryCharge { get; set; }
        public decimal Transport { get; set; }
        public string TransportCode { get; set; }
        public decimal Price { get; set; }

    }
}