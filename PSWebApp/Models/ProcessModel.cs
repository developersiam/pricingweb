﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSWebApp.Models
{
    public class ProcessModel
    {
        public string C_PROG_ID { get; set; }
        public string C_PROCESS_DOC_NO { get; set; }
        public string C_PROCESS_NO { get; set; }
        public DateTime D_PROCESS_DOC_DATE { get; set; }
        public int N_CROP { get; set; }
        public string C_TYPE { get; set; }
        public string C_SEASON { get; set; }
        public string C_PACKED_GRADE { get; set; }
        public string C_PACK_MAT_CODE { get; set; }
        public decimal N_PACK_MAT_CHARGE { get; set; }
        public string C_TRAN_CHARGE_CODE { get; set; }
        public decimal N_TRAN_CHARGE { get; set; }
        public string C_REDRY_CHARGE_CODE { get; set; }

    }
}