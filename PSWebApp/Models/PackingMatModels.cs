﻿using System;

namespace PSWebApp.Models
{
    [Serializable]
    public class PackingMatModels
    {
        public int Crop { get; set; }
        public string PackingMat { get; set; }
        public decimal Price { get; set; }
    }
}