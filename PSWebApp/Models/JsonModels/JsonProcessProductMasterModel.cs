﻿using System;

namespace PSWebApp.Models.JsonModels
{
    public class JsonProcessProductMasterModel
    {
        public string _doc_no { get; set; }
        public string DocNo
        {
            set { _doc_no = value; }
            private get { return String.Format("\"doc_no\": \"{0}\"", _doc_no); }
        }
        public string _processingP_no { get; set; }
        public string ProcessingP_No
        {
            set { _processingP_no = value; }
            private get { return String.Format("\"processingP_no\": \"{0}\"", _processingP_no); }
        }
        public string _crop { get; set; }
        public string Crop
        {
            set { _crop = value; }
            private get { return String.Format("\"crop\": \"{0}\"", _crop); }
        }
        public string _doc_date { get; set; }
        public string Doc_Date
        {
            set { _doc_date = value; }
            private get { return String.Format("\"doc_date\": \"{0}\"", _doc_date); }
        }
        public string _company { get; set; }
        public string Company
        {
            set { _company = value; }
            private get { return String.Format("\"company\": \"{0}\"", _company); }
        }
        public string _type { get; set; }
        public string Type
        {
            set { _type = value; }
            private get { return String.Format("\"type\": \"{0}\"", _type); }
        }
        public string _season { get; set; }
        public string Season
        {
            set { _season = value; }
            private get { return String.Format("\"season\": \"{0}\"", _season); }
        }
 
        public override string ToString()
        {
            return String.Format("{{{0}, {1}, {2}, {3}, {4}, {5}, {6}}}", DocNo, ProcessingP_No, Crop, Doc_Date, Company, Type, Season);
        }
    }
}