﻿using System;

namespace PSWebApp.Models.JsonModels
{
    public class JsonReceivingDetailsModels
    {
        public string _bale_no { get; set; }
        public string BaleNo
        {
            set { _bale_no = value; }
            private get { return String.Format("\"bale_no\": \"{0}\"", _bale_no); }
        }
        public string _green_grade { get; set; }
        public string GreenGrade
        {
            set { _green_grade = value; }
            private get { return String.Format("\"green_grade\": \"{0}\"", _green_grade); }
        }
        public string _classified_grade { get; set; }
        public string ClassifiedGrade
        {
            set { _classified_grade = value; }
            private get { return String.Format("\"classified_grade\": \"{0}\"", _classified_grade); }
        }
        public string _buy_weight { get; set; }
        public string BuyWeight
        {
            set { _buy_weight = value; }
            private get { return String.Format("\"buy_weight\": \"{0}\"", _buy_weight); }
        }
        public string _weight { get; set; }
        public string Weight
        {
            set { _weight = value; }
            private get { return String.Format("\"weight\": \"{0}\"", _weight); }
        }
        public string _unit_price { get; set; }
        public string UnitPrice
        {
            set { _unit_price = value; }
            private get { return String.Format("\"unit_price\": \"{0}\"", _unit_price); }
        }
        public string _t_charge { get; set; }
        public string TCharge
        {
            set { _t_charge = value; }
            private get { return String.Format("\"t_charge\": \"{0}\"", _t_charge); }
        }
        public string _total_unit_price { get; set; }
        public string TotalUnitPrice
        {
            set { _total_unit_price = value; }
            private get { return String.Format("\"total_unit_price\": \"{0}\"", _total_unit_price); }
        }
        public string _amount { get; set; }
        public string Amount
        {
            set { _amount = value; }
            private get { return String.Format("\"amount\": \"{0}\"", _amount); }
        }
        public string _fcv_class { get; set; }
        public string FCVClass
        {
            set { _fcv_class = value; }
            private get { return String.Format("\"fcv_class\": \"{0}\"", _fcv_class); }
        }
        public override string ToString()
        {
            return String.Format("{{{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}}}", BaleNo, GreenGrade, ClassifiedGrade, BuyWeight, Weight, UnitPrice, TCharge, TotalUnitPrice, Amount, FCVClass);
        }
    }
}