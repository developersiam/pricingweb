﻿using System;

namespace PSWebApp.Models.JsonModels
{
    public class JsonGreenTransferMasterModels
    {
        public string _trans_doc_no { get; set; }
        public string Transfer_Doc_No
        {
            set { _trans_doc_no = value; }
            private get { return String.Format("\"trans_doc_no\": \"{0}\"", _trans_doc_no); }
        }

        public string _trans_no { get; set; }
        public string Transfer_No
        {
            set { _trans_no = value; }
            private get { return string.Format("\"trans_no\": \"{0}\"", _trans_no); }
        }
        public string _crop { get; set; }
        public string Crop
        {
            set { _crop = value; }
            private get { return String.Format("\"crop\": \"{0}\"", _crop); }
        }

        public string _trans_doc_date { get; set; }
        public string Transfer_Doc_Date
        {
            set { _trans_doc_date = value; }
            private get { return string.Format("\"trans_doc_date\": \"{0}\"", _trans_doc_date); }
        }

        public string _company { get; set; }
        public string Company
        {
            set { _company = value; }
            private get { return string.Format("\"company\": \"{0}\"", _company); }
        }
        public string _type { get; set; }
        public string Type
        {
            set { _type = value; }
            private get { return String.Format("\"type\": \"{0}\"", _type); }
        }

        public string _season { get; set; }
        public string Season
        {
            set { _season = value; }
            private get { return string.Format("\"season\": \"{0}\"", _season); }
        }

        public override string ToString()
        {
            return String.Format("{{{0}, {1}, {2}, {3}, {4}, {5}, {6}}}", Transfer_Doc_No, Transfer_No, Crop, Transfer_Doc_Date, Company, Type, Season);
        }

    }
}