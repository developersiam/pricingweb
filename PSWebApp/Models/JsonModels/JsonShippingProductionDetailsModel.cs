﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSWebApp.Models.JsonModels
{
    public class JsonShippingProductionDetailsModel
    {
        public string _company { get; set; }
        public string Company
        {
            set { _company = value; }
            private get { return String.Format("\"company\": \"{0}\"", _company); }
        }
        public string _packed_grade { get; set; }
        public string Packed_grade
        {
            set { _packed_grade = value; }
            private get { return String.Format("\"packed_grade\": \"{0}\"", _packed_grade); }
        }
        public string _case_no { get; set; }
        public string Case_no
        {
            set { _case_no = value; }
            private get { return String.Format("\"case_no\": \"{0}\"", _case_no); }
        }
        public string _weight { get; set; }
        public string Weight
        {
            set { _weight = value; }
            private get { return String.Format("\"weight\": \"{0}\"", _weight); }
        }
        public string _unit_cost { get; set; }
        public string Unit_cost
        {
            set { _unit_cost = value; }
            private get { return String.Format("\"unit_cost\": \"{0}\"", _unit_cost); }
        }

        public string _amount { get; set; }
        public string Amount
        {
            set { _amount = value; }
            private get { return String.Format("\"amount\": \"{0}\"", _amount); }
        }

        public override string ToString()
        {
            return String.Format("{{{0}, {1}, {2}, {3}, {4}, {5}}}", Company , Packed_grade, Case_no, Weight, Unit_cost, Amount);
        }
    }
}