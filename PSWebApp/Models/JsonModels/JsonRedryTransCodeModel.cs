﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSWebApp.Models.JsonModels
{
    public class JsonRedryTransCodeModel
    {
        public string _packed_grade { get; set; }
        public string PackedGrade
        {
            set { _packed_grade = value; }
            private get { return String.Format("\"packed_grade\": \"{0}\"", _packed_grade); }
        }
        public string _type { get; set; }
        public string Type
        {
            set { _type = value; }
            private get { return String.Format("\"type\": \"{0}\"", _type); }
        }
        public string _customer { get; set; }
        public string Customer
        {
            set { _customer = value; }
            private get { return String.Format("\"customer\": \"{0}\"", _customer); }
        }

        public string _form { get; set; }
        public string Form
        {
            set { _form = value; }
            private get { return String.Format("\"form\": \"{0}\"", _form); }
        }
        public string _net_def { get; set; }
        public string NetDef
        {
            set { _net_def = value; }
            private get { return String.Format("\"net_def\": \"{0}\"", _net_def); }
        }
        public string _tare_def { get; set; }
        public string TareDef
        {
            set { _tare_def = value; }
            private get { return String.Format("\"tare_def\": \"{0}\"", _tare_def); }
        }
        public string _gross_def { get; set; }
        public string GrossDef
        {
            set { _gross_def = value; }
            private get { return String.Format("\"gross_def\": \"{0}\"", _gross_def); }
        }
        public string _gep { get; set; }
        public string Gep
        {
            set { _gep = value; }
            private get { return String.Format("\"gep\": \"{0}\"", _gep); }
        }
        public string _gep_baht { get; set; }
        public string GepBaht
        {
            set { _gep_baht = value; }
            private get { return String.Format("\"gep_baht\": \"{0}\"", _gep_baht); }
        }
        public string _redry_code { get; set; }
        public string RedryCode
        {
            set { _redry_code = value; }
            private get { return String.Format("\"redry_code\": \"{0}\"", _redry_code); }
        }

        public string _redry_charge { get; set; }
        public string RedryCharge
        {
            set { _redry_charge = value; }
            private get { return String.Format("\"redry_charge\": \"{0}\"", _redry_charge); }
        }
        public string _transport { get; set; }
        public string Transport
        {
            set { _transport = value; }
            private get { return String.Format("\"transport\": \"{0}\"", _transport); }
        }
        public string _transport_code { get; set; }
        public string TransportCode
        {
            set { _transport_code = value; }
            private get { return String.Format("\"transport_code\": \"{0}\"", _transport_code); }
        }
        public string _price { get; set; }
        public string Price
        {
            set { _price = value; }
            private get { return String.Format("\"price\": \"{0}\"", _price); }
        }
        public string _crop { get; set; }
        public string Crop
        {
            set { _crop = value; }
            private get { return String.Format("\"Crop\": \"{0}\"", _crop); }
        }
        public override string ToString()
        {
            return String.Format("{{{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}}}", PackedGrade, Type, Customer, Form, NetDef, TareDef, GrossDef, Gep, GepBaht, RedryCode, RedryCharge,Transport, TransportCode, Price, Crop);
        }
    }
}