﻿using System;


namespace PSWebApp.Models.JsonModels
{
    public class JsonProcessesModel
    {
        private string _prog_id;
        public string C_PROG_ID
        {
            set { _prog_id = value; }
            private get { return String.Format("\"prog_id\": \"{0}\"", _prog_id); }
        }

        private string _process_doc_no;
        public string C_PROCESS_DOC_NO
        {
            set { _process_doc_no = value; }
            private get { return String.Format("\"process_doc_no\": \"{0}\"", _process_doc_no); }
        }

        private string _process_no;
        public string C_PROCESS_NO
        {
            set { _process_no = value; }
            private get { return String.Format("\"process_no\":\"{0}\"", _process_no); }
        }

        private string _process_doc_date;
        public string D_PROCESS_DOC_DATE
        {
            set { _process_doc_date = value; }
            private get { return String.Format("\"process_doc_date\": \"{0}\"", _process_doc_date); }
        }

        private string _crop;
        public string N_CROP
        {
            set { _crop = value; }
            private get { return String.Format("\"crop\": \"{0}\"", _crop); }
        }

        private string _type;
        public string C_TYPE
        {
            set { _type = value; }
            private get { return String.Format("\"type\": \"{0}\"", _type); }
        }

        private string _season;
        public string C_SEASON
        {
            set { _season = value; }
            private get { return String.Format("\"season\": \"{0}\"", _season); }
        }

        private string _packed_grade;
        public string C_PACKED_GRADE
        {
            set { _packed_grade = value; }
            private get { return String.Format("\"packed_grade\": \"{0}\"", _packed_grade); }
        }

        private string _pack_mat_code;
        public string C_PACK_MAT_CODE
        {
            set { _pack_mat_code = value; }
            private get { return String.Format("\"pack_mat_code\": \"{0}\"", _pack_mat_code); }
        }

        private string _pack_mat_charge;
        public string N_PACK_MAT_CHARGE
        {
            set { _pack_mat_charge = value; }
            private get { return String.Format("\"pack_mat_charge\": \"{0}\"", _pack_mat_charge); }
        }

        private string _tran_charge_code;
        public string C_TRAN_CHARGE_CODE
        {
            set { _tran_charge_code = value; }
            private get { return String.Format("\"tran_charge_code\": \"{0}\"", _tran_charge_code); }
        }

        private string _tran_charge;
        public string N_TRAN_CHARGE
        {
            set { _tran_charge = value; }
            private get { return String.Format("\"tran_charge\": \"{0}\"", _tran_charge); }
        }

        private string _redry_charge_code;
        public string C_REDRY_CHARGE_CODE
        {
            set { _redry_charge_code = value; }
            private get { return String.Format("\"redry_charge_code\": \"{0}\"", _redry_charge_code); }
        }

        private string _redry_charge;
        public string N_REDRY_CHARGE
        {
            set { _redry_charge = value; }
            private get { return String.Format("\"redry_charge\": \"{0}\"", _redry_charge); }
        }

        private string _case_weight;
        public string N_CASE_WEIGHT
        {
            set { _case_weight = value; }
            private get { return String.Format("\"case_weight\": \"{0}\"", _case_weight); }
        }

        private string _packed_weight;
        public string N_PACKED_WEIGHT
        {
            set { _packed_weight = value; }
            private get { return String.Format("\"packed_weight\": \"{0}\"", _packed_weight); }
        }

        private string _cr_by;
        public string C_CR_BY
        {
            set { _cr_by = value; }
            private get { return String.Format("\"cr_by\": \"{0}\"", _cr_by); }
        }

        private string _cr_date;
        public string D_CR_DATE
        {
            set { _cr_date = value; }
            private get { return String.Format("\"cr_date\": \"{0}\"", _cr_date); }
        }

        private string _upd_by;
        public string C_UPD_BY
        {
            set { _upd_by = value; }
            private get { return String.Format("\"upd_by\": \"{0}\"", _upd_by); }
        }

        private string _upd_date;
        public string D_UPD_DATE
        {
            set { _upd_date = value; }
            private get { return String.Format("\"upd_date\": \"{0}\"", _upd_date); }
        }
        public override string ToString()
        {
            return String.Format("{{{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}, {15}, {16}, {17}, {18}, {19}}}", C_PROG_ID, C_PROCESS_DOC_NO, C_PROCESS_NO, D_PROCESS_DOC_DATE, N_CROP, C_TYPE, C_SEASON, C_PACKED_GRADE, C_PACK_MAT_CODE, N_PACK_MAT_CHARGE, C_TRAN_CHARGE_CODE, N_TRAN_CHARGE, C_REDRY_CHARGE_CODE, N_REDRY_CHARGE, N_CASE_WEIGHT, N_PACKED_WEIGHT, C_CR_BY, D_CR_DATE, C_UPD_BY, D_UPD_DATE);
        }
    }
}