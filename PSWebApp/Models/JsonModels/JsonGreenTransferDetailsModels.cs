﻿using System;

namespace PSWebApp.Models.JsonModels
{
    public class JsonGreenTransferDetailsModels
    {
        public string _bale_no { get; set; }
        public string BaleNo
        {
            set { _bale_no = value; }
            private get { return String.Format("\"bale_no\": \"{0}\"", _bale_no); }
        }
        public string _supplier { get; set; }
        public string Supplier
        {
            set { _supplier = value; }
            private get { return String.Format("\"supplier\": \"{0}\"", _supplier); }
        }
        public string _green_grade { get; set; }
        public string GreenGrade
        {
            set { _green_grade = value; }
            private get { return String.Format("\"green_grade\": \"{0}\"", _green_grade); }
        }
        public string _from_classified { get; set; }
        public string FromClassified
        {
            set { _from_classified = value; }
            private get { return String.Format("\"from_classified\": \"{0}\"", _from_classified); }
        }
        public string _to_classified { get; set; }
        public string ToClassified
        {
            set { _to_classified = value; }
            private get { return String.Format("\"to_classified\": \"{0}\"", _to_classified); }
        }
        public string _weight { get; set; }
        public string Weight
        {
            set { _weight = value; }
            private get { return String.Format("\"weight\": \"{0}\"", _weight); }
        }
        public string _unit_price { get; set; }
        public string UnitPrice
        {
            set { _unit_price = value; }
            private get { return String.Format("\"unit_price\": \"{0}\"", _unit_price); }
        }
        public string _amount { get; set; }
        public string Amount
        {
            set { _amount = value; }
            private get { return String.Format("\"amount\": \"{0}\"", _amount); }
        }

        public override string ToString()
        {
            return String.Format("{{{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}}}", BaleNo, Supplier, GreenGrade, FromClassified, ToClassified, Weight, UnitPrice, Amount);
        }
    }
}