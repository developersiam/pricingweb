﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSWebApp.Models.JsonModels
{
    public class JsonClassifiedExportValueModels
    {
        public string _classified_grade { get; set; }
        public string Classified_Grade
        {
            set { _classified_grade = value; }
            private get { return String.Format("\"classified_grade\": \"{0}\"", _classified_grade); }
        }

        public string _crop { get; set; }
        public string Crop
        {
            set { _crop = value; }
            private get { return String.Format("\"crop\": \"{0}\"", _crop); }
        }

  
        public string _export_value { get; set; }
        public string Export_Value
        {
            set { _export_value = value; }
            private get { return string.Format("\"export_value\": \"{0}\"", _export_value); }
        }


        public override string ToString()
        {
            return String.Format("{{{0}, {1}, {2}, {3}}}", Classified_Grade, Crop, Export_Value);
        }

    }
}