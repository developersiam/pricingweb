﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSWebApp.Models.JsonModels
{
    public class JsonShippingProductionMasterModel
    {
        public string _doc_no { get; set; }
        public string DocNo
        {
            set { _doc_no = value; }
            private get { return String.Format("\"doc_no\": \"{0}\"", _doc_no); }
        }
        public string _shipping_no { get; set; }
        public string Shipping_no
        {
            set { _shipping_no = value; }
            private get { return String.Format("\"shipping_no\": \"{0}\"", _shipping_no); }
        }
        public string _crop { get; set; }
        public string Crop
        {
            set { _crop = value; }
            private get { return String.Format("\"crop\": \"{0}\"", _crop); }
        }
        public string _doc_date { get; set; }
        public string Doc_Date
        {
            set { _doc_date = value; }
            private get { return String.Format("\"doc_date\": \"{0}\"", _doc_date); }
        }
        public string _type { get; set; }
        public string Type
        {
            set { _type = value; }
            private get { return String.Format("\"type\": \"{0}\"", _type); }
        }

        public override string ToString()
        {
            return String.Format("{{{0}, {1}, {2}, {3}, {4}}}", DocNo, Shipping_no, Crop, Doc_Date, Type);
        }
    }
}