﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSWebApp.Models.JsonModels
{
    public class JsonExchangeRateModels
    {
        public string _effective_date { get; set; }
        public string Effective_Date
        {
            set { _effective_date = value; }
            private get { return String.Format("\"effective_date\": \"{0}\"", _effective_date); }
        }

        public string _exchange_rate { get; set; }
        public string Exchange_Rate
        {
            set { _exchange_rate = value; }
            private get { return String.Format("\"exchange_rate\": \"{0}\"", _exchange_rate); }
        }

        public override string ToString()
        {
            return String.Format("{{{0}, {1}}}", Effective_Date, Exchange_Rate);
        }
    }
}