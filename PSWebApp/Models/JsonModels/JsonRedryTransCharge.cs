﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSWebApp.Models.JsonModels
{
    public class JsonRedryTransCharge
    {
        public string _redry_trans_charge_code { get; set; }
        public string Redry_Trans_Charge_Code
        {
            set { _redry_trans_charge_code = value; }
            private get { return String.Format("\"redry_trans_charge_code\": \"{0}\"", _redry_trans_charge_code); }
        }

        public string _redry_trans_charge_desc { get; set; }
        public string Redry_Trans_Charge_Desc
        {
            set { _redry_trans_charge_desc = value; }
            private get { return string.Format("\"redry_trans_charge_desc\": \"{0}\"", _redry_trans_charge_desc); }
        }
        public string _crop { get; set; }
        public string Crop
        {
            set { _crop = value; }
            private get { return String.Format("\"crop\": \"{0}\"", _crop); }
        }

        public string _charge { get; set; }
        public string Charge
        {
            set { _charge = value; }
            private get { return string.Format("\"charge\": \"{0}\"", _charge); }
        }


        public override string ToString()
        {
            return String.Format("{{{0}, {1}, {2}, {3}}}", Redry_Trans_Charge_Code, Redry_Trans_Charge_Desc, Crop, Charge);
        }
    }
}