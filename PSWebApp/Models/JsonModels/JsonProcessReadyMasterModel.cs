﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSWebApp.Models.JsonModels
{
    public class JsonProcessReadyMasterModel
    {
        public string _doc_no { get; set; }
        public string DocNo
        {
            set { _doc_no = value; }
            private get { return String.Format("\"doc_no\": \"{0}\"", _doc_no); }
        }
        public string _processingR_no { get; set; }
        public string ProcessingR_No
        {
            set { _processingR_no = value; }
            private get { return String.Format("\"processingR_no\": \"{0}\"", _processingR_no); }
        }
        public string _crop { get; set; }
        public string Crop
        {
            set { _crop = value; }
            private get { return String.Format("\"crop\": \"{0}\"", _crop); }
        }
        public string _doc_date { get; set; }
        public string Doc_Date
        {
            set { _doc_date = value; }
            private get { return String.Format("\"doc_date\": \"{0}\"", _doc_date); }
        }
        public string _packed_grade { get; set; }
        public string Packed_Grade
        {
            set { _packed_grade = value; }
            private get { return String.Format("\"packed_grade\": \"{0}\"", _packed_grade); }
        }
        public string _type { get; set; }
        public string Type
        {
            set { _type = value; }
            private get { return String.Format("\"type\": \"{0}\"", _type); }
        }
        public string _season { get; set; }
        public string Season
        {
            set { _season = value; }
            private get { return String.Format("\"season\": \"{0}\"", _season); }
        }
        public string _packed_weight { get; set; }
        public string Packed_Weight
        {
            set { _packed_weight = value; }
            private get { return String.Format("\"packed_weight\": \"{0}\"", _packed_weight); }
        }
        public string _weight { get; set; }
        public string Weight
        {
            set { _weight = value; }
            private get { return String.Format("\"weight\": \"{0}\"", _weight); }
        }
        public string _redry_charge { get; set; }
        public string Redry_Charge
        {
            set { _redry_charge = value; }
            private get { return String.Format("\"redry_charge\": \"{0}\"", _redry_charge); }
        }
        public string _pack_mat { get; set; }
        public string Pack_Mat
        {
            set { _pack_mat = value; }
            private get { return String.Format("\"pack_mat\": \"{0}\"", _pack_mat); }
        }
        public string _transportation { get; set; }
        public string Transportation
        {
            set { _transportation = value; }
            private get { return String.Format("\"transportation\": \"{0}\"", _transportation); }
        }

        public override string ToString()
        {
            return String.Format("{{{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7},{8}, {9}, {10}, {11}}}", DocNo, ProcessingR_No, Crop, Doc_Date, Packed_Grade, Type, Season, Packed_Weight, Weight, Redry_Charge, Pack_Mat, Transportation);
        }
    }
}