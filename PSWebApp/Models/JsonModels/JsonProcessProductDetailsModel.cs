﻿using System;

namespace PSWebApp.Models.JsonModels
{
    public class JsonProcessProductDetailsModel
    {
        public string _packed_grade { get; set; }
        public string Packed_grade
        {
            set { _packed_grade = value; }
            private get { return String.Format("\"packed_grade\": \"{0}\"", _packed_grade); }
        }
        public string _weight { get; set; }
        public string Weight
        {
            set { _weight = value; }
            private get { return String.Format("\"weight\": \"{0}\"", _weight); }
        }
        public string _unit_price { get; set; }
        public string UnitPrice
        {
            set { _unit_price = value; }
            private get { return String.Format("\"unit_price\": \"{0}\"", _unit_price); }
        }

        public string _amount { get; set; }
        public string Amount
        {
            set { _amount = value; }
            private get { return String.Format("\"amount\": \"{0}\"", _amount); }
        }
        public string _packing_mat { get; set; }
        public string Packing_mat
        {
            set { _packing_mat = value; }
            private get { return String.Format("\"packing_mat\": \"{0}\"", _packing_mat); }
        }
        public string _trans_charge { get; set; }
        public string Trans_charge
        {
            set { _trans_charge = value; }
            private get { return String.Format("\"trans_charge\": \"{0}\"", _trans_charge); }
        }
        public string _total_cost { get; set; }
        public string Total_cost
        {
            set { _total_cost = value; }
            private get { return String.Format("\"total_cost\": \"{0}\"", _total_cost); }
        }

        public override string ToString()
        {
            return String.Format("{{{0}, {1}, {2}, {3}, {4}, {5}, {6}}}", Packed_grade, Weight, UnitPrice, Amount, Packing_mat, Trans_charge, Total_cost);
        }

    }
}