﻿using System;

namespace PSWebApp.Models.JsonModels
{
    public class JsonReceivingMasterModels
    {
        public string _rec_doc_no { get; set; }
        public string C_REC_DOC_NO
        {
            set { _rec_doc_no = value; }
            private get { return String.Format("\"rec_doc_no\": \"{0}\"", _rec_doc_no); }
        }

        public string _rec_no { get; set; }
        public string C_REC_NO
        {
            set { _rec_no = value; }
            private get { return string.Format("\"rec_no\": \"{0}\"", _rec_no); }
        }
        public string _crop { get; set; }
        public string N_CROP
        {
            set { _crop = value; }
            private get { return String.Format("\"crop\": \"{0}\"", _crop); }
        }

        public string _rec_doc_date { get; set; }
        public string D_REC_DOC_DATE
        {
            set { _rec_doc_date = value; }
            private get { return string.Format("\"rec_doc_date\": \"{0}\"", _rec_doc_date); }
        }

        public string _buy_doc_no { get; set; }
        public string C_BUY_DOC_NO
        {
            set { _buy_doc_no = value; }
            private get { return String.Format("\"buy_doc_no\": \"{0}\"", _buy_doc_no); }
        }

        public string _company { get; set; }
        public string C_COMPANY
        {
            set { _company = value; }
            private get { return string.Format("\"company\": \"{0}\"", _company); }
        }
        public string _type { get; set; }
        public string C_TYPE
        {
            set { _type = value; }
            private get { return String.Format("\"type\": \"{0}\"", _type); }
        }

        public string _season { get; set; }
        public string C_SEASON
        {
            set { _season = value; }
            private get { return string.Format("\"season\": \"{0}\"", _season); }
        }

        public string _curer_code { get; set; }
        public string C_CURER_CODE
        {
            set { _curer_code = value; }
            private get { return String.Format("\"curer_code\": \"{0}\"", _curer_code); }
        }

        public string _curer_name { get; set; }
        public string C_CURER_NAME
        {
            set { _curer_name = value; }
            private get { return string.Format("\"curer_name\": \"{0}\"", _curer_name); }
        }

        public string _area { get; set; }
        public string C_AREA
        {
            set { _area = value; }
            private get { return String.Format("\"area\": \"{0}\"", _area); }
        }

        public string _green_price_code { get; set; }
        public string C_GREEN_PRICE_CODE
        {
            set { _green_price_code = value; }
            private get { return string.Format("\"green_price_code\": \"{0}\"", _green_price_code); }
        }
        public string _rec_type_rb { get; set; }
        public string C_REC_TYPE_RB
        {
            set { _rec_type_rb = value; }
            private get { return String.Format("\"rec_type_rb\": \"{0}\"", _rec_type_rb); }
        }

        public string _prog_id { get; set; }
        public string C_PROG_ID
        {
            set { _prog_id = value; }
            private get { return string.Format("\"prog_id\": \"{0}\"", _prog_id); }
        }

        public override string ToString()
        {
            return String.Format("{{{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}}}", C_REC_DOC_NO, C_REC_NO, N_CROP, D_REC_DOC_DATE, C_BUY_DOC_NO, C_COMPANY, C_TYPE, C_SEASON, C_CURER_CODE, C_CURER_NAME, C_AREA, C_GREEN_PRICE_CODE, C_REC_TYPE_RB, C_PROG_ID);
        }

    }
}