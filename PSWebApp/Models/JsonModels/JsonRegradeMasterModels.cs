﻿using System;

namespace PSWebApp.Models.JsonModels
{
    public class JsonRegradeMasterModels
    {
        public string _regrade_doc_no { get; set; }
        public string Regrade_Doc_No
        {
            set { _regrade_doc_no = value; }
            private get { return String.Format("\"regrade_doc_no\": \"{0}\"", _regrade_doc_no); }
        }

        public string _regrade_no { get; set; }
        public string Regrade_No
        {
            set { _regrade_no = value; }
            private get { return string.Format("\"regrade_no\": \"{0}\"", _regrade_no); }
        }
        public string _crop { get; set; }
        public string Crop
        {
            set { _crop = value; }
            private get { return String.Format("\"crop\": \"{0}\"", _crop); }
        }

        public string _regrade_doc_date { get; set; }
        public string Regrade_Doc_Date
        {
            set { _regrade_doc_date = value; }
            private get { return string.Format("\"regrade_doc_date\": \"{0}\"", _regrade_doc_date); }
        }

        public string _company { get; set; }
        public string Company
        {
            set { _company = value; }
            private get { return string.Format("\"company\": \"{0}\"", _company); }
        }
        public string _type { get; set; }
        public string Type
        {
            set { _type = value; }
            private get { return String.Format("\"type\": \"{0}\"", _type); }
        }

        public string _season { get; set; }
        public string Season
        {
            set { _season = value; }
            private get { return string.Format("\"season\": \"{0}\"", _season); }
        }

        public override string ToString()
        {
            return String.Format("{{{0}, {1}, {2}, {3}, {4}, {5}, {6}}}", Regrade_Doc_No, Regrade_No, Crop, Regrade_Doc_Date, Company, Type, Season);
        }
    }
}