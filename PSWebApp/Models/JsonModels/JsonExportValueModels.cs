﻿using System;

namespace PSWebApp.Models.JsonModels
{
    public class JsonExportValueModels
    {
        public string _packed_grade { get; set; }
        public string Packed_Grade
        {
            set { _packed_grade = value; }
            private get { return String.Format("\"packed_grade\": \"{0}\"", _packed_grade); }
        }

        public string _customer { get; set; }
        public string Customer
        {
            set { _customer = value; }
            private get { return string.Format("\"customer\": \"{0}\"", _customer); }
        }
        public string _crop { get; set; }
        public string Crop
        {
            set { _crop = value; }
            private get { return String.Format("\"crop\": \"{0}\"", _crop); }
        }

        public string _customer_grade { get; set; }
        public string Customer_Grade
        {
            set { _customer_grade = value; }
            private get { return string.Format("\"customer_grade\": \"{0}\"", _customer_grade); }
        }

        public string _export_value { get; set; }
        public string Export_Value
        {
            set { _export_value = value; }
            private get { return string.Format("\"export_value\": \"{0}\"", _export_value); }
        }


        public override string ToString()
        {
            return String.Format("{{{0}, {1}, {2}, {3}, {4}}}", Packed_Grade, Customer, Crop, Customer_Grade, Export_Value);
        }
    }
}