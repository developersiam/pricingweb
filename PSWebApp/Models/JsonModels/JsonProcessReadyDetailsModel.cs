﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSWebApp.Models.JsonModels
{
    public class JsonProcessReadyDetailsModel
    {
        public string _bale_no { get; set; }
        public string BaleNo
        {
            set { _bale_no = value; }
            private get { return String.Format("\"bale_no\": \"{0}\"", _bale_no); }
        }
        public string _green_grade { get; set; }
        public string GreenGrade
        {
            set { _green_grade = value; }
            private get { return String.Format("\"green_grade\": \"{0}\"", _green_grade); }
        }

        public string _packed_grade { get; set; }
        public string Packed_grade
        {
            set { _packed_grade = value; }
            private get { return String.Format("\"packed_grade\": \"{0}\"", _packed_grade); }
        }
        public string _weight { get; set; }
        public string Weight
        {
            set { _weight = value; }
            private get { return String.Format("\"weight\": \"{0}\"", _weight); }
        }
        public string _unit_price { get; set; }
        public string UnitPrice
        {
            set { _unit_price = value; }
            private get { return String.Format("\"unit_price\": \"{0}\"", _unit_price); }
        }
        public string _gep { get; set; }
        public string Gep
        {
            set { _gep = value; }
            private get { return String.Format("\"gep\": \"{0}\"", _gep); }
        }
        public string _gep_per { get; set; }
        public string Gep_per
        {
            set { _gep_per = value; }
            private get { return String.Format("\"gep_per\": \"{0}\"", _gep_per); }
        }
        public string _total_unit { get; set; }
        public string Total_unit
        {
            set { _total_unit = value; }
            private get { return String.Format("\"total_unit\": \"{0}\"", _total_unit); }
        }

        public string _amount { get; set; }
        public string Amount
        {
            set { _amount = value; }
            private get { return String.Format("\"amount\": \"{0}\"", _amount); }
        }
        public string _flag { get; set; }
        public string Flag
        {
            set { _flag = value; }
            private get { return String.Format("\"flag\": \"{0}\"", _flag); }
        }

        public override string ToString()
        {
            return String.Format("{{{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}}}", BaleNo, GreenGrade, Packed_grade, Weight, UnitPrice, Gep, Gep_per, Total_unit, Amount, Flag);
        }

    }
}