﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSWebApp.Models.JsonModels
{
    public class JsonPackingMatModels
    {
        public string _packing_mat { get; set; }
        public string Packing_Mat
        {
            set { _packing_mat = value; }
            private get { return String.Format("\"packing_mat\": \"{0}\"", _packing_mat); }
        }

        public string _crop { get; set; }
        public string Crop
        {
            set { _crop = value; }
            private get { return String.Format("\"crop\": \"{0}\"", _crop); }
        }

        public string _price { get; set; }
        public string Price
        {
            set { _price = value; }
            private get { return string.Format("\"price\": \"{0}\"", _price); }
        }


        public override string ToString()
        {
            return String.Format("{{{0}, {1}, {2}}}", Packing_Mat, Crop, Price);
        }
    }
}