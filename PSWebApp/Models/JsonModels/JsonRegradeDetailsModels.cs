﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSWebApp.Models.JsonModels
{
    public class JsonRegradeDetailsModels
    {
        public string _bale_no { get; set; }
        public string BaleNo
        {
            set { _bale_no = value; }
            private get { return String.Format("\"bale_no\": \"{0}\"", _bale_no); }
        }
        public string _green_grade { get; set; }
        public string GreenGrade
        {
            set { _green_grade = value; }
            private get { return String.Format("\"green_grade\": \"{0}\"", _green_grade); }
        }
        public string _classified { get; set; }
        public string Classified
        {
            set { _classified = value; }
            private get { return String.Format("\"classified\": \"{0}\"", _classified); }
        }

        public string _weight { get; set; }
        public string Weight
        {
            set { _weight = value; }
            private get { return String.Format("\"weight\": \"{0}\"", _weight); }
        }
        public string _unit_price { get; set; }
        public string UnitPrice
        {
            set { _unit_price = value; }
            private get { return String.Format("\"unit_price\": \"{0}\"", _unit_price); }
        }
        public string _amount { get; set; }
        public string Amount
        {
            set { _amount = value; }
            private get { return String.Format("\"amount\": \"{0}\"", _amount); }
        }
        public string _flag_io { get; set; }
        public string FlagIO
        {
            set { _flag_io = value; }
            private get { return String.Format("\"flag_io\": \"{0}\"", _flag_io); }
        }
        public override string ToString()
        {
            return String.Format("{{{0}, {1}, {2}, {3}, {4}, {5}, {6}}}", BaleNo, GreenGrade, Classified, Weight, UnitPrice, Amount, FlagIO);
        }
    }
}