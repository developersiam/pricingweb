﻿namespace PSWebApp.Models
{
    public class NavigationModel
    {
        public string Title { get; set; }
        public string Instruction { get; set; }
        public string Url { get; set; }
    }
}