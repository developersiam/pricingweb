﻿using System;

namespace PSWebApp.Models
{
    [Serializable]
    public class ReceivingModels
    {
        //PS_RECEIVING_M & PS_V_MAT_RECEIVING
        public string ReceivingNo { get; set; }
        public string DocumentNo { get; set; }
        public string BuyingDocNo { get; set; }
        public DateTime DocumentDate { get; set; }
        public string CompanyCode { get; set; }
        public int Crop { get; set; }
        public string Type { get; set; }
        public string Season { get; set; }
        public string CurerCode { get; set; }
        public string CurerName { get; set; }
        public string Area { get; set; }
        public string RecordType { get; set; }
        public string GreenPriceCode { get; set; }
        public string WeightBuy { get; set; }
        public string WeightReceived { get; set; }
        public string AveragePriceBuy { get; set; }
        public string AveragePriceRec { get; set; }
        public string TotalAmount { get; set; }

    }
}