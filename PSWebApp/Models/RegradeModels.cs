﻿using System;

namespace PSWebApp.Models
{
    [Serializable]
    public class RegradeModels
    {
        public string RegradeNo { get; set; }
        public string DocumentNo { get; set; }
        public DateTime DocumentDate { get; set; }
        public string CompanyCode { get; set; }
        public int Crop { get; set; }
        public string Type { get; set; }
        public string Season { get; set; }
        public string TotalWeight { get; set; }
        public string AveragePrice { get; set; }
        public string TotalAmount { get; set; }
    }
}