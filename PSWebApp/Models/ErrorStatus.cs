﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace PSWebApp.Models
{
    public enum ErrorStatus
    {
        PasswordExpired,
        PermissionFeature,
        PermissionDataSource,
        MultipleLogin,
        SelectedSource,
        Limit,
        List1,
        List2,
        TicketNotExist,
        TimeOut,
        Default
    }

    public class ErrorStatusMessage
    {
        public string Title { get; set; }
        public string Message { get; set; }
        public static ErrorStatusMessage GetError(ErrorStatus status)
        {
            string title = string.Empty;
            string error = string.Empty;

            switch (status)
            {
                case ErrorStatus.PasswordExpired:
                    {
                        title = "Expired";
                        error = "Password was expired";
                        break;
                    }
                case ErrorStatus.PermissionFeature:
                    {
                        title = "Access Denied";
                        error = "You have no access this feature. Please contact the administrator if this is incorrect.";
                        break;
                    }
                case ErrorStatus.PermissionDataSource:
                    {
                        title = "Access Denied";
                        error = "You do not have permission to access any Data Sources";
                        break;
                    }
                case ErrorStatus.MultipleLogin:
                    {
                        title = "Access Denied";
                        error = "Another login has been detected, please retry later.";
                        break;
                    }
                case ErrorStatus.SelectedSource:
                    {
                        title = "Access Denied";
                        error = "No Selected Source";
                        break;
                    }
                case ErrorStatus.Limit:
                    {
                        string limit = ConfigurationManager.AppSettings["ProcurementCheckEntityLimitNumber"];
                        title = "Limit Exceeded";
                        error = string.Format("Number of matched entities exceeded {0} in total from both lists. Please reduce the size and try again.", limit);
                        break;
                    }
                case ErrorStatus.List1:
                    {
                        title = "Error Occured in List 1";
                        break;
                    }
                case ErrorStatus.List2:
                    {
                        title = "Error Occured in List 2";
                        break;
                    }
                case ErrorStatus.TicketNotExist:
                    {
                        title = "Request Fail";
                        error = "Cannot find request";
                        break;
                    }
                case ErrorStatus.TimeOut:
                    {
                        title = "Timeout";
                        error = "Request Timeout";
                        break;
                    }
                default:
                    {
                        title = "We're sorry";
                        error = "Service is temporarily unavailable. Our officers are working to resolve the issue.";
                        break;
                    }
            }
            return new ErrorStatusMessage() { Title = title, Message = error };
        }
    }
}