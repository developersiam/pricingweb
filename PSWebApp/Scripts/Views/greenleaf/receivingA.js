﻿var table;
$(document).ready(function () {
    $(".dropdown-menu li a").click(function () {
        var selText = $(this).text();
        $(this).parents('.btn-group').find('.dropdown-toggle').html(selText + ' <span data-item="' + $(this).data("item") + '" ></span>');
    });
});

$(window).load(function () {
    //table = $('#receiving-table').bootstrapTable({
    //    rowAttributes: {
    //        "data-id": function (rowItem) {
    //            return rowItem.baleNo;
    //        }
    //    },
    //}).on('click-row.bs.table', function (e, row, $element) {
    //    $('#receiving-table > tbody > tr').removeClass('highlight');
    //    $element.addClass('highlight');
    //    $('#baleNo').val($($element).attr('data-id'));
    //}).on('dbl-click-row.bs.table', function (e, row, $element) {
    //    $('#baleNo').val($($element).attr('data-id'));
    //    $('#frm-receiving-create').submit();
    //}).on('load-success.bs.table', function () {
    //    adjust_table_by_size();
    //    adjust_report_table
    //});

    $('.pagination-detail').removeClass('hidden-xs');

    adjust_table_by_size();
    adjust_report_table();
})

$(window).resize(function () {
    adjust_table_by_size();
    adjust_report_table
});


$('ul[aria-labelledby="dropdown-recno"] li a').click(function () {
    var recDocNo = $(this).text();
    var recDocNo = $(this).attr('value');
    var crop = $(this).attr('crop');
    var btn_recno = $('#dropdown-recno');
    btn_recno.html(recDocNo);
    show_receivingMaster();
    show_receivingtable(recDocNo, crop);
    //window.location.href = window.location.href;
});

function show_receivingMaster(recDocNo) {
    var url = '/Receiving/AddReceiving';
    var selected_rec = $('#ReceivingNo').val();
    $.ajax({
        url: url,
        type: 'GET',
        data: { recDocNo: selected_rec },
        success: function (data) {
            //$('#frm-receiving-create').submit();
        }
    });    
}

function show_receivingtable(recDocNo, crop) {
    //var crop = $('#dropdown-recno option:selected').attr('crop')
    var load_table_url = '/Receiving/JsonAddReceivingDetails';
    var param = {
        recDocNo: recDocNo,
        crop: crop
    }
    $.ajax({
        url: load_table_url,
        data: { param: param },
        type: "POST",
        success: function (data) {
            table = $('#receiving-table').bootstrapTable({
                serverParams: {
                    recDocNo: recDocNo,
                    crop: crop
                }
            }).on('load-success.bs.table', function () {
                table.bootstrapTable('load', data.rows);
                adjust_table_by_size();
                adjust_report_table();
            });


            table.bootstrapTable('refresh');
        },
        dataType: 'json'
    });
}

function adjust_table_by_size() {
    var screen_width = $(window).width();
    if (screen_width < 768) {
        //extra small device
        adjust_table_column("xs");
    }
    else if (screen_width < 992) {
        //small device
        adjust_table_column("sm");
    }
    else if (screen_width < 1200) {
        //medium device
        adjust_table_column("md");
    }
}

function adjust_table_column(size) {
    var hide_column = [];
    if (size == "md") {
        hide_column = [];
    }
    else if (size == "sm") {
        hide_column = ["rec_unit_price", "fcv_class"];
    }
    else if (size == "xs") {
        hide_column = ["t_charge","rec_unit_price", "fcv_class"];
    }
    for (var i = 0; i < hide_column.length; i++) {
        table.bootstrapTable('hideColumn', hide_column[i]);
    }
}

function adjust_report_table() {
    var height = parseInt($('.hero').css('margin-top')) +
        $('.fixed-table-header').height() +
        $(".filterBox").outerHeight() +
        $('.clearfix').height() +
        $('.navbar-fixed-bottom').height();
    height = $(window).height() - height - 100;
    $('.fixed-table-body').height(height);
}

$('#frm-receiving-create').submit(function (e) {
    $(this).validate().form();

    var is_valid = true;
    var error_count = $('[data-valmsg-for]:has(>span)').length;

    if (error_count > 0)
        is_valid = false;

    if (is_valid) {
        bootbox.dialog({
            message: '<h4 class="text-center"><i class="fa fa-refresh fa-spin fa-fw"></i></i>&nbsp;Submitting Receiving Information</h4>',
            closeButton: false
        });
    }
});

