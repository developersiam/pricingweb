﻿var table_packed;
var table_classified;
var change_action = "packed";
var crop;
var packed;

table_packed = $("#export-table");
table_classified = $("#classified-table");

$(window).load(function () {
    Initial();

    if ($('ul[role="menu"] li a').data('action') == "packed") {
        table = $('#export-table').bootstrapTable({
            rowAttributes: {
                "data-id": function (rowItem) {
                    return rowItem.crop;
                }
            },
        }).on('click-row.bs.table', function (e, row, $element) {
            $('#export-table > tbody > tr').removeClass('highlight');
            $element.addClass('highlight');
            $('#crop').val(row.crop);
            $('#btn-input-export').removeClass('disabled');
        }).on('dbl-click-row.bs.table', function (e, row, $element) {
            $('#crop').val($($element).attr('data-id'));
            $('#txtpacked').val(row.packed_grade);
            $('#txtcustomer').val(row.customer);
            $('#txtcustomerPacked').val(row.customer_grade);
            $('#txtexport').val(row.export_value);
            $('#btn-input-export').removeClass('disabled');
            $('#btn-delete-export').removeClass('disabled');
        }).on('load-success.bs.table', function () {
            adjust_table_by_size();
            adjust_report_table
        });
    }

    //$('.pagination-detail').removeClass('hidden-xs');

    //adjust_table_by_size();
    //adjust_report_table();

});


$('ul[role="menu"] li a').click(function () {
    Initial();
    change_action = $(this).data('action');
    var action_text = $(this).text();
    $("#dropdown-type").html($('<strong>' + action_text + '</strong></span>'));
    //Show data
    if (change_action == "packed")
        showPackedGrade();
    else
        showClassified();
});

function FindExport(e) {
    Initial();
    if (e.keyCode == 13) {
        //Initial();
        change_action = $('ul[role="menu"] li a').data('action');
        if (change_action == "packed")
        {
            //Show Packed Grade detail
            showPackedGrade();
        }
        else
            //Show Classified
            showClassified();
    }
}

function Initial() {
    $('#btn-input-export').addClass('disabled');
    table_packed.hide();
    table_classified.hide();
}


function showPackedGrade() {
    table_packed.show();
    table_classified.hide();

    //var load_table_url = '/ExportValue/JsonPackedGrade';
    var param = {
        scrop: $('#txt-crop').val()
    }
    var option = table.bootstrapTable('getOptions');
    $.ajax({
        url: urlpacked,
        data: { param: param },
        success: function (data) {
            if (data.success) {
                table = $('#export-table').bootstrapTable({
                    serverParams: {
                        scrop: $('#txt-crop').val()
                    }
                }).on('load-success.bs.table', function () {
                    table.bootstrapTable('load', data.rows);
                    adjust_table_by_size();
                    adjust_report_table();
                });
                table.bootstrapTable('load', data.rows);
                table.bootstrapTable('refresh');
            }
        },
        dataType: 'json'
    });
}

function showClassified() {
    table_classified.show();
    table_packed.hide();

    //var load_table_url = '/ExportValue/JsonClassifiedGrade';
    var param = {
        scrop: $('#txt-crop').val()
    }
    var option = table.bootstrapTable('getOptions');
    $.ajax({
        url: urlclassify,
        data: { param: param },
        success: function (data) {
            if (data.success) {
                table = $('#classified-table').bootstrapTable({
                serverParams: {
                    scrop: $('#txt-crop').val()
                }
            }).on('load-success.bs.table', function () {
                table.bootstrapTable('load', data.rows);
                adjust_table_by_size();
                adjust_report_table();
            });
            table.bootstrapTable('load', data.rows);
            table.bootstrapTable('refresh');
            }
        },
        dataType: 'json'
    });
}


$(window).resize(function () {
    adjust_table_by_size();
    adjust_report_table
});


$('#btn-delete-export').click(function () {
    var Crop = $('#txt-crop').val();
    var Customer = $('#txtcustomer').val();
    var CustomerPacked = $('#txtcustomerPacked').val();
    var PackedGrade = $('#txtpacked').val();

    var frm_delete = $('<form action="/Pricing/ExportValue/DeleteExportValue" method="post"/>');
    $('body').append(frm_delete);
    frm_delete.append($('<input name="Crop" type="hidden" value="' + Crop + '" />'));
    frm_delete.append($('<input name="Customer" type="hidden" value="' + Customer + '" />'));
    frm_delete.append($('<input name="CustomerPacked" type="hidden" value="' + CustomerPacked + '" />'));
    frm_delete.append($('<input name="PackedGrade" type="hidden" value="' + PackedGrade + '" />'));
    frm_delete.submit();
});


function adjust_table_by_size() {
    var screen_width = $(window).width();
    if (screen_width < 768) {
        //extra small device
        adjust_table_column("xs");
    }
    else if (screen_width < 992) {
        //small device
        adjust_table_column("sm");
    }
    else if (screen_width < 1200) {
        //medium device
        adjust_table_column("md");
    }
}

function adjust_table_column(size) {
    var hide_column = [];
    if (size == "md") {
        hide_column = [];
    }
    else if (size == "sm") {
        hide_column = [];
    }
    else if (size == "xs") {
        hide_column = [];
    }
    for (var i = 0; i < hide_column.length; i++) {
        table.bootstrapTable('hideColumn', hide_column[i]);
    }
}

function adjust_report_table() {
    var height = parseInt($('.hero').css('margin-top')) +
        $('.fixed-table-header').height() +
        $(".filterBox").outerHeight() +
        $('.clearfix').height() +
        $('.navbar-fixed-bottom').height();
    height = $(window).height() - height - 100;
    $('.fixed-table-body').height(height);
}

