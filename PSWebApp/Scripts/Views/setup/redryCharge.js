﻿var table;
var table_edit;
var change_action = "redry";
var crop;
var code;

$(window).load(function () {
    table = $('#redryC-table').bootstrapTable({
        rowAttributes: {
            "data-id": function (rowItem) {
                return rowItem.redry_trans_charge_code;
            }
        },
    }).on('click-row.bs.table', function (e, row, $element) {
        $('#redryC-table > tbody > tr').removeClass('highlight');
        $element.addClass('highlight');
        $('#Code').val($($element).attr('data-id'));
    }).on('dbl-click-row.bs.table', function (e, row, $element) {
        $('#Code').val($($element).attr('data-id'));
        $('#txtCode').val($($element).attr('data-id'));
        $('#txtDescription').val(row.redry_trans_charge_desc);
        $('#txtUnit').val(row.charge);
        $('#btn-submit-redrycharge').removeClass('disabled');
        $('#btn-delete-redrycharge').removeClass('disabled');
    }).on('load-success.bs.table', function () {
        adjust_table_by_size();
        adjust_report_table
    });

    $('.pagination-detail').removeClass('hidden-xs');

    adjust_table_by_size();
    adjust_report_table();
});


$('ul[role="menu"] li a').click(function () {
    Initial();
    change_action = $(this).data('action');
    var action_text = $(this).text();
    $("#dropdown-type").html($('<strong>' + action_text + '</strong></span>'));
    //Show data
    if (change_action == "redry")
        showRedrydata();
    else
        showTransport();
});

function FindRedryCharge(e) {
    if (e.keyCode == 13) {
        Initial();
        if (change_action == "redry")
            showRedrydata();
        else
            showTransport();
    }
}

function Initial() {
    $('#btn-submit-redrycharge').addClass('disabled');
    $('#btn-delete-redrycharge').addClass('disabled');
    //$('#txtCode').val() = "";
    //$('#txtDescription').val() = "";
    //$('#txtUnit').val() = "";
}

function showRedrydata() {
    //var load_table_url = '/RedryCharge/JsonShowRedryCharge';
    var param = {
        scrop: $('#txt-crop').val()
    }
    var option = table.bootstrapTable('getOptions');
    $.ajax({
        url: urlpath,
        data: { param: param },
        success: function (data) {
            table = $('#redryC-table').bootstrapTable({
            serverParams: {
                scrop: $('#txt-crop').val()
            }}).on('load-success.bs.table', function () {
                table.bootstrapTable('load', data.rows);
                adjust_table_by_size();
                adjust_report_table();
            });
            table.bootstrapTable('load', data.rows);
            table.bootstrapTable('refresh');
        },
        dataType: 'json'
    });
}

function showTransport() {
    //var load_table_url = '/RedryCharge/JsonShowTransCharge';
    var param = {
        scrop: $('#txt-crop').val()
    }
    var option = table.bootstrapTable('getOptions');
    $.ajax({
        url: urlnew,
        data: { param: param },
        success: function (data) {
            table = $('#redryC-table').bootstrapTable({
                serverParams: {
                    scrop: $('#txt-crop').val()
                }
            }).on('load-success.bs.table', function () {
                table.bootstrapTable('load', data.rows);
                adjust_table_by_size();
                adjust_report_table();
            });
            table.bootstrapTable('load', data.rows);
            table.bootstrapTable('refresh');
        },
        dataType: 'json'
    });
}


$(window).resize(function () {
    adjust_table_by_size();
    adjust_report_table
});


$('#btn-delete-redrycharge').click(function () {
    var Crop = $('#txt-crop').val();
    var Code = $('#txtCode').val();
    var frm_delete = $('<form action="/Pricing/RedryCharge/DeleteRedryCharge" method="post"/>');
    $('body').append(frm_delete);
    frm_delete.append($('<input name="Code" type="hidden" value="' + Code + '" />'));
    frm_delete.append($('<input name="Crop" type="hidden" value="' + Crop + '" />'));
    frm_delete.append($('<input name="type" type="hidden" value="' + change_action + '" />'));
    frm_delete.submit();
});

$('#btn-submit-redrycharge').click(function () {
    var Crop = $('#txt-crop').val();
    var Code = $('#txtCode').val();
    var Desc = $('#txtDescription').val();
    var Cost = $('#txtUnit').val();
    var frm_submit = $('<form action="/Pricing/RedryCharge/SubmitRedryCharge" method="post"/>');
    $('body').append(frm_submit);
    frm_submit.append($('<input name="Code" type="hidden" value="' + Code + '" />'));
    frm_submit.append($('<input name="Crop" type="hidden" value="' + Crop + '" />'));
    frm_submit.append($('<input name="type" type="hidden" value="' + change_action + '" />'));
    frm_submit.append($('<input name="Desc" type="hidden" value="' + Desc + '" />'));
    frm_submit.append($('<input name="Cost" type="hidden" value="' + Cost + '" />'));
    frm_submit.submit();
});


$('#redryC-modal').on('hidden.bs.modal', function (e) {
    $('#frm-setup-redryC')[0].reset();
})
$('#btn-redryC-modal-close').click(function () {
    $('#redryC-modal').hide();
})
$('#btn-redryC-modal-close2').click(function () {
    $('#redryC-modal').hide();
})
$('#txtCode').click(function () {
    $('#btn-submit-redrycharge').removeClass('disabled');
})

//$('#btn-save-redryC').click(function () {
//    var is_valid = true;
//    $('#change-credit-row').removeClass('has-error');
//    if ($('#txt-change-balance').val().trim() == "") {
//        $('#change-credit-row').addClass('has-error');
//        is_valid = false;
//    }
//    $('#change-reason-row').removeClass('has-error');
//    if ($('#change-reason').val() == "") {
//        $('#change-reason-row').addClass('has-error');
//        is_valid = false;
//    }
//    if (is_valid) {
//        var change_balance = $('#txt-change-balance').val();
//        var remark = $('#change-reason').val();
//        var userid = $('#user-id').val();

//        if (!is_number(change_balance) || change_balance < 0)
//            change_balance = 0;
//        else
//            change_balance = parseFloat(change_balance);
//        var action_url;
//        if (change_action == "topup")
//            action_url = "/Credit/TopupUserCredit";
//        else if (change_action == "reduce")
//            action_url = "/Credit/ReduceUserCredit";
//        $.ajax({
//            url: action_url,
//            type: "POST",
//            cache: false,
//            data: {
//                value: change_balance,
//                remark: remark,
//                id: userid
//            },
//            success: function (data) {
//                if (data.success) {
//                    $('#user-history-table').bootstrapTable('refresh');
//                    $('#change-balance-modal').modal('hide');
//                    $('#txt-change-balance').val('');
//                    $('#change-reason').val('');
//                    var current_balance = $('[data-id="current-balance"]');

//                    current_balance.removeClass('negative');
//                    current_balance.removeClass('warning');
//                    if (new_balance < 0)
//                        current_balance.addClass('negative');
//                    else if (new_balance < 100)
//                        current_balance.addClass('warning');
//                    current_balance.html(new_balance.toFixed(2));
//                }
//            }
//        });
//    }
//});

function adjust_table_by_size() {
    var screen_width = $(window).width();
    if (screen_width < 768) {
        //extra small device
        adjust_table_column("xs");
    }
    else if (screen_width < 992) {
        //small device
        adjust_table_column("sm");
    }
    else if (screen_width < 1200) {
        //medium device
        adjust_table_column("md");
    }
}

function adjust_table_column(size) {
    var hide_column = [];
    if (size == "md") {
        hide_column = [];
    }
    else if (size == "sm") {
        hide_column = [];
    }
    else if (size == "xs") {
        hide_column = [];
    }
    for (var i = 0; i < hide_column.length; i++) {
        table.bootstrapTable('hideColumn', hide_column[i]);
    }
}

function adjust_report_table() {
    var height = parseInt($('.hero').css('margin-top')) +
        $('.fixed-table-header').height() +
        $(".filterBox").outerHeight() +
        $('.clearfix').height() +
        $('.navbar-fixed-bottom').height();
    height = $(window).height() - height - 100;
    $('.fixed-table-body').height(height);
}

