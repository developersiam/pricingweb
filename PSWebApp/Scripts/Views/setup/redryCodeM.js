﻿var table;
var table_edit;
var ByProduct = "Product";
var crop;
var code;

$(document).ready(function () {
    $('input[name="byProductOptions"]:radio').click(function () {
        //alert("Clicked");
        if ($(this).attr('checkstate') == 'true') {
            $(this).attr('checked', false);
            $(this).attr('checkstate', false);
            showRedryCode();
        }
        else {
            $(this).attr('checked', true);
            $(this).attr('checkstate', true);
            showRedryCodeByProduct();
        }
    });
    //$('input[name="byProductOptions"]:radio:first').prop('checked', true).trigger("click");
    $('input[name="byProductOptions"]').removeAttr('checked');
});

$(window).load(function () {
    table = $('#redryCode-table').bootstrapTable({
        rowAttributes: {
            "data-id": function (rowItem) {
                return rowItem.crop;
            }
        },
        //rowAttributes: {
        //    "data-id": function (rowItem) {
        //        return rowItem.hand_no;
        //    }
        //},
    }).on('click-row.bs.table', function (e, row, $element) {
        $('#redryCode-table > tbody > tr').removeClass('highlight');
        $element.addClass('highlight');
        $('#txtPacked').val(row.packed_grade);
        $('#txtCrop').val(row.Crop);
        $('#packGrade').val($('#txtPacked').val());
        $('#cropSelected').val($('#txtCrop').val());
        $('#btn-update-redryCode').removeClass('disabled');
        $('#btn-delete-redryCode').removeClass('disabled');
    }).on('dbl-click-row.bs.table', function (e, row, $element) {
        $('#txtPacked').val(row.packed_grade);
        $('#txtCrop').val(row.Crop);
        //$('#txtType').val(row.type);
        //$('#txtCustomer').val(row.customer);
        //$('#txtFrom').val(row.form);
        //$('#txtNet').val(row.net_def);
        //$('#txtTare').val(row.tare_def);
        //$('#txtGross').val(row.gross_def);
        //$('#txtgep').val(row.gep);
        //$('#txtGEPBaht').val(row.gep_baht);
        //$('#dropdown-Transport').val(row.transport_code);
        //$('#txtTransportPrice').val(row.transport);
        //$('#ByProduct').val(row.price);
        //$('#dropdown-Redrying').val(row.redry_code);
        //$('#txtRedryPrice').val(row.redry_charge);
        $('#packGrade').val($('#txtPacked').val());
        $('#cropSelected').val($('#txtCrop').val());
        $('#frm-redryCode-details').submit();
    }).on('load-success.bs.table', function () {
        adjust_table_by_size();
        adjust_report_table
    });

    $('.pagination-detail').removeClass('hidden-xs');

    adjust_table_by_size();
    adjust_report_table();
});

$('#dropdown-crop').change(function () {
    Initial();
    //GetRedryAndTransport();
    if ($('input[name="byProductOptions"]:radio').attr('checkstate') == 'true') {
        //Show ByProduct Query
        //$('#dropdown-Redrying').addClass('hidden');
        //$('#txtRedryPrice').addClass('hidden');
        showRedryCodeByProduct();
    }
    else {
        //Show others
        showRedryCode();
        //$('#dropdown-Redrying').removeClass('hidden');
        //$('#txtRedryPrice').removeClass('hidden');
    }
});

function Initial() {
    $('#btn-update-redryCode').addClass('disabled');
    $('#btn-delete-redryCode').addClass('disabled');
}

function GetRedryAndTransport() {
    var selectedCrop = $('#dropdown-crop').val();
    $('#dropdown-Transport').find('option[data-crop]').hide()
    $('#dropdown-Transport').find('option[data-crop="' + $(this).val() + '"]').show();
}

function showRedryCodeByProduct() {
    //var load_table_url = '/RedryCode/JsonRedryCodeByProduct';
    var param = {
        scrop: $('#dropdown-crop').val()
    }
    var option = table.bootstrapTable('getOptions');
    $.ajax({
        url: urlpath,
        data: { param: param },
        success: function (data) {
            table = $('#redryCode-table').bootstrapTable({
                serverParams: {
                    scrop: $('#dropdown-crop').val()
                }
            }).on('load-success.bs.table', function () {
                table.bootstrapTable('load', data.rows);
                adjust_table_by_size();
                adjust_report_table();
            });
            table.bootstrapTable('load', data.rows);
            table.bootstrapTable('refresh');
        },
        dataType: 'json'
    });
}

function showRedryCode() {
    //var load_table_url = '/RedryCode/JsonRedryCode';
    var param = {
        scrop: $('#dropdown-crop').val()
    }
    var option = table.bootstrapTable('getOptions');
    $.ajax({
        url: urlnew,
        data: { param: param },
        success: function (data) {
            table = $('#redryCode-table').bootstrapTable({
                serverParams: {
                    scrop: $('#dropdown-crop').val()
                }
            }).on('load-success.bs.table', function () {
                table.bootstrapTable('load', data.rows);
                adjust_table_by_size();
                adjust_report_table();
            });
            table.bootstrapTable('load', data.rows);
            table.bootstrapTable('refresh');
        },
        dataType: 'json'
    });
}


$(window).resize(function () {
    adjust_table_by_size();
    adjust_report_table
});


//$('#btn-delete-redrycharge').click(function () {
//    var Crop = $('#txt-crop').val();
//    var Code = $('#txtCode').val();
//    var frm_delete = $('<form action="/RedryCharge/DeleteRedryCharge" method="post"/>');
//    $('body').append(frm_delete);
//    frm_delete.append($('<input name="Code" type="hidden" value="' + Code + '" />'));
//    frm_delete.append($('<input name="Crop" type="hidden" value="' + Crop + '" />'));
//    frm_delete.append($('<input name="type" type="hidden" value="' + change_action + '" />'));
//    frm_delete.submit();
//});

//$('#btn-submit-redrycharge').click(function () {
//    var Crop = $('#txt-crop').val();
//    var Code = $('#txtCode').val();
//    var Desc = $('#txtDescription').val();
//    var Cost = $('#txtUnit').val();
//    var frm_submit = $('<form action="/RedryCharge/SubmitRedryCharge" method="post"/>');
//    $('body').append(frm_submit);
//    frm_submit.append($('<input name="Code" type="hidden" value="' + Code + '" />'));
//    frm_submit.append($('<input name="Crop" type="hidden" value="' + Crop + '" />'));
//    frm_submit.append($('<input name="type" type="hidden" value="' + change_action + '" />'));
//    frm_submit.append($('<input name="Desc" type="hidden" value="' + Desc + '" />'));
//    frm_submit.append($('<input name="Cost" type="hidden" value="' + Cost + '" />'));
//    frm_submit.submit();
//});


//$('#redryC-modal').on('hidden.bs.modal', function (e) {
//    $('#frm-setup-redryC')[0].reset();
//})
//$('#btn-redryC-modal-close').click(function () {
//    $('#redryC-modal').hide();
//})
//$('#btn-redryC-modal-close2').click(function () {
//    $('#redryC-modal').hide();
//})
//$('#txtCode').click(function () {
//    $('#btn-submit-redrycharge').removeClass('disabled');
//})

function adjust_table_by_size() {
    var screen_width = $(window).width();
    if (screen_width < 768) {
        //extra small device
        adjust_table_column("xs");
    }
    else if (screen_width < 992) {
        //small device
        adjust_table_column("sm");
    }
    else if (screen_width < 1200) {
        //medium device
        adjust_table_column("md");
    }
}

function adjust_table_column(size) {
    var hide_column = [];
    if (size == "md") {
        hide_column = [];
    }
    else if (size == "sm") {
        hide_column = [];
    }
    else if (size == "xs") {
        hide_column = [];
    }
    for (var i = 0; i < hide_column.length; i++) {
        table.bootstrapTable('hideColumn', hide_column[i]);
    }
}

function adjust_report_table() {
    var height = parseInt($('.hero').css('margin-top')) +
        $('.fixed-table-header').height() +
        $(".filterBox").outerHeight() +
        $('.clearfix').height() +
        $('.navbar-fixed-bottom').height();
    height = $(window).height() - height - 100;
    $('.fixed-table-body').height(height);
}

