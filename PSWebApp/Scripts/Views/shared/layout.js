﻿var user_balance = 0;
var ACTION = {
	MAP: 0,
	INCREASE_DEGREE: 1,
	DECREASE_DEGREE: 2,
	EXPAND: 3,
	FIND_INTERCONNECT: 5,
	PROCUREMENT_CHECK: 6,
	RED_LIST_REPORT: 8,
	ENTITY_PROFILE_REPORT: 9
};
var GRAPH_TYPE = {
	SIMPLE_GRAPH: 0,
	INTERCONNECTION_GRAPH: 1, 
	LBO_GRAPH: 3,
	USH_GRAPH: 4
}
var uncollapse_pages = ["Home/Index", "Account/LogOn"];
var unshow_pages = ["Account/NewRegister", "Account/Register", "Account/ForgotPassword", "Home/Terms", "Account/AcceptTermOfUse"];

$(document).ready(function () {
	arrange_header_menu();
	if ($('.nav-row-toggle').length > 0) {
		$('.nav-row-toggle').click(function () {
			if ($('.nav-menu-row').css('display') == 'none') {
				$('.nav-menu-row').slideDown('fast');
			} else {
				$('.nav-menu-row').slideUp('fast');
			}
		});
	}
	if ($('.switch-screen-mode').length > 0) {

	}
	//remove localStoreage
	if (window.location.pathname.split('/').indexOf('Users') < 0) {
		localStorage.removeItem('searchUser');
		localStorage.removeItem('statusUser');
	}

	oldOpenFunc = XMLHttpRequest.prototype.open;
	XMLHttpRequest.prototype.open = function () {
		localStorage.setItem("sessiontime", new Date())
		oldOpenFunc.apply(this, arguments);
	};

	//stamp time
	localStorage.setItem("sessiontime", new Date())
	var checker = setInterval(function () {
		session_checker(checker);
	}, 1000);

	$("#session-signout").click(function () {
		window.location.href = '/Account/LogOff';
	});
	$("#session-continue").click(function () {
		session_continue();
	});
});

$(window).resize(function () {
	arrange_header_menu();
});

function session_continue() {
	$.ajax({
		url: "/Home/ContinueSession",
	});
}
var userlogin = false
function session_checker(checker) {
	var session_time = $('#session-time').val();
	var isLogin = $('#is-login').val();

	if (isLogin == 'True') {
		var timeStamp = new Date(localStorage.getItem("sessiontime"));
		var timeDiff = ((new Date().getTime() - timeStamp.getTime()) / 1000); //second

		var leftTime = parseInt(session_time - timeDiff).toFixed(0);

		if (leftTime < 60 && leftTime >= 0) {
			$('#session-timeout-modal').modal('show');
			$('#session-countdown').html(leftTime);
		}
		else if (leftTime < 0) {
			$('#session-timeout-modal').modal('hide');
			$.ajax({
				url: "/Account/JsonLogOff",
				success: function (data) {
					clearInterval(checker);
					//bootbox.alert('<h4>Your session has expired.</h4>');
					$('#session-expired-modal').modal('show');
				}
			});
		}
	}
}



function arrange_header_menu() {
	var current_page = $('#current-page').val();
	if (unshow_pages.indexOf(current_page) == -1) {
		if (uncollapse_pages.indexOf(current_page) == -1 && $(window).width() > 964) {
			collapse_navbar();
		}
		else {
			show_navbar();
		}
	}
	if ($(window).height() > $(window).width()) {
		$('.nav-title-row').hide();
	}
	else {
		$('.nav-title-row').show();
	}
}

$(window).load(function () {
	update_user_credit_display();
});

//Model deduction-rate
$('#deduction-rate-modal').on('shown.bs.modal', function () {
	var tabs = $('#tabs-deduction');
	if (tabs.html() == "") {
		$.get('/Credit/JsonUserConnectionSources', function (data) {
			if (data.success) {
				var id;
				$('#connection-source').val('');
				data.sources.forEach(function (s, i) {
					var source = $('<li data-source="' + s.id + '"><a data-toggle="tab" href="#' + s.name + '">' + s.name + '</a></li>');
					tabs.append(source);
					if (id === undefined)
						id = s.id;
				});
				changeTab(id);
				$('li[data-source]').click(function () {
					var id = $(this).attr('data-source');
					$('#connection-source').val(id);
					changeTab(id)
				});
			}
		});
	}
});

function changeTab(sourceGuid) {
	$('li[data-source]').removeClass('active');
	$('li[data-source="' + sourceGuid + '"]').attr('class', 'active');
	var deduction_rate_url = '/Credit/JsonDeductionRates';
	$.ajax({
		url: deduction_rate_url,
		dataType: 'json',
		data: { sourceGuid: sourceGuid },
		success: function (data) {
			$('#deduction-rate-table > tbody').empty();
			$(data).each(function () {
				if (this.rate != 0 && this.action != "A P I E P Report Firm" && this.action != "A P I E P Report Person" && this.action != "A P I Benefical Ownership Report") {
					var deduction_rate = ['<tr><td>', this.action, '</td><td>', this.rate, '</td><td>Credits</td></tr>'];
					$('#deduction-rate-table > tbody').append(deduction_rate.join(''));
				}
			});
		}
	});
};
//End Model deduction-rate

$(document).unbind('keydown').bind('keydown', function (event) {
	var doPrevent = false;
	if (event.keyCode === 8) {
		var d = event.srcElement || event.target;
		if ((d.tagName.toUpperCase() === 'INPUT' &&
             (
                 d.type.toUpperCase() === 'TEXT' ||
                 d.type.toUpperCase() === 'PASSWORD' ||
                 d.type.toUpperCase() === 'FILE' ||
                 d.type.toUpperCase() === 'EMAIL' ||
                 d.type.toUpperCase() === 'SEARCH' ||
                 d.type.toUpperCase() === 'DATE')
             ) ||
             d.tagName.toUpperCase() === 'TEXTAREA') {
			doPrevent = d.readOnly || d.disabled;
		}
		else {
			doPrevent = true;
		}
	}
	if (doPrevent) {
		event.preventDefault();
	}
});
$('#frm-error').submit(function (e) {
	e.preventDefault();
	var form_data = new FormData();
	var files = $("#error-files").get(0).files;
	for (var i = 0; i < files.length; i++) {
		form_data.append("files", files[i]);
	}
	form_data.append('topic', $('#report-title').val());
	form_data.append('body', $('#report-details').val());
	var report_url = '/Home/Contact';
	var dialog;
	$.ajax({
		url: report_url,
		contentType: false,
		processData: false,
		data: form_data,
		type: "POST",
		beforeSend: function () {
			$('#report-modal').modal('hide');
			dialog = bootbox.dialog({
				message: '<h4 class="text-center">Sending Report...</h4>',
				closeButton: false
			});
		},
		success: function (data) {
			dialog.modal('hide');
			$('#frm-error')[0].reset();
			$('#error-files').MultiFile('reset');
			if (data.success)
				bootbox.alert('<h4>Report has been sent, Thank you for your feedback.</h4>');
			else
				bootbox.alert('<h4>Some issue occur, Please contect admin to solve this problem.</h4>');
		}
	});
});
function update_user_credit_display() {
	if ($('#user-credit-balance').length > 0) {
		var load_balance_url = '/Credit/JsonUserBalance';
		$.ajax({
			url: load_balance_url,
			dataType: 'json',
			success: function (data) {
				user_balance = parseFloat(data.credit.split(' ')[0]);
				update_credit_balance_value(user_balance);
			}
		});
	}
}
function update_credit_balance_value(balance) {
	if (is_number(balance)) {
		var balance = parseFloat(balance);
		if (balance < 0)
			$('#user-credit-balance').parent().attr('class', 'btn btn-danger');
		else if (balance < 100)
			$('#user-credit-balance').parent().attr('class', 'btn btn-primary');
	}
	$('#user-credit-balance').html(balance.toFixed(2) + " Credits");
}
function collapse_navbar() {
	$('.nav-menu-row').hide();
}
function show_navbar() {
	$('.nav-menu-row').show();
}
function is_number(n) {
	return !isNaN(parseFloat(n)) && isFinite(n);
}
function deduction_action_block(action, source, graph_type, callback) {
	var url = '/Credit/JsonDeductionCheck';
	var connection = {
		source: source,
		sources: ""
	}
	if (typeof source === 'object') {
		connection.source = source.guid;
		connection.sources = source.guids
	}

	var type = GRAPH_TYPE.SIMPLE_GRAPH;
	if (graph_type == "Legal Beneficial Ownership")
		type = GRAPH_TYPE.LBO_GRAPH;
	else if (graph_type == "Investee Companies")
		type = GRAPH_TYPE.USH_GRAPH;
	$.ajax({
		url: url,
		dataType: 'json',
		data: {
			deductionAction: action,
			source: connection.source,
			sources: connection.sources,
			graphType: type
		},
		success: function (data) {
			execute_deduction_action(data, callback);
		}
	});
}
function graph_deduction_action_block(action, source, entity_guids, graph_type, callback, cancel) {
	var url = '/Graph/JsonDeductionCheck';
	var connection = {
		source: source,
		sources: ""
	}
	if (typeof source === 'object') {
		connection.source = source.guid;
		connection.sources = source.guids
	}

	$.ajax({
		url: url,
		dataType: 'json',
		type: 'POST',
		data: {
			deductionAction: action,
			entityGuid: entity_guids,
			source: connection.source,
			sources: connection.sources,
			graphType: graph_type
		},
		success: function (data) {
			execute_deduction_action(data, callback, cancel);
		}
	});
}
function execute_deduction_action(data, callback, cancel) {
	if (data.success) {
		if (callback)
			callback(data);
	}
	else {
		if (data.type == "error")
			bootbox.alert(data.error);
		else if (data.type == "warning") {
			var json = JSON.parse(data.message);
			var isPostPaid = false;

			var messages = [];
			var rates = [];
			var balances = [];

			if (data.creditRole != null) {
				if (data.creditRole == "PostPaid") {
					isPostPaid = true;
				}
			}

			if (isPostPaid) {
				messages = ['<h4 class="text-center"><strong>The cost for this action will be as follows:</strong></h4>'];
				rates = ['<div class="row">'];
				var shortNames = { "CRMY": 25, "CM" : 40 };
				var total = 0;
				for (var i = 0; i < json.length; i++) {
					var result = json[i];
					var name = result.short_name;
					var rate = parseFloat(Object.keys(shortNames).indexOf(name) == -1 ? result.rate : shortNames[name]);
					total += parseFloat(rate);
					rates = rates.concat([
						'<div class="col-md-offset-3 col-md-3 text-left"><strong>' + name + '</strong></div>',
						'<div class="col-md-3 text-left">MYR ' + rate + '</div>'
					]);
				}
				rates = rates.concat([
						'<div class="col-md-offset-3 col-md-3 text-left" style="margin-top: 15px;"><strong>Total</strong></div>',
						'<div class="col-md-3 text-left" style="margin-top: 15px;">MYR ' + total.toFixed(0) + '</div>'
				]);
				rates.push('</div>');
			}
			else {
				messages = ['<h4 class="text-center"><strong>This action will deduct the following from your balance:</strong></h4>'];
				rates = ['<div class="row">'];
				balances = [
					'<table class="table table-condensed">',
					'<thead><tr><th width="40%"></th><th class="text-center" width="25%">Current Balance</th><th></th><th class="text-center" width="30%">New Balance</th></tr></thead>',
					'<tbody>'
				];

				var freebie_balances = [];
				var nodeduct_balances = [];
				var is_insufficient = false;
				for (var i = 0; i < json.length; i++) {
					var result = json[i];
					var action_text = "Prepaid Action";
					if (result.balance == "0")
						action_text = "Credits";
					if (result.action == 0)
						action_text = "";
					rates = rates.concat([
						'<div class="col-md-offset-3 col-md-3 text-left"><strong>' + result.short_name + '</strong></div>',
						'<div class="col-md-3 text-center">' + result.rate + ' ' + action_text + '</div>'
					]);
					if (result.action == 0) {
						nodeduct_balances = nodeduct_balances.concat([
						'<tr><td class="text-center"><strong>' + result.short_name + '</strong></td>',
						'<td class="text-center">Unlimited</td>',
						'<td class="text-center"><span style="color: #337ab7" class="glyphicon glyphicon-arrow-right"></span></td>',
						'<td class="text-center"><strong>Unlimited</strong></td></tr>']);
					}
					else {
						if (result.balance != "0") {
							var balance = result.balance;
							freebie_balances = freebie_balances.concat([
							'<tr><td class="text-center"><strong>' + result.action_text + ' [' + result.short_name + ']</strong></td>',
							'<td class="text-center">' + balance + '</td>',
							'<td class="text-center" style="color: #337ab7"><span class="glyphicon glyphicon-arrow-right"></span></td>',
							'<td class="text-center" style="color: red;"><strong>' + result.new_balance + '</strong></td></tr>']);
						}
					}
				}

				balances = balances.concat(freebie_balances);
				if (data.deduct > 0) {
					var balance = data.balance;
					var new_balance = balance - data.deduct;
					if (new_balance < 0) {
						new_balance = "Insufficient";
						is_insufficient = true;
					}
					else
						new_balance = new_balance.toFixed(2);
					if (is_insufficient)
						new_balance = '<span>' + new_balance + '</span>';
					balances = balances.concat([
						'<tr><td class="text-center"><strong>Credit</strong></td>',
						'<td class="text-center">' + balance.toFixed(2) + '</td>',
						'<td class="text-center" style="color: #337ab7"><span class="glyphicon glyphicon-arrow-right"></span></td>',
						'<td class="text-center" style="color: red;"><strong>' + new_balance + '</strong></td></tr>']);
				}
				balances = balances.concat(nodeduct_balances);
				balances.push('</tbody></table>');
				rates.push('</div><hr style="margin-bottom: 0px;"/>');
			}

			var message = messages.join('') + rates.join('') + balances.join('');
			if (is_insufficient) {
				message = message + ['<div class="text-center" style="margin-top: 20px;">',
                    '<span style="color: red;"><strong>You have insufficient balance for this action.</strong><br/>',
                    '<strong>Kindly contact us at hotline 03-2722 8800 or email to<br/>customersupport@cmctos.com.my for further details.</strong></span></div>'].join('');
			}
			else
				message = message + '<div class="text-center" style="margin-top: 20px;"><span><strong>Would you like to proceed?</strong></span></div>';

			if (is_insufficient) {
				bootbox.alert({
					message: message,
					buttons: {
						ok: {
							label: 'Cancel',
							className: 'btn btn-default'
						}
					},
				});
			} else {
				bootbox.confirm({
					message: message,
					buttons: {
						confirm: {
							label: 'OK'
						}
					},
					callback: function (confirm) {
						if (confirm) {
							if (is_insufficient) {
								window.open('/Epayment', '_blank');
							}
							else {
								update_credit_balance_value(data.balance - data.deduct);
								if (callback)
									callback(data);
							}
						}
						else {
							if (cancel)
								cancel();
						}
					}
				});
			}
		}
	}
}

function color_distance(v1, v2) {
	var i,
        d = 0;
	for (i = 0; i < v1.length; i++) {
		d += (v1[i] - v2[i]) * (v1[i] - v2[i]);
	}
	return Math.sqrt(d);
};


function hexToRgb(hex) {
	var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
	hex = hex.replace(shorthandRegex, function (m, r, g, b) {
		return r + r + g + g + b + b;
	});
	var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
	var rgbResult = [];
	rgbResult.push(parseInt(result[1], 16));
	rgbResult.push(parseInt(result[2], 16));
	rgbResult.push(parseInt(result[3], 16));
	return rgbResult;;
}

function componentToHex(c) {
	var hex = c.toString(16);
	return hex.length == 1 ? "0" + hex : hex;
}

function rgbToHex(r, g, b) {
	return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
}

function compare_color_bw(strHex) {

	var rgbW = [255, 255, 255];
	var rgbB = [20, 25, 7];

	if (typeof strHex !== "undefined" && strHex != "") {
		var rgb = hexToRgb(strHex);
		var dW = color_distance(rgb, rgbW);
		var dB = color_distance(rgb, rgbB);

		if (dW >= dB)
			return rgbToHex(rgbW[0], rgbW[1], rgbW[2]);
		else
			return rgbToHex(rgbB[0], rgbB[1], rgbB[2]);
	}
	else {
		return rgbToHex(rgbW[0], rgbW[1], rgbW[2]);
	}
};
function dateSorter(a, b) {
	var dateA = toSorterDate(a);
	var dateB = toSorterDate(b);

	if (dateA < dateB) return 1;
	if (dateA > dateB) return -1;
	return 0;
}
function toSorterDate(dateString) {
	var dates = dateString.split('/');
	var newDate = dates.filter(function (e, idx) { return idx < 2 }).reverse().concat(dates[2]).join('-');
	return new Date(newDate);
}
function dateFormatter(value) {
	var dates = value.split('/');
	var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

	if (dates.length > 1) {
		var result = dates[0] + " " + months[(parseInt(dates[1], 10) - 1)] + " " + dates[2];
		return result;
	}
	return value;
}