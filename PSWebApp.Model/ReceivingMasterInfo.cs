﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSWebApp.Models
{
    [Serializable]
    public class ReceivingMasterInfo
    {
        public string C_REC_DOC_NO { get; set; }
        public string C_REC_NO { get; set; }
        public int N_CROP { get; set; }
        public DateTime D_REC_DOC_DATE { get; set; }
        public string C_BUY_DOC_NO { get; set; }
        public string C_COMPANY { get; set; }
        public string C_TYPE { get; set; }
        public string C_SEASON { get; set; }
        public string C_CURER_CODE { get; set; }
        public string C_CURER_NAME { get; set; }
        public string C_AREA { get; set; }
        public string C_GREEN_PRICE_CODE { get; set; }
        public string C_REC_TYPE_RB { get; set; }
        public string C_PROG_ID { get; set; }

    }
}