﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSWebApp.Model
{
    public class ExchangeRateInfo
    {
        public string EffectiveDate { get; set; }
        public decimal ExchangeRate { get; set; }
    }
}