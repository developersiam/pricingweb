﻿using System;

namespace PSWebApp.Model
{
    [Serializable]
    public class RedryChargeInfo
    {
        public int Crop { get; set; }
        public string Redry_Charge_Code { get; set; }
        public string Redry_Charge_Description { get; set; }
        public decimal Charge { get; set; }
    }

    [Serializable]
    public class TransportChargeInfo
    {
        public int Crop { get; set; }
        public string Trans_Charge_Code { get; set; }
        public string Trans_Charge_Description { get; set; }
        public decimal Charge { get; set; }
    }
}