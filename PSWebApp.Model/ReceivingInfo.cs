﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSWebApp.Models
{
    [Serializable]
    public class ReceivingInfo
    {
        public string C_REC_DOC_NO { get; set; }
        public string C_REC_NO { get; set; }
        public int N_CROP { get; set; }
        public DateTime D_REC_DOC_DATE { get; set; }
        public string C_BUY_DOC_NO { get; set; }
        public string C_COMPANY { get; set; }
        public string C_REC_TYPE_RB { get; set; }
        public string C_CR_BY { get; set; }
        public string C_TYPE { get; set; }
        public DateTime D_CR_DATE { get; set; }
        public string C_SEASON { get; set; }
        public string C_CURER_CODE { get; set; }
        public string C_GREEN_PRICE_CODE { get; set; }
        public string C_CURER_NAME { get; set; }
        public string C_AREA { get; set; }
        public DateTime D_UPD_DATE { get; set; }
        public string C_UPD_BY { get; set; }
        public string C_PROG_ID { get; set; }
    }

    public class CropReceiving
    {
        public int N_CROP { get; set; }
    }

    [Serializable]
    public class MatReceivingInfo
    {
        public string ReceivingNo { get; set; }
        public string DocumentNo { get; set; }
        public string BuyingDocNo { get; set; }
        public DateTime DocumentDate { get; set; }
        public string CompanyCode { get; set; }
        public int Crop { get; set; }
        public string Type { get; set; }
        public string Season { get; set; }
        public string CurerCode { get; set; }
        public string CurerName { get; set; }
        public string Area { get; set; }
        public string RecordType { get; set; }
        public string GreenPriceCode { get; set; }
        public double WeightBuy { get; set; }
        public double WeightReceived { get; set; }
        public double AveragePriceBuy { get; set; }
        public double AveragePriceRec { get; set; }
        public double TotalAmount { get; set; }
    }

    //public class ReceivingListNotDone
    //{
    //    public int Crop { get; set; }
    //    public int DocNo { get; set; }
    //    public int CBale { get; set; }
    //}


    [Serializable]
    public class ReceivingDetails
    {
        public string BaleNo { get; set; }             //bale no , C_BALE_NO                                             
        public string GreenGrade { get; set; }   //green  , C_GREEN_GRADE                                              
        public string ClassifiedGrade { get; set; } //classify, C_CLASSIFIED_GRADE
        public string BuyWeight { get; set; } //BuyWeight, N_BUY_WEIGHT
        public string Weight { get; set; } //RecWeight,N_WEIGHT
        public string UnitPrice { get; set; }  //Unit Price,N_UNIT_PRICE
        public string TCharge { get; set; }  //Tcharge,N_T_CHARGE
        public string TotalUnitPrice { get; set; } //UnitPrice + TCharge,N_TOTAL_UNIT_PRICE
        public string Amount { get; set; } //price,N_AMOUNT
        public string FCVClass { get; set; } //FCV,C_FCV_CLASS

    }
}