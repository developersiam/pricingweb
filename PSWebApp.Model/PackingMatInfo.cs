﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSWebApp.Model
{
    public class PackingMatInfo
    {
        public int Crop { get; set; }
        public string Packing_Mat { get; set; }
        public decimal Price { get; set; }
    }
}