﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSWebApp.Model
{
    public class ExportValueInfo
    {
        //For Packed
        public int Crop { get; set; }
        public string PackedGrade { get; set; }
        public string Customer { get; set; }
        public string CustomerGrade { get; set; }
        public decimal Value { get; set; }

    }
    [Serializable]
    public class ClassifiedExportValueInfo
    {
        public int Crop { get; set; }
        public string ClassifiedGrade { get; set; }
        public decimal Value { get; set; }
    }

    [Serializable]
    public class PackedGradeCustomerInfo
    {
        public string PackedGrade { get; set; }
        public string CustomerName { get; set; }
        public string CustomerGrade { get; set; }
    }

    [Serializable]
    public class PackedGradeUnitWeightInfo
    {
        public string UnitPrice { get; set; }
        public string AvgWeight { get; set; }
    }

    [Serializable]
    public class PackedInfo
    {
        public string PackedGrade { get; set; }
    }
    [Serializable]
    public class GraderInfo
    {
        public string CustomerGrade { get; set; }
    }
    [Serializable]
    public class CustomerInfo
    {
        public string CustomerName { get; set; }
    }
}