﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSWebApp.Model
{
    [Serializable]
    public class ProcessReadyMasterInfo
    {
        public string Doc_No { get; set; }
        public string ProcessingR_No { get; set; }
        public int Crop { get; set; }
        public DateTime Doc_Date { get; set; }
        public string Packed_Grade { get; set; }
        public string Type { get; set; }
        public string Season { get; set; }
        public decimal Packed_Weight { get; set; }
        public decimal Weight { get; set; }
        public decimal Redry_Charge { get; set; }
        public decimal Pack_Mat { get; set; }
        public decimal Transportation { get; set; }
    }
    [Serializable]
    public class PDProcessReadyMasterInfo
    {
        public string Doc_No { get; set; }
        public string ProcessingR_No { get; set; }
        public int Crop { get; set; }
        public DateTime Doc_Date { get; set; }
        public string Packed_Grade { get; set; }
        public string Type { get; set; }
        public string Season { get; set; }
        public decimal Packed_Weight { get; set; }
        public decimal Weight { get; set; }
        public decimal Redry_Charge { get; set; }
        public decimal Pack_Mat { get; set; }
        public decimal Transportation { get; set; }
        public decimal Total_Amt { get; set; }
        public decimal Total_Weight { get; set; }
        public decimal Average_Price { get; set; }
    }
    [Serializable]
    public class ProcessReadyDetails
    {
        public string BaleNo { get; set; }
        public string GreenGrade { get; set; }
        public string PackedGrade { get; set; }
        public string Weight { get; set; }
        public string UnitPrice { get; set; }
        public string GEP { get; set; }
        public string GEP_Per { get; set; }
        public string ToTalUnit { get; set; }
        public string Amount { get; set; }
        public string Flag { get; set; }
    }
}