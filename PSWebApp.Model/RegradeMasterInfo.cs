﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSWebApp.Model
{
    [Serializable]
    public class RegradeMasterInfo
    {
        public string Regrade_Doc_No { get; set; }
        public string Regrade_No { get; set; }
        public int Crop { get; set; }
        public DateTime Regrade_Doc_Date { get; set; }
        public string Company { get; set; }
        public string Type { get; set; }
        public string Season { get; set; }
    }

    [Serializable]
    public class MatIsRegradeInfo
    {
        public string RegradeNo { get; set; }
        public string DocumentNo { get; set; }
        public DateTime DocumentDate { get; set; }
        public string CompanyCode { get; set; }
        public int Crop { get; set; }
        public string Type { get; set; }
        public string Season { get; set; }
        public string TotalWeight { get; set; }
        public string AveragePrice { get; set; }
        public string TotalAmount { get; set; }
    }

    [Serializable]
    public class RegradeDetails
    {
        public string BaleNo { get; set; }
        public string GreenGrade { get; set; }
        public string Classified { get; set; }
        public string Weight { get; set; }
        public string UnitPrice { get; set; }
        public string Amount { get; set; }
        public string FlagIO { get; set; }

    }
}