﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSWebApp.Model
{
    [Serializable]
    public class TypeInfo
    {
        public string TypeName { get; set; }
        public string Description { get; set; }
    }

    [Serializable]
    public class TypeRBInfo
    {
        public string TypeName { get; set; }
        public string Description { get; set; }
    }
}