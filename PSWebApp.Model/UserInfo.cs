﻿using System;

namespace PSWebApp.Models
{
    [Serializable]
    public class UserInfo
    {
        public string C_USER_LOGIN { get; set; }
        public string C_NAME { get; set; }
        public string C_PASSWORD { get; set; }
        public string C_USER_GROUP_CODE { get; set; }
        public string C_CR_BY { get; set; }
        public DateTime D_CR_DATE { get; set; }
        public string C_UPD_BY { get; set; }
        public DateTime D_UPD_DATE { get; set; }
        public string C_PROG_ID { get; set; }
    }
}