﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSWebApp.Model
{
    [Serializable]
    public class RedryCodeInfo
    {
        public string PackedGrade { get; set; }
        public string Type { get; set; }
        public string Customer { get; set; }
        public string Form { get; set; }
        public decimal Netdef { get; set; }
        public decimal Taredef { get; set; }
        public decimal Grossdef { get; set; }
        public decimal Gep { get; set; }
        public decimal GepBaht { get; set; }
        public string RedryCode { get; set; }
        public decimal RedryCharge { get; set; }
        public decimal Transport { get; set; }
        public string TranspostCode { get; set; }
        public decimal Price { get; set; }
        public int Crop { get; set; }

    }
    [Serializable]
    public class TransportationInfo
    {
        public string TransportCode { get; set; }
        public decimal TransportCharge { get; set; }
    }
    [Serializable]
    public class RedryInfo
    {
        public string RedryCode { get; set; }
        public decimal RedryCharge { get; set; }
    }
    [Serializable]
    public class PackedGradeInfo
    {
        public string PackedGrade { get; set; }
    }
}