﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using PSWebApp.Models;
using PSWebApp.Model;
using System.Data;
using System.Data.SqlClient;

namespace PSWebApp.Repository
{
    public class RegradeRepository : PSWebRepository
    {
        string constr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        public List<CropReceiving> GetCorp()
        {
            var command = new SqlCommand("sp_PS_Get_CorpReceiving") { CommandType = CommandType.StoredProcedure };
            var results = ExecuteCommand(command, new string[] { "crop" });
            if (!results.Any())
                return new List<CropReceiving>();

            return results.Select(c => new CropReceiving()
            {
                N_CROP = Convert.ToInt16(c["crop"])
            }).ToList();
        }
        //----------------------- Get List of Regrade Main ----------------------------
        public List<RegradeMasterInfo> GetJsonRegradeMaster(string Crop)
        {
            var command = new SqlCommand("sp_PS_Get_RegradeByCorp") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@crop", Crop));

            var data = ExecuteCommand(command, new string[] { "C_REGRADE_NO", "C_REGRADE_DOC_NO", "D_REGRADE_DOC_DATE", "C_COMPANY", "N_CROP", "C_TYPE", "C_SEASON" });
            List<RegradeMasterInfo> lstRegradeM = new List<RegradeMasterInfo>();

            foreach (var r in data)
            {
                lstRegradeM.Add(new RegradeMasterInfo()
                {
                    Regrade_Doc_No = r["C_REGRADE_DOC_NO"],
                    Regrade_No = r["C_REGRADE_NO"],
                    Regrade_Doc_Date = DateTime.Parse(r["D_REGRADE_DOC_DATE"]),
                    Company = r["C_COMPANY"],
                    Crop = Int16.Parse(r["N_CROP"]),
                    Type = r["C_TYPE"],
                    Season = r["C_SEASON"]
                });
            }
            return lstRegradeM;
        }
        //Get New Hand Strip
        public List<RegradeMasterInfo> GetJsonAddRegradeMaster(int Crop)
        {
            var command = new SqlCommand("sp_PS_Get_RegradeMasterNotDone") { CommandType = CommandType.StoredProcedure };

            var data = ExecuteCommand(command, new string[] { "C_REGRADE_NO", "C_REGRADE_DOC_NO", "D_REGRADE_DOC_DATE", "C_COMPANY", "N_CROP", "C_TYPE", "C_SEASON" });
            List<RegradeMasterInfo> lstRegradeM = new List<RegradeMasterInfo>();
            foreach (var r in data)
            {
                lstRegradeM.Add(new RegradeMasterInfo()
                {
                    Regrade_Doc_No = r["C_REGRADE_DOC_NO"],
                    Regrade_No = r["C_REGRADE_NO"],
                    Regrade_Doc_Date = DateTime.Parse(r["D_REGRADE_DOC_DATE"]),
                    Company = r["C_COMPANY"],
                    Crop = Int16.Parse(r["N_CROP"]),
                    Type = r["C_TYPE"],
                    Season = r["C_SEASON"]
                });
            }
            return lstRegradeM;
        }
        //---------------------------- Regrade Details -----------------------------------------------------
        public MatIsRegradeInfo GetRegradeCalculate(string RGNo)
        {
            var command = new SqlCommand("sp_PS_Get_MatISRegradeByRGNo") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@RGNO", RGNo));

            var rg = ExecuteCommand(command, new string[] { "RegradeNo", "DocumentNo", "DocumentDate", "CompanyCode", "Crop", "Type", "Season", "TotalWeight", "AveragePrice", "TotalAmount" });
            if (!rg.Any())
                return null;
            return new MatIsRegradeInfo()
            {
                RegradeNo = rg[0]["RegradeNo"],
                DocumentNo = rg[0]["DocumentNo"],
                DocumentDate = DateTime.Parse(rg[0]["DocumentDate"].ToString()),
                CompanyCode = rg[0]["CompanyCode"],
                Crop = Int16.Parse(rg[0]["Crop"]),
                Type = rg[0]["Type"],
                Season = rg[0]["Season"],
                TotalWeight = Double.Parse(rg[0]["TotalWeight"]).ToString("#,##0.00"),
                AveragePrice = Double.Parse(rg[0]["AveragePrice"]).ToString("#,##0.00"),
                TotalAmount = Double.Parse(rg[0]["TotalAmount"]).ToString("#,##0.00")
            };
        }
        public List<RegradeDetails> GetJsonRegradeDetails(string RGNo)
        {
            var command = new SqlCommand("sp_Sel_REGRADE_MAT") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@RegradeNo", RGNo));

            var results = ExecuteCommand(command, new string[] { "Bale No", "Green Grade", "Classified Grade", "FLAG_IO", "Weight", "Unit Price", "Amount", "bc", "rgno", "rcno", "docno", "crop", "company", "supplier", "type", "subtype", "PriceCode", "FCV", "isno" });
            List<RegradeDetails> lstRegradeD = new List<RegradeDetails>();
            foreach (var r in results)
            {
                lstRegradeD.Add(new RegradeDetails()
                {
                    BaleNo = r["Bale No"],
                    GreenGrade = r["Green Grade"],
                    Classified = r["Classified Grade"],
                    FlagIO = r["FLAG_IO"],
                    UnitPrice = Convert.ToString(Math.Round(double.Parse(r["Unit Price"]), 2)),
                    Weight = Convert.ToString(Math.Round(double.Parse(r["Weight"]), 2)),
                    Amount = Convert.ToString(Math.Round(double.Parse(r["Amount"]), 2))
                });
            }
            return lstRegradeD;
        }
        //----------- Delete Regrade ------------------
        public void DeleteRegrade(string RGDocNo)
        {
            var command = new SqlCommand("sp_Del_PS_REGRADE_M") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@C_REGRADE_DOC_NO", RGDocNo));
            ExecuteCommand(command, new string[] { });
        }
        //----------- Adding New Hand Strip -------------
        public string GetDocumentNo(int crop)
        {
            string DocumentNo;
            //Update to running no.
            var cmd = new SqlCommand("sp_Upd_Doc_Sequence") { CommandType = CommandType.StoredProcedure };
            cmd.Parameters.Add(new SqlParameter("@N_CROP", crop));
            cmd.Parameters.Add(new SqlParameter("@C_MODULE_CODE", "REG"));
            ExecuteCommand(cmd, new string[] { });

            var command = new SqlCommand("sp_Sel_Doc_Sequence") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@N_CROP", crop));
            command.Parameters.Add(new SqlParameter("@C_MODULE_CODE", "REG"));
            var newDocNo = ExecuteCommand(command, new string[] { "N_CROP", "C_MODULE_CODE", "N_RUN_NO" });

            if (!newDocNo.Any())
                return string.Empty;
            else
                DocumentNo = newDocNo[0]["N_CROP"] + newDocNo[0]["C_MODULE_CODE"] + Int32.Parse(newDocNo[0]["N_RUN_NO"]).ToString("D6");
            return DocumentNo;
        }
        public void AddRegrade(string DocNo, DateTime DocDate, string RGNo, int Crop, string Company, string Type, string Season, string User)
        {
            using (var cmd = new SqlCommand("sp_Ins_PS_REGRADE_M"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@C_REGRADE_DOC_NO", DocNo);
                cmd.Parameters.AddWithValue("@D_REGRADE_DOC_DATE", DocDate);
                cmd.Parameters.AddWithValue("@C_REGRADE_NO", RGNo);
                cmd.Parameters.AddWithValue("@N_CROP", Crop);
                cmd.Parameters.AddWithValue("@C_COMPANY", Company);
                cmd.Parameters.AddWithValue("@C_TYPE", Type);
                cmd.Parameters.AddWithValue("@C_SEASON", Season);
                cmd.Parameters.AddWithValue("@C_CR_BY", User);
                cmd.Parameters.AddWithValue("@D_CR_DATE", DateTime.Now);
                cmd.Parameters.AddWithValue("@C_UPD_BY", User);
                cmd.Parameters.AddWithValue("@D_UPD_DATE", DateTime.Now);
                cmd.Parameters.AddWithValue("@C_PROG_ID", "FrmRegrade");
                ExecuteCommand(cmd, new string[] { });
            }
        }
    }
}