﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace PSWebApp.Repository.Helper
{
    public class PasswordHasher
    {
        private const int SALT_SIZE = 8;
        private const int B64_SIZE = 12;
        private readonly string HASH_ALGORITHM;

        public PasswordHasher(string hashAlgorithm)
        {
            if (hashAlgorithm == null)
                throw new ArgumentNullException("hashAlgorithm");
            this.HASH_ALGORITHM = hashAlgorithm;
        }

        private string getNewSalt()
        {
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] buffer = new byte[SALT_SIZE];
            rng.GetBytes(buffer);
            string salt = Convert.ToBase64String(buffer);
            return salt;
        }

        public string HashPassword(string password)
        {
            if (string.IsNullOrEmpty(password))
                throw new ArgumentNullException("password");
            return hashPassword(password, getNewSalt());
        }

        private string hashPassword(string password, string salt)
        {
            string saltedPassword = password + salt;
            byte[] plainBytes = Encoding.UTF8.GetBytes(saltedPassword);
            HashAlgorithm hasher = HashAlgorithm.Create(HASH_ALGORITHM);
            if (hasher == null)
                throw new Exception("The provided hash algorithm is invalid");
            byte[] hashedBytes = hasher.ComputeHash(plainBytes);
            string hash = Convert.ToBase64String(hashedBytes);
            string saltedHash = hash + salt;
            return saltedHash;
        }

        public bool ValidatePassword(string password, string hashedPassword)
        {
            string salt = hashedPassword.Substring(hashedPassword.Length - B64_SIZE);
            string saltedHash = hashPassword(password, salt);
            bool v = saltedHash == hashedPassword;
            return v;
        }


    }
}