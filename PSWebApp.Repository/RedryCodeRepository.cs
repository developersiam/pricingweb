﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using PSWebApp.Models;
using PSWebApp.Model;
using System.Data;
using System.Data.SqlClient;

namespace PSWebApp.Repository
{
    public class RedryCodeRepository : PSWebRepository
    {
        string constr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        decimal Net = 0;
        decimal Tare = 0;
        decimal Gross = 0;
        decimal Gep = 0;
        decimal GepBaht = 0;
        decimal RedryC = 0;
        decimal TransC = 0;
        decimal Price = 0;
        public List<CropReceiving> GetCorp()
        {
            var command = new SqlCommand("sp_PS_Get_CropPackedGrade") { CommandType = CommandType.StoredProcedure };
            var results = ExecuteCommand(command, new string[] { "crop" });
            if (!results.Any())
                return new List<CropReceiving>();

            return results.Select(c => new CropReceiving()
            {
                N_CROP = Convert.ToInt16(c["crop"])
            }).ToList();
        }
        //----------------------- Get List of Redry Code Main ----------------------------
        public List<RedryCodeInfo> GetJsonRedryCodeMaster(string Crop)
        {
            var command = new SqlCommand("sp_PS_Get_PackedGradeByCrop") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@crop", Int16.Parse(Crop)));
            var data = ExecuteCommand(command, new string[] { "packedgrade", "type", "customer", "form", "netdef", "taredef", "grossdef", "gep", "gepbaht", "redrycode", "redrycharge", "transport", "transportcode", "price", "crop" });
            List<RedryCodeInfo> lstRedryCodeM = new List<RedryCodeInfo>();

            foreach (var r in data)
            {
                if (decimal.TryParse(r["netdef"], out Net))
                    Net = Decimal.Parse(r["netdef"]); else Net = 0;
                if (decimal.TryParse(r["taredef"], out Tare))
                    Tare = Decimal.Parse(r["taredef"]); else Tare = 0;
                if (decimal.TryParse(r["grossdef"], out Gross))
                    Gross = Decimal.Parse(r["grossdef"]); else Gross = 0;
                if (decimal.TryParse(r["gep"], out Gep))
                    Gep = Decimal.Parse(r["gep"]); else Gep = 0;
                if (decimal.TryParse(r["gepbaht"], out GepBaht))
                    GepBaht = Decimal.Parse(r["gepbaht"]); else GepBaht = 0;
                if (decimal.TryParse(r["redrycharge"], out RedryC))
                    RedryC = Decimal.Parse(r["redrycharge"]); else RedryC = 0;
                if (decimal.TryParse(r["transport"], out TransC))
                    TransC = Decimal.Parse(r["transport"]); else TransC = 0;
                if (decimal.TryParse(r["price"], out Price))
                    Price = Decimal.Parse(r["price"]); else Price = 0;

                lstRedryCodeM.Add(new RedryCodeInfo()
                {
                    PackedGrade = r["packedgrade"],
                    Type = r["type"],
                    Customer = r["customer"],
                    Form = r["form"],
                    Netdef = Net,
                    Taredef = Tare,
                    Grossdef = Gross,
                    Gep = Gep,
                    GepBaht = GepBaht,
                    RedryCode = r["redrycode"],
                    RedryCharge = RedryC,
                    Transport = TransC,
                    Crop = Int16.Parse(r["crop"]),
                    TranspostCode = r["transportcode"],
                    Price = Price
                });
            }
            return lstRedryCodeM;
        }
        //For ByProduct
        public List<RedryCodeInfo> GetJsonRedryCodeByProduct(string Crop)
        {

            var command = new SqlCommand("sp_PS_Get_PackedGradeProductByCrop") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@crop", Int16.Parse(Crop)));
            var data = ExecuteCommand(command, new string[] { "packedgrade", "type", "customer", "form", "netdef", "taredef", "grossdef", "gep", "gepbaht", "redrycode", "redrycharge", "transport", "transportcode", "price", "crop" });
            List<RedryCodeInfo> lstRedryCodeM = new List<RedryCodeInfo>();
            foreach (var r in data)
            {
                if (decimal.TryParse(r["netdef"], out Net))
                    Net = Decimal.Parse(r["netdef"]); else Net = 0;
                if (decimal.TryParse(r["taredef"], out Tare))
                    Tare = Decimal.Parse(r["taredef"]); else Tare = 0;
                if (decimal.TryParse(r["grossdef"], out Gross))
                    Gross = Decimal.Parse(r["grossdef"]); else Gross = 0;
                if (decimal.TryParse(r["gep"], out Gep))
                    Gep = Decimal.Parse(r["gep"]); else Gep = 0;
                if (decimal.TryParse(r["gepbaht"], out GepBaht))
                    GepBaht = Decimal.Parse(r["gepbaht"]); else GepBaht = 0;
                if (decimal.TryParse(r["redrycharge"], out RedryC))
                    RedryC = Decimal.Parse(r["redrycharge"]); else RedryC = 0;
                if (decimal.TryParse(r["transport"],out TransC))
                    TransC = Decimal.Parse(r["transport"]); else TransC = 0;
                if (decimal.TryParse(r["price"], out Price)) 
                    Price = Decimal.Parse(r["price"]); else Price = 0;

                lstRedryCodeM.Add(new RedryCodeInfo()
                {
                    PackedGrade = r["packedgrade"],
                    Type = r["type"],
                    Customer = r["customer"],
                    Form = r["form"],
                    Netdef = Net,
                    Taredef = Tare,
                    Grossdef = Gross,
                    Gep = Gep,
                    GepBaht = GepBaht,
                    RedryCode = r["redrycode"],
                    RedryCharge = RedryC,
                    Transport = TransC,
                    Crop = Int16.Parse(r["crop"]),
                    TranspostCode = r["transportcode"],
                    Price = Price
                });
            }
            return lstRedryCodeM;
        }
        //----------------------------Redry Code Details --------------------------
        public RedryCodeInfo GetRedryCodeExisted(int Crop, string PackedGrade)
        {

            var command = new SqlCommand("sp_PS_Get_PackedGradeByGradeCrop") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@crop", Crop));
            command.Parameters.Add(new SqlParameter("@packedGrade", PackedGrade));

            var data = ExecuteCommand(command, new string[] { "packedgrade", "type", "customer", "form", "netdef", "taredef", "grossdef", "gep", "gepbaht", "redrycode", "redrycharge", "transport", "transportcode", "price", "crop" });
            if (!data.Any())
                return null;
            foreach (var r in data)
            {
                if (decimal.TryParse(r["netdef"], out Net))
                    Net = Decimal.Parse(data[0]["netdef"]);
                else Net = 0;
                if (decimal.TryParse(r["taredef"], out Tare))
                    Tare = Decimal.Parse(r["taredef"]);
                else Tare = 0;
                if (decimal.TryParse(r["grossdef"], out Gross))
                    Gross = Decimal.Parse(r["grossdef"]);
                else Gross = 0;
                if (decimal.TryParse(r["gep"], out Gep))
                    Gep = Decimal.Parse(r["gep"]);
                else Gep = 0;
                if (decimal.TryParse(r["gepbaht"], out GepBaht))
                    GepBaht = Decimal.Parse(r["gepbaht"]);
                else GepBaht = 0;
                if (decimal.TryParse(r["redrycharge"], out RedryC))
                    RedryC = Decimal.Parse(r["redrycharge"]);
                else RedryC = 0;
                if (decimal.TryParse(r["transport"], out TransC))
                    TransC = Decimal.Parse(r["transport"]);
                else TransC = 0;
                if (decimal.TryParse(r["price"], out Price))
                    Price = Decimal.Parse(r["price"]);
                else Price = 0;
            }
            return new RedryCodeInfo()
            {              
                PackedGrade = data[0]["packedgrade"],
                Type = data[0]["type"],
                Customer = data[0]["customer"],
                Form = data[0]["form"],
                Netdef = Net,
                Taredef = Tare,
                Grossdef = Gross,
                Gep = Gep,
                GepBaht = GepBaht,
                RedryCode = data[0]["redrycode"],
                RedryCharge = RedryC,
                Transport = TransC,
                Crop = Int16.Parse(data[0]["crop"]),
                TranspostCode = data[0]["transportcode"],
                Price = Price
            };
        }

        //--------------------------- Get Charge Transport ------------------------
        public List<TransportationInfo> GetTransportation(int crop)
        {
            var command = new SqlCommand("sp_PS_Get_TransportationByCrop") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@crop", crop));
            var results = ExecuteCommand(command, new string[] { "C_TRAN_CHARGE_CODE", "N_CHARGE"});
            if (!results.Any())
                return new List<TransportationInfo>();

            return results.Select(c => new TransportationInfo()
            {
                TransportCode = c["C_TRAN_CHARGE_CODE"],
                TransportCharge = decimal.Parse(c["N_CHARGE"])
            }).ToList();
        }
        public decimal GetChargeTransportation(int crop,string TransportCode)
        {
            var Charge = GetTransportation(crop).Select(u => u.TransportCode == TransportCode);
            if (Charge.Any())
                return Convert.ToDecimal(0);
            //return Convert.ToDecimal(Charge.TransportCharge);
            return 0;
        }

        //--------------------------- Get Charge Redry ------------------------
        public List<RedryInfo> GetRedrying(int crop)
        {
            var command = new SqlCommand("sp_PS_Get_RedryByCrop") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@crop", crop));
            var results = ExecuteCommand(command, new string[] { "C_REDRY_CHARGE_CODE", "N_CHARGE" });
            if (!results.Any())
                return new List<RedryInfo>();

            return results.Select(c => new RedryInfo()
            {
                RedryCode = c["C_REDRY_CHARGE_CODE"],
                RedryCharge = decimal.Parse(c["N_CHARGE"])
            }).ToList();
        }
        public decimal GetChargeRedrying(int crop, string RedryCode)
        {
            var Charge = GetRedrying(crop).Select(u => u.RedryCode == RedryCode);
            if (Charge.Any())
                return Convert.ToDecimal(0);
            return 0;
        }
    }
}