﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace PSWebApp.Repository
{
    public class SessionRepository : IDisposable
    {
        SqlConnection _connection;
        public SessionRepository()
        {
            _connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            _connection.Open();
        }

        //public void AddUserSession(Guid userId, string sessionId)
        //{
        //    using (var cmd = new SqlCommand("Session_OnLogon", _connection) { CommandType = CommandType.StoredProcedure })
        //    {
        //        cmd.Parameters.AddWithValue("@UserGuid", userId);
        //        cmd.Parameters.AddWithValue("@SessionId", sessionId);
        //        cmd.ExecuteNonQuery();
        //    }
        //}

        //public int CountUserSession(Guid userId)
        //{
        //    using (var cmd = new SqlCommand("Session_CountNumberOfSession", _connection) { CommandType = CommandType.StoredProcedure })
        //    {
        //        cmd.Parameters.AddWithValue("@UserGuid", userId);
        //        cmd.Parameters.Add("@counter", SqlDbType.Int).Direction = ParameterDirection.Output;
        //        cmd.ExecuteNonQuery();
        //        int counter = Convert.ToInt32(cmd.Parameters["@counter"].Value);
        //        return counter;
        //    }
        //}

        //public void RemoveUserSession(string sessionId)
        //{
        //    using (var cmd = new SqlCommand("Session_OnLogoff", _connection) { CommandType = CommandType.StoredProcedure })
        //    {
        //        cmd.Parameters.AddWithValue("SessionId", sessionId);
        //        cmd.ExecuteNonQuery();
        //    }
        //}

        public void Dispose()
        {
            _connection.Close();
            _connection.Dispose();
        }
    }
}