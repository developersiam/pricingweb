﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Data;

namespace PSWebApp.Repository
{
    public static class DTHelper
    {
        private static PropertyInfo[] CreateTable<T>(ref DataTable dataTable)
        {
            Type type = typeof(T);
            var properties = type.GetProperties();

            if (dataTable == null)
            {
                dataTable = new DataTable();
                foreach (PropertyInfo info in properties)
                {
                    if (info.CanRead)
                        dataTable.Columns.Add(new DataColumn(info.Name, Nullable.GetUnderlyingType(info.PropertyType) ?? info.PropertyType));
                }
            }

            return properties;
        }

        private static void RemoveCol(List<string> removeCol, DataTable dataTable)
        {
            if (removeCol != null)
            {
                foreach (var name in removeCol)
                {
                    if (dataTable.Columns.Contains(name))
                        dataTable.Columns.Remove(name);
                }
            }
        }
        public static DataTable ConvertStringToDataTable(string[] values, string columnName)
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add(columnName, typeof(String));
            foreach (string item in values)
            {
                var value = item.Trim();
                if (!String.IsNullOrWhiteSpace(value))
                    dataTable.Rows.Add(value);
            }
            return dataTable;
        }

        public static DataTable ConvertPrimitiveToDataTable<T>(IEnumerable<T> list, string columnName)
        {
            DataTable dataTable = new DataTable();
            Type realType = Nullable.GetUnderlyingType(typeof(T)) ?? typeof(T);
            if (realType.IsClass)
            {
                throw new Exception("T is class. Please use method ConvertToDataTable instead.");
            }

            dataTable.Columns.Add(columnName, realType);
            foreach (T item in list)
            {
                dataTable.Rows.Add(item);
            }
            return dataTable;
        }

        public static DataTable ConvertToDataTable<T>(IEnumerable<T> list, DataTable dataTable = null, List<string> removeCol = null)
        {
            var realType = Nullable.GetUnderlyingType(typeof(T)) ?? typeof(T);
            if (!realType.IsClass)
            {
                throw new Exception("T is not class. Please use method ConvertPrimitiveToDataTable instead.");
            }

            PropertyInfo[] properties = CreateTable<T>(ref dataTable);

            if (list != null)
            {
                foreach (T entity in list)
                {
                    List<object> values = new List<object>();
                    foreach (var prop in properties)
                    {
                        if (prop.CanRead)
                            values.Add(prop.GetValue(entity));
                    }
                    dataTable.Rows.Add(values.ToArray());
                }
            }

            RemoveCol(removeCol, dataTable);
            return dataTable;
        }


        /// <summary>
        /// Convert object to DataTable 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <returns></returns>
        public static DataTable ConvertToDataTable<T>(List<IEnumerable<T>> list, List<string> removeCol = null)
        {
            DataTable dataTable = null;
            PropertyInfo[] properties = CreateTable<T>(ref dataTable);

            foreach (var items in list)
                dataTable = ConvertToDataTable(items, dataTable, null);

            RemoveCol(removeCol, dataTable);
            return dataTable;
        }
    }
}