﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PSWebApp.Models;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using PSWebApp.Model;

namespace PSWebApp.Repository
{
    public class ProcessProductRepository : PSWebRepository
    {
        string constr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        decimal Total_Cost = 0;
        decimal Total_Weight = 0;
        decimal Average_Price = 0;

        public List<CropReceiving> GetProcessingCorp()
        {
            var command = new SqlCommand("sp_PS_Get_CropProcess") { CommandType = CommandType.StoredProcedure };
            var results = ExecuteCommand(command, new string[] { "crop" });
            if (!results.Any())
                return new List<CropReceiving>();

            return results.Select(c => new CropReceiving()
            {
                N_CROP = Convert.ToInt16(c["crop"])
            }).ToList();
        }
        //----------------------- Get List of Processing By Product Main ----------------------------
        public List<ProcessProductMasterInfo> GetJsonProcessProduct(int Crop)
        {
            var command = new SqlCommand("sp_PS_Get_ProcessProductByCrop") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@Crop", Crop));
            var data = ExecuteCommand(command, new string[] { "Doc_No", "Doc_Date", "Crop", "Type", "Season", "Company", "ProcessingP_No" });
            List<ProcessProductMasterInfo> lstProduct = new List<ProcessProductMasterInfo>();

            foreach (var r in data)
            {

                lstProduct.Add(new ProcessProductMasterInfo()
                {
                    Doc_No = r["Doc_No"],
                    ProcessingP_No = r["ProcessingP_No"],
                    Doc_Date = DateTime.Parse(r["Doc_Date"]),
                    Crop = Int16.Parse(r["Crop"]),
                    Type = r["Type"],
                    Season = r["Season"],
                    Company = r["Company"]
                });
            }
            return lstProduct;
        }

        //Get New Processing Ready
        public List<ProcessProductMasterInfo> GetNewJsonProcessProduct()
        {
            string New_DocNo = "";
            string New_type = "";
            string New_Company = "";
            string New_Season = "";
            var command = new SqlCommand("sp_PS_Get_ProcessProductNotDone") { CommandType = CommandType.StoredProcedure };
            var data = ExecuteCommand(command, new string[] { "Doc_No", "Doc_Date", "Crop", "Type", "Season", "Company", "ProcessingP_No" });
            List<ProcessProductMasterInfo> lstProduct = new List<ProcessProductMasterInfo>();

            foreach (var r in data)
            {
                if (r["Doc_No"] == "")
                    New_DocNo = "";
                else
                    New_DocNo = r["Doc_No"];

                if (r["Type"] == "")
                    New_type = "";
                else
                    New_type = r["Type"];

                if (r["Company"] == "")
                    New_Company = "";
                else
                    New_Company = r["Company"];

                if (r["Season"] == "")
                    New_Season = "";
                else
                    New_Season = r["Season"];

                lstProduct.Add(new ProcessProductMasterInfo()
                {
                    Doc_No = New_DocNo,
                    ProcessingP_No = r["ProcessingP_No"],
                    Doc_Date = DateTime.Parse(r["Doc_Date"]),
                    Crop = Int16.Parse(r["Crop"]),
                    Type = New_type,
                    Season = New_Season,
                    Company = New_Company
                });
            }
            return lstProduct;
        }
        //---------------------------- Process Product Details -----------------------------------------------------
        public PDProcessProductMasterInfo GetProcessProductCalculate(string ProcessNo)
        {
            decimal cost = 0;
            decimal weight = 0;
            Total_Cost = 0;
            Total_Weight = 0;
            Average_Price = 0;


            var command = new SqlCommand("sp_PS_Get_PdSetupProcessProductByProcessNo") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@ProcessNo", ProcessNo));

            var gt = ExecuteCommand(command, new string[] { "DocumentNo", "DocDate", "ProcessNo", "Crop", "Company", "Type", "Season" });
            if (!gt.Any())
                return null;

            //Get Total amount
            var cmd = new SqlCommand("sp_Sel_PROCESS_BY_MAT") { CommandType = CommandType.StoredProcedure };
            cmd.Parameters.Add(new SqlParameter("@ProcessNo", ProcessNo));

            var data_amt = ExecuteCommand(cmd, new string[] { "bc", "crop", "company", "caseno", "Packed Grade", "Weight", "Unit Price", "Amount", "Packing Material", "PackCharge", "TransCode", "Transport Charge", "Total Cost", "box", "pdno" });

            foreach (var amt in data_amt)
            {
                if (decimal.TryParse(amt["Total Cost"], out cost))
                    cost = Decimal.Parse(amt["Total Cost"]);
                else cost = 0;
                if (decimal.TryParse(amt["Weight"], out weight))
                    weight = Decimal.Parse(amt["Weight"]);
                else weight = 0;

                Total_Cost = Total_Cost + cost;
                Total_Weight = Total_Weight + weight;
            }
            Average_Price = (Total_Cost / Total_Weight);


            return new PDProcessProductMasterInfo()
            {
                Crop = Int16.Parse(gt[0]["Crop"]),
                Type = gt[0]["Type"],
                Season = gt[0]["Season"],
                Company = gt[0]["Company"],
                ProcessingR_No = gt[0]["ProcessNo"],
                Doc_No = gt[0]["DocumentNo"],
                Doc_Date = DateTime.Parse(gt[0]["DocDate"].ToString()),
                Total_Cost = Math.Round(Total_Cost, 2),
                Total_Weight = Math.Round(Total_Weight, 2),
                Average_Price = Math.Round(Average_Price, 2)
            };
        }
        public List<ProcessProductDetails> GetJsonProcessPDetails(string ProcessNo)
        {
            var command = new SqlCommand("sp_Sel_PROCESS_BY_MAT") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@ProcessNo", ProcessNo));

            var results = ExecuteCommand(command, new string[] { "bc", "crop", "company", "caseno", "Packed Grade", "Weight", "Unit Price", "Amount", "Packing Material", "PackCharge", "TransCode", "Transport Charge", "Total Cost", "box", "pdno" });
            List<ProcessProductDetails> lstPP = new List<ProcessProductDetails>();
            foreach (var r in results)
            {

                lstPP.Add(new ProcessProductDetails()
                {
                    PackedGrade = r["Packed Grade"],
                    Weight = r["Weight"],
                    UnitPrice = r["Unit Price"],
                    Amount = r["Amount"],
                    PackingMaterial = r["Packing Material"],
                    Trans_Charge = r["Transport Charge"],
                    ToTalCost = r["Total Cost"]
                });
            }
            return lstPP;
        }
        //----------- Adding New ProcessReady -------------
        public string GetDocumentNo(int crop)
        {
            string DocumentNo;
            //Update to running no.
            var cmd = new SqlCommand("sp_Upd_Doc_Sequence") { CommandType = CommandType.StoredProcedure };
            cmd.Parameters.Add(new SqlParameter("@N_CROP", crop));
            cmd.Parameters.Add(new SqlParameter("@C_MODULE_CODE", "PDB"));
            ExecuteCommand(cmd, new string[] { });

            var command = new SqlCommand("sp_Sel_Doc_Sequence") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@N_CROP", crop));
            command.Parameters.Add(new SqlParameter("@C_MODULE_CODE", "PDB"));
            var newDocNo = ExecuteCommand(command, new string[] { "N_CROP", "C_MODULE_CODE", "N_RUN_NO" });

            if (!newDocNo.Any())
                return string.Empty;
            else
                DocumentNo = newDocNo[0]["N_CROP"] + newDocNo[0]["C_MODULE_CODE"] + Int32.Parse(newDocNo[0]["N_RUN_NO"]).ToString("D6");
            return DocumentNo;
        }
        public void AddProcessProduct(string DocNo, DateTime DocDate, string PPNo, int Crop,string Company, string Type, string Season, string User)
        {
            using (var cmd = new SqlCommand("sp_Ins_PS_PROCESS_BY_PRODUCT_M"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@C_PROCESS_DOC_NO", DocNo);
                cmd.Parameters.AddWithValue("@D_PROCESS_DOC_DATE", DocDate);
                cmd.Parameters.AddWithValue("@C_PROCESS_NO", PPNo);
                cmd.Parameters.AddWithValue("@N_CROP", Crop);
                cmd.Parameters.AddWithValue("@C_COMPANY", Company);
                cmd.Parameters.AddWithValue("@C_TYPE", Type);
                cmd.Parameters.AddWithValue("@C_SEASON", Season);
                cmd.Parameters.AddWithValue("@C_CR_BY", User);
                cmd.Parameters.AddWithValue("@D_CR_DATE", DateTime.Now);
                cmd.Parameters.AddWithValue("@C_UPD_BY", User);
                cmd.Parameters.AddWithValue("@D_UPD_DATE", DateTime.Now);
                cmd.Parameters.AddWithValue("@C_PROG_ID", "frmByProduct");
                ExecuteCommand(cmd, new string[] { });
            }
        }


        //----------- Delete ------------------
        public void DeleteProcessProduct(string PPDocNo)
        {
            var command = new SqlCommand("sp_Del_PS_PROCESS_BY_PRODUCT_M") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@C_PROCESS_DOC_NO", PPDocNo));
            ExecuteCommand(command, new string[] { });
        }

    }
}