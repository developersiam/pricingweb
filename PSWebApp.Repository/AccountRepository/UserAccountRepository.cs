﻿using PSWebApp.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Configuration;

namespace PSWebApp.Repository
{
    public partial class AccountRepository
    {
        string constr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        public UserInfo GetAuthentification(string username, string password)
        {
            using (SqlConnection con = new SqlConnection(constr))
            {
                con.Open();
                var command = new SqlCommand("sp_Sel_PS_S_USER") { CommandType = CommandType.StoredProcedure };
                command.Parameters.Add(new SqlParameter("@C_USER_LOGIN", username));
                var data = ExecuteCommand(command, new string[] { "C_USER_LOGIN", "C_NAME", "C_PASSWORD", "C_USER_GROUP_CODE", "C_CR_BY", "D_CR_DATE", "C_UPD_BY", "D_UPD_DATE", "C_PROG_ID" });
                if (!data.Any())
                    return null;
                var user = data.ToList()[0];
                if (password != user["C_PASSWORD"])
                    return null;

                return new UserInfo()
                {
                    C_USER_LOGIN = user["C_USER_LOGIN"],
                    C_NAME = user["C_NAME"],
                    C_PASSWORD = user["C_PASSWORD"],
                    C_USER_GROUP_CODE = user["C_USER_GROUP_CODE"],
                    C_CR_BY = user["C_CR_BY"],
                    D_CR_DATE = DateTime.Parse(user["D_CR_DATE"]),
                    C_UPD_BY = user["C_UPD_BY"],
                    D_UPD_DATE = DateTime.Parse(user["D_UPD_DATE"]),
                    C_PROG_ID = user["C_PROG_ID"]
                };
            }
        }
    }
}