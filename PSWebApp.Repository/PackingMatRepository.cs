﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using PSWebApp.Model;
namespace PSWebApp.Repository
{
    public class PackingMatRepository : PSWebRepository
    {
        string constr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        SqlConnection _connection;

        public PackingMatRepository()
        {
            _connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            _connection.Open();
        }
        public List<PackingMatInfo> GetPackingMat(int Crop)
        {
            var command = new SqlCommand("sp_PS_Get_PackingCostByCrop") { CommandType = CommandType.StoredProcedure };
            command.Parameters.Add(new SqlParameter("@crop", Crop));

            var data = ExecuteCommand(command, new string[] { "crop", "packingmat", "price" });
            List<PackingMatInfo> lstPackingInfo = new List<PackingMatInfo>();
            foreach (var p in data)
            {
                lstPackingInfo.Add(new PackingMatInfo()
                {
                    Crop = Int16.Parse(p["crop"]),
                    Packing_Mat = p["packingmat"],
                    Price = decimal.Parse(p["price"])
                });
            }
            return lstPackingInfo;
        }
        //------------------------ Submit  ---------------------
        public void SubmitPackingMat(int Crop, string Packing, decimal Price)
        {
            using (var cmd = new SqlCommand("sp_PS_Insert_PackingCost"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@crop", Crop);
                cmd.Parameters.AddWithValue("@packing", Packing);
                cmd.Parameters.AddWithValue("@price", Price);
                ExecuteCommand(cmd, new string[] { });
            }
        }
        //--------------------- Delete ----------------------
        public bool DeletePackingMat(int Crop, string Packing, decimal Price)
        {
            using (var cmd = new SqlCommand("sp_PS_Del_PackingCost", _connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@crop", Crop);
                cmd.Parameters.AddWithValue("@packing", Packing);
                cmd.Parameters.AddWithValue("@price", Price);

                SqlParameter count_result = cmd.Parameters.Add("@ExistedPacking", SqlDbType.Int);
                count_result.Direction = ParameterDirection.ReturnValue;

                cmd.ExecuteNonQ‌​uery();
                _connection.Close();

                if ((int)cmd.Parameters["@ExistedPacking"].Value > 0)
                    return true;
                return false;
            }
        }

    }
} 