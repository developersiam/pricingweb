﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using PSWebApp.Models;

namespace PSWebApp.Repository
{
    public class ProcessRepository : PSWebRepository
    {
        string constr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        public List<ProcessesInfo> GetProcess_M()
        {
            using (SqlConnection con = new SqlConnection(constr))
            {
                con.Open();
                var command = new SqlCommand("sp_SelAll_PS_PROCESS_READY_M")  { CommandType = CommandType.StoredProcedure };
                var data = ExecuteCommand(command, new string[] { "C_PROG_ID", "C_PROCESS_DOC_NO", "C_PROCESS_NO", "D_PROCESS_DOC_DATE", "N_CROP", "C_TYPE", "C_SEASON", "C_PACKED_GRADE", "C_PACK_MAT_CODE", "N_PACK_MAT_CHARGE", "C_TRAN_CHARGE_CODE", "N_TRAN_CHARGE", "C_REDRY_CHARGE_CODE", "N_REDRY_CHARGE", "N_CASE_WEIGHT", "N_PACKED_WEIGHT", "C_CR_BY", "D_CR_DATE", "C_UPD_BY", "D_UPD_DATE" });
                List<ProcessesInfo> lstProcessInfo = new List<ProcessesInfo>();
                foreach(var p in data)
                {
                    lstProcessInfo.Add(new ProcessesInfo()
                    {
                        C_PROG_ID = p["C_PROG_ID"],
                        C_PROCESS_DOC_NO = p["C_PROCESS_DOC_NO"],
                        C_PROCESS_NO = p["C_PROCESS_NO"],
                        D_PROCESS_DOC_DATE = DateTime.Parse(p["D_PROCESS_DOC_DATE"].ToString()).ToString("dd/MM/yyyy"),
                        N_CROP = p["N_CROP"],
                        C_TYPE = p["C_TYPE"],
                        C_SEASON = p["C_SEASON"],
                        C_PACKED_GRADE = p["C_PACKED_GRADE"],
                        C_PACK_MAT_CODE = p["C_PACK_MAT_CODE"],
                        N_PACK_MAT_CHARGE = p["N_PACK_MAT_CHARGE"],
                        C_TRAN_CHARGE_CODE = p["C_TRAN_CHARGE_CODE"],
                        N_TRAN_CHARGE = p["N_TRAN_CHARGE"],
                        C_REDRY_CHARGE_CODE = p["C_REDRY_CHARGE_CODE"],
                        N_REDRY_CHARGE = p["N_REDRY_CHARGE"],
                        N_CASE_WEIGHT = p["N_CASE_WEIGHT"],
                        N_PACKED_WEIGHT = p["N_PACKED_WEIGHT"],
                        C_CR_BY = p["C_CR_BY"],
                        D_CR_DATE = DateTime.Parse(p["D_CR_DATE"].ToString()).ToString("dd/MM/yyyy"),
                        C_UPD_BY = p["C_UPD_BY"],
                        D_UPD_DATE = DateTime.Parse(p["D_UPD_DATE"].ToString()).ToString("dd/MM/yyyy"),

                    });
                }
                return lstProcessInfo;
            }
        }

    }
}