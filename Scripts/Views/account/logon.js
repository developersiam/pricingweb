﻿$(window).load(function () {
    show_navbar();
    if ($('#model-error').length > 0) {
        $('.logon-container').find('span').addClass('has-error help-block');
    }
});

$('#btn-logon').click(function () {
    if (validate_logon()) {
        $('.form-signin').submit();
    }
});

function validate_logon() {
    var valid = true;
    $('#model-error').hide();
    $('#username-error').hide();
    $('#password-error').hide();
    if (!$('#Username').valid()) {
        $('#username-error').show();
        valid = false;
    }
    else if (!$('#Password').valid()) {
        $('#password-error').show();
        valid = false;
    }
    if (!valid) {
        $('.logon-container').find('span').addClass('has-error help-block');
    }
    else {
        $('.logon-container').find('span').removeClass('has-error help-block');
    }
    return valid;
}