﻿var table;
$('document').ready(function () {
    adjust_receiving_details_table();
    load_ProcessProduct_details_table();
});

$('ul[aria-labelledby="dropdown-type-rb"] li a').click(function () {
    var typeRB = $(this).text();
    var typeRB_V = $(this).attr('data-value');
    var btn_typeRB = $('#dropdown-type-rb');
    btn_typeRB.html(typeRB);
    btn_typeRB.val(typeRB);

    $('#shippingTypeRB').val(typeRB_V);

});

$('ul[aria-labelledby="dropdown-type"] li a').click(function () {
    var type = $(this).text();
    var type_V = $(this).attr('data-value');
    var btn_type = $('#dropdown-type');
    btn_type.html(type);
    btn_type.val(type);

    $('#shippingType').val(type_V);

});

function load_ProcessProduct_details_table() {
    var load_table_url = '/ShippingProduction/JsonShippingProductDetails';
    var param = {
        ShippingNo: $('#Shipping_No').val()
    }
    $.ajax({
        url: load_table_url,
        data: { param: param },
        type: "POST",
        success: function (data) {
            table = $('#shipping-table').bootstrapTable({
                serverParams: {
                    ShippingNo: $('#Shipping_No').val()
                }
            }).on('load-success.bs.table', function () {
                table.bootstrapTable('load', data.rows);
                adjust_table_by_size();
                adjust_receiving_details_table();
            });
            table.bootstrapTable('load', data.rows);
            table.bootstrapTable('refresh');
        },
        dataType: 'json'
    });

}

function adjust_receiving_details_table() {
    var height = $('.headerBar div').outerHeight(true) + $('.fixed-table-header').outerHeight(true) + $('.clearfix').height() + $('.form-group fieldset-row').height(true);
    height = $(window).height() - height - 130;
    $('.fixed-table-body').height(height);
}

function adjust_table_by_size() {
    var screen_width = $(window).width();
    if (screen_width < 768) {
        //extra small device
        adjust_table_column("xs");
    }
    else if (screen_width < 992) {
        //small device
        adjust_table_column("sm");
    }
    else if (screen_width < 1200) {
        //medium device
        adjust_table_column("md");
    }
}

function adjust_table_column(size) {
    var hide_column = [];
    if (size == "md") {
        hide_column = [];
    }
    else if (size == "sm") {
        hide_column = [];
    }
    else if (size == "xs") {
        hide_column = ["weight", "unit price"];
    }
    for (var i = 0; i < hide_column.length; i++) {
        table.bootstrapTable('hideColumn', hide_column[i]);
    }
}

$('#btn-delete-shipping-Product').click(function () {
    var DocNo = $('#Doc_No').val();
    var frm_delete = $('<form action="/ShippingProduction/DeleteShippingProduct" method="post"/>');
    $('body').append(frm_delete);
    frm_delete.append($('<input name="DocNo" type="hidden" value="' + DocNo + '" />'));
    frm_delete.submit();
});

$('#frm-shipping-product-details').submit(function (e) {
    $(this).validate().form();

    var is_valid = true;
    var error_count = $('[data-valmsg-for]:has(>span)').length;

    if (error_count > 0)
        is_valid = false;

    if (is_valid) {
        bootbox.dialog({
            message: '<h4 class="text-center"><i class="fa fa-refresh fa-spin fa-fw"></i></i>&nbsp;Submitting Information</h4>',
            closeButton: false
        });
    }
});