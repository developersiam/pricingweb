﻿var table;
$(window).load(function () {
    adjust_user_table();
});

$(document).ready(function () {
    table = $('#process-table').bootstrapTable({
        rowAttributes: {
            "data-id": function (rowItem) {
                return rowItem.id;
            }}
        });
    load_process();

    //$('#frm-generate-report')
    //    .insertBefore(".fixed-table-pagination")
    //    .each(function () {
    //        $("#btn-generate-pdf").click(function () {
    //            $("#report-from-date").val($('#input-from-date').val());
    //            $("#report-to-date").val($('#input-to-date').val());
    //            $("#frm-generate-report").submit();
    //        });
    //    });
});

$(window).resize(function () {
    adjust_user_table();
    adjust_table_by_size();
});

function load_process() {
    var load_process_url = '/Process/JsonProcesses';
    $.ajax({
        url: load_process_url,
        success: function (data) {
            table.bootstrapTable('load', data.rows);
            table.bootstrapTable('refresh');
        },
        dataType: 'json'
    });
}
function adjust_user_table() {
    var height = $(".headerBar div").height() + $("#navbar-user-map").height() + $('#optionfooter').height() + $('.fixed-table-header').height() + 240;
    height = $(window).height() - height - $(".navbar-fixed-bottom").height();
    $('.fixed-table-body').height(height);
}

function adjust_table_by_size() {
    var screen_width = $(window).width();
    if (screen_width < 768) {
        //extra small device
        adjust_table_column("xs");
    }
    else if (screen_width < 992) {
        //small device
        adjust_table_column("sm");
    }
    else if (screen_width < 1200) {
        //medium device
        adjust_table_column("md");
    }
}
function adjust_table_column(size) {
    var hide_column = [];
    if (size == "md") {
        hide_column = ["last_name", "organization", "expiry_date"];
    }
    else if (size == "sm") {
        hide_column = ["last_name", "organization", "expiry_date"];
    }
    else if (size == "xs") {
        hide_column = ["status", "last_name", "email", "email", "organization", "account_type", "expiry_date"];
    }
    for (var i = 0; i < hide_column.length; i++) {
        table.bootstrapTable('hideColumn', hide_column[i]);
    }
}