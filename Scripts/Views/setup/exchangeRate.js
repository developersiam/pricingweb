﻿var table;
var table_edit;
var change_action = "redry";
var crop;
var code;

$(document).ready(function () {
    var effective_date = $('#exchange-eff-date').datepicker({
        todayHighlight: true,
        startDate: '-2y',
        endDate: '0d',
        format: "dd/mm/yyyy"
    }).on('changeDate', function (ev) {
        $('#exchange-input-date').val($('#input-exchange-eff-date').val());
        effective_date.hide();
        load_exchange_rate()
    }).data('datepicker');

    table = $('#exchange-table').bootstrapTable({
        rowAttributes: {
            "data-id": function (rowItem) {
                return rowItem.effective_date;
            }
        },
        serverParams: {
            sEffDate: $('#exchange-input-date').val()
        }
    }).on('click-row.bs.table', function (e, row, $element) {
        $('#exchange-table > tbody > tr').removeClass('highlight');
        $element.addClass('highlight');
        $('#ExchangeRate').val($($element).attr('data-id'));
        $('#btn-submit-exchange-rate').removeClass('disabled');
    }).on('dbl-click-row.bs.table', function (e, row, $element) {
        $('#ExchangeRate').val($($element).attr('data-id'));
        $('#input-exchange-eff-date').val(row.effective_date);
        $('#txtexchange').val(row.exchange_rate);
        $('#btn-submit-exchange-rate').removeClass('disabled');
        $('#btn-delete-exchange-rate').removeClass('disabled');
    }).on('load-success.bs.table', function () {
        adjust_table_by_size();
        adjust_report_table
    });

    $('.pagination-detail').removeClass('hidden-xs');

    adjust_table_by_size();
    adjust_report_table();

})

function load_exchange_rate() {
    var load_user_url = '/ExchangeRate/JsonShowExchangeRate';
    var param = {
        sEffDate: $('#exchange-input-date').val()
    }
    $.ajax({
        url: load_user_url,
        data: { param: param },
        success: function (data) {
            table.bootstrapTable('load', data.rows);
            table.bootstrapTable('refresh');
        },
        dataType: 'json'
    });
}


function FindPackingMat(e) {
    if (e.keyCode == 13) {
        Initial();
        showPackingMat();
    }
}

function Initial() {
    $('#btn-submit-packingmat').addClass('disabled');
    $('#btn-delete-packingmat').addClass('disabled');
}


$(window).resize(function () {
    adjust_table_by_size();
    adjust_report_table
});


$('#btn-delete-exchange-rate').click(function () {
    var Date = $('#input-exchange-eff-date').val();
    var Rate = $('#txtexchange').val();
    var frm_delete = $('<form action="/ExchangeRate/DeleteExchangeRate" method="post"/>');
    $('body').append(frm_delete);
    frm_delete.append($('<input name="EffDate" type="hidden" value="' + Date + '" />'));
    frm_delete.append($('<input name="Rate" type="hidden" value="' + Rate + '" />'));
    frm_delete.submit();
});

$('#btn-submit-exchange-rate').click(function () {
    var Date = $('#input-exchange-eff-date').val();
    var Rate = $('#txtexchange').val();
    var frm_submit = $('<form action="/ExchangeRate/SubmitExchangeRate" method="post"/>');
    $('body').append(frm_submit);
    frm_submit.append($('<input name="EffDate" type="hidden" value="' + Date + '" />'));
    frm_submit.append($('<input name="Rate" type="hidden" value="' + Rate + '" />'));
    frm_submit.submit();
});


function adjust_table_by_size() {
    var screen_width = $(window).width();
    if (screen_width < 768) {
        //extra small device
        adjust_table_column("xs");
    }
    else if (screen_width < 992) {
        //small device
        adjust_table_column("sm");
    }
    else if (screen_width < 1200) {
        //medium device
        adjust_table_column("md");
    }
}

function adjust_table_column(size) {
    var hide_column = [];
    if (size == "md") {
        hide_column = ["C_CURER_CODE", "C_AREA", "C_REC_TYPE_RB"];
    }
    else if (size == "sm") {
        hide_column = ["C_CURER_CODE", "C_AREA", "C_REC_TYPE_RB"];
    }
    else if (size == "xs") {
        hide_column = ["C_COMPANY", "C_SEASON", "C_CURER_CODE", "C_CURER_NAME", "C_AREA", "C_REC_TYPE_RB"];
    }
    for (var i = 0; i < hide_column.length; i++) {
        table.bootstrapTable('hideColumn', hide_column[i]);
    }
}

function adjust_report_table() {
    var height = parseInt($('.hero').css('margin-top')) +
        $('.fixed-table-header').height() +
        $(".filterBox").outerHeight() +
        $('.clearfix').height() +
        $('.navbar-fixed-bottom').height();
    height = $(window).height() - height - 100;
    $('.fixed-table-body').height(height);
}

