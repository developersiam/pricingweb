﻿var table;
var table_edit;
var change_action = "redry";
var crop;
var code;

$(window).load(function () {
    table = $('#packing-table').bootstrapTable({
        rowAttributes: {
            "data-id": function (rowItem) {
                return rowItem.packing_mat;
            }
        },
    }).on('click-row.bs.table', function (e, row, $element) {
        $('#packing-table > tbody > tr').removeClass('highlight');
        $element.addClass('highlight');
        $('#Packing').val($($element).attr('data-id'));
        $('#btn-submit-packingmat').removeClass('disabled');
    }).on('dbl-click-row.bs.table', function (e, row, $element) {
        $('#Packing').val($($element).attr('data-id'));
        $('#txtPacking').val(row.packing_mat);
        $('#txtCost').val(row.price);
        $('#btn-submit-packingmat').removeClass('disabled');
        $('#btn-delete-packingmat').removeClass('disabled');
    }).on('load-success.bs.table', function () {
        adjust_table_by_size();
        adjust_report_table
    });

    $('.pagination-detail').removeClass('hidden-xs');

    adjust_table_by_size();
    adjust_report_table();
});


function FindPackingMat(e) {
    if (e.keyCode == 13) {
        Initial();
        showPackingMat();
    }
}

function Initial() {
    $('#btn-submit-packingmat').addClass('disabled');
    $('#btn-delete-packingmat').addClass('disabled');
}

function showPackingMat() {
    var load_table_url = '/PackingMat/JsonShowPackingMat';
    var param = {
        scrop: $('#txt-crop').val()
    }
    var option = table.bootstrapTable('getOptions');
    $.ajax({
        url: load_table_url,
        data: { param: param },
        success: function (data) {
            table = $('#packing-table').bootstrapTable({
            serverParams: {
                scrop: $('#txt-crop').val()
            }}).on('load-success.bs.table', function () {
                table.bootstrapTable('load', data.rows);
                adjust_table_by_size();
                adjust_report_table();
            });
            table.bootstrapTable('load', data.rows);
            table.bootstrapTable('refresh');
        },
        dataType: 'json'
    });
}

$(window).resize(function () {
    adjust_table_by_size();
    adjust_report_table
});


$('#btn-delete-packingmat').click(function () {
    var Crop = $('#txt-crop').val();
    var Packing = $('#txtPacking').val();
    var Price = $('#txtCost').val();
    var frm_delete = $('<form action="/PackingMat/DeletePackingPrice" method="post"/>');
    $('body').append(frm_delete);
    frm_delete.append($('<input name="Packing" type="hidden" value="' + Packing + '" />'));
    frm_delete.append($('<input name="Crop" type="hidden" value="' + Crop + '" />'));
    frm_delete.append($('<input name="Price" type="hidden" value="' + Price + '" />'));
    frm_delete.submit();
});

$('#btn-submit-packingmat').click(function () {
    var Crop = $('#txt-crop').val();
    var Packing = $('#txtPacking').val();
    var Price = $('#txtCost').val();
    var frm_submit = $('<form action="/PackingMat/SubmitPackingPrice" method="post"/>');
    $('body').append(frm_submit);
    frm_submit.append($('<input name="Packing" type="hidden" value="' + Packing + '" />'));
    frm_submit.append($('<input name="Crop" type="hidden" value="' + Crop + '" />'));
    frm_submit.append($('<input name="Price" type="hidden" value="' + Price + '" />'));
    frm_submit.submit();
});


$('#redryC-modal').on('hidden.bs.modal', function (e) {
    $('#frm-setup-redryC')[0].reset();
})
$('#btn-redryC-modal-close').click(function () {
    $('#redryC-modal').hide();
})
$('#btn-redryC-modal-close2').click(function () {
    $('#redryC-modal').hide();
})
$('#txtCode').click(function () {
    $('#btn-submit-redrycharge').removeClass('disabled');
})


function adjust_table_by_size() {
    var screen_width = $(window).width();
    if (screen_width < 768) {
        //extra small device
        adjust_table_column("xs");
    }
    else if (screen_width < 992) {
        //small device
        adjust_table_column("sm");
    }
    else if (screen_width < 1200) {
        //medium device
        adjust_table_column("md");
    }
}

function adjust_table_column(size) {
    var hide_column = [];
    if (size == "md") {
        hide_column = ["C_CURER_CODE", "C_AREA", "C_REC_TYPE_RB"];
    }
    else if (size == "sm") {
        hide_column = ["C_CURER_CODE", "C_AREA", "C_REC_TYPE_RB"];
    }
    else if (size == "xs") {
        hide_column = ["C_COMPANY", "C_SEASON", "C_CURER_CODE", "C_CURER_NAME", "C_AREA", "C_REC_TYPE_RB"];
    }
    for (var i = 0; i < hide_column.length; i++) {
        table.bootstrapTable('hideColumn', hide_column[i]);
    }
}

function adjust_report_table() {
    var height = parseInt($('.hero').css('margin-top')) +
        $('.fixed-table-header').height() +
        $(".filterBox").outerHeight() +
        $('.clearfix').height() +
        $('.navbar-fixed-bottom').height();
    height = $(window).height() - height - 100;
    $('.fixed-table-body').height(height);
}

