﻿var table;
$(document).ready(function () {
    $(".dropdown-menu li a").click(function () {
        var selText = $(this).text();
        $(this).parents('.btn-group').find('.dropdown-toggle').html(selText + ' <span class="caret" data-item="' + $(this).data("item") + '" ></span>');
    });
});

$(window).resize(function () {
    adjust_table_by_size();
    adjust_report_table
});

$(window).load(function () {
    table = $('#GreenM-table').bootstrapTable({
        rowAttributes: {
            "data-id": function (rowItem) {
                return rowItem.trans_no;
            }
        },
    }).on('click-row.bs.table', function (e, row, $element) {
        $('#GreenM-table > tbody > tr').removeClass('highlight');
        $element.addClass('highlight');
        $('#tfNo').val($($element).attr('data-id'));
        $('#btn-details-greentransfer').removeClass('disabled');
        $('#btn-new-greentransfer').removeClass('disabled');
    }).on('dbl-click-row.bs.table', function (e, row, $element) {
        $('#tfNo').val($($element).attr('data-id'));
        $('#frm-greenTrans-details').submit();
    }).on('load-success.bs.table', function () {
        adjust_table_by_size();
        adjust_report_table
    });

    $('.pagination-detail').removeClass('hidden-xs');

    //$('#dropdown-crop').change(function () {
    //    show_GreenTransfertable();
    //    $('#btn-manage-greentransfer').addClass('disabled');
    //})

    adjust_table_by_size();
    adjust_report_table();
});

$('#dropdown-crop').change(function () {
    //$('#btn-details-greentransfer').removeClass('disabled');
    $('#btn-new-greentransfer').removeClass('disabled');

    show_GreenTransfertable();
});


$('#btn-new-greentransfer').click(function () {
    show_newGreenTransfertable();
})

function show_GreenTransfertable() {
    var load_table_url = '/GreenTransfer/JsonGreenTMaster';
    var param = {
        scrop: $('#dropdown-crop').val()
    }
    var option = table.bootstrapTable('getOptions');
    $.ajax({
        url: load_table_url,
        data: { param: param , limit: option.pageSize},
        success: function (data) {
            table.bootstrapTable('load', data.rows);
            table.bootstrapTable('refresh');
        },
        dataType: 'json'
    });
}

function show_newGreenTransfertable() {
    var load_table_url = '/GreenTransfer/JsonAddingGreenTMaster';
    var param = {
        scrop: $('#dropdown-crop').val()
    }
    var option = table.bootstrapTable('getOptions');
    $.ajax({
        url: load_table_url,
        data: { param: param },
        success: function (data) {
            table = $('#GreenM-table').bootstrapTable({
            }).on('load-success.bs.table', function () {
                table.bootstrapTable('load', data.rows);
                adjust_table_by_size();
                adjust_report_table();
            });
            table.bootstrapTable('load', data.rows);
            table.bootstrapTable('refresh');
        },
        dataType: 'json'
    });
}


function adjust_table_by_size() {
    var screen_width = $(window).width();
    if (screen_width < 768) {
        //extra small device
        adjust_table_column("xs");
    }
    else if (screen_width < 992) {
        //small device
        adjust_table_column("sm");
    }
    else if (screen_width < 1200) {
        //medium device
        adjust_table_column("md");
    }
}

function adjust_table_column(size) {
    var hide_column = [];
    if (size == "md") {
        hide_column = ["C_CURER_CODE", "C_AREA", "C_REC_TYPE_RB"];
    }
    else if (size == "sm") {
        hide_column = ["C_CURER_CODE", "C_AREA", "C_REC_TYPE_RB"];
    }
    else if (size == "xs") {
        hide_column = ["C_COMPANY", "C_SEASON", "C_CURER_CODE", "C_CURER_NAME", "C_AREA", "C_REC_TYPE_RB"];
    }
    for (var i = 0; i < hide_column.length; i++) {
        table.bootstrapTable('hideColumn', hide_column[i]);
    }
}

function adjust_report_table() {
    var height = parseInt($('.hero').css('margin-top')) +
        $('.fixed-table-header').height() +
        $(".filterBox").outerHeight() +
        $('.clearfix').height() +
        $('.navbar-fixed-bottom').height();
    height = $(window).height() - height - 100;
    $('.fixed-table-body').height(height);
}

