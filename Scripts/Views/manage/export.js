﻿var table;
var table_edit;
var change_action = "redry";
var crop;
var packed;

$(window).load(function () {
    table = $('#export-table').bootstrapTable({
        rowAttributes: {
            "data-id": function (rowItem) {
                return rowItem.packed_grade;
            }
        },
    }).on('click-row.bs.table', function (e, row, $element) {
        $('#export-table > tbody > tr').removeClass('highlight');
        $element.addClass('highlight');
        $('#Code').val($($element).attr('data-id'));
    }).on('dbl-click-row.bs.table', function (e, row, $element) {
        $('#Code').val($($element).attr('data-id'));
        $('#txtCode').val($($element).attr('data-id'));
        $('#txtDescription').val(row.redry_trans_charge_desc);
        $('#txtUnit').val(row.charge);
        $('#btn-submit-export').removeClass('disabled');
        $('#btn-delete-export').removeClass('disabled');
    }).on('load-success.bs.table', function () {
        adjust_table_by_size();
        adjust_report_table
    });

    $('.pagination-detail').removeClass('hidden-xs');

    adjust_table_by_size();
    adjust_report_table();
});



function Initial() {
    //$(window).location.reload(true);
    $('#btn-submit-export').addClass('disabled');
    $('#btn-delete-export').addClass('disabled');
    //$('#txtCode').val() = "";
    //$('#txtDescription').val() = "";
    //$('#txtUnit').val() = "";
}

$('#dropdown-packed').change(function () {
    $('#dropdown-customer option:first').attr('selected', 'selected');
    $('#rate-content').empty();
    $('#dropdown-customer').find('option[data-packed]').hide()
    $('#dropdown-customer').find('option[data-packed="' + $(this).val() + '"]').show();
});

$('#dropdown-customer').change(function () {
    $('#dropdown-customer-packed option:first').attr('selected', 'selected');
    $('#dropdown-customer-packed').find('option[data-customer]').hide()
    $('#dropdown-customer-packed').find('option[data-packed]').hide()
    $('#dropdown-customer-packed').find('option[data-packed="' + $('#dropdown-packed').val() + '"]').show();
    //$('#dropdown-customer-packed').find('option[data-customer="' + $(this).val() + '"]').show();
});

$('#dropdown-customer-packed').change(function () {
    var customerPacked = $(this).val();
    //Calculate Weight
    //showUnitandWeight(customerPacked);
})



function showUnitandWeight(customerPacked) {
    var param = {
        crop: $('#txt-crop').val(),
        packed: $('#dropdown-packed').val(),
        customer: $('#dropdown-customer').val(),
        customerPacked: customerPacked
    }
    $.ajax({
        type: "POST",
        url: '/ExportValue/JsonUnitAndWeight',
        data: param,
        success: function (data) {
            $('#rate-content').html(data);
            //table = $('#deduction-change-table').bootstrapTable({
            //    serverParams: {
            //        sourceGuid: source
            //    }
            //}).on('load-success.bs.table', function () {
            //    adjust_table_by_size();
            //});

            //$('input[id$="rate"]').each(function () {
            //    deduction_rates[$(this).attr('id')] = parseFloat($(this).val());
            //});

            //$('#unitPrice').val() = parseFloat($(this).val())

            //$('button[data-id="btn-save-rate"]').click(function () {
            //    var rate_input = $(this).data('input');
            //    var rate = $('#' + rate_input).val();
            //    var $div = $(this).parent().parent().parent();

            //    if (is_number(rate)) {
            //        if (parseFloat(rate) != deduction_rates[rate_input]) {
            //            var deduction_type = $(this).data('rate-of');
            //            var action_url = '/Credit/DeductionRate';
            //            $.ajax({
            //                url: action_url,
            //                type: "POST",
            //                cache: false,
            //                data: {
            //                    deductionType: deduction_type,
            //                    ratePrice: rate,
            //                    sourceGuid: source
            //                },
            //                success: function (data) {
            //                    if (data.success) {
            //                        $('#deduction-change-table').bootstrapTable('refresh');
            //                        deduction_rates[rate_input] = parseFloat(rate);
            //                        $('<span class="text-right text-alert">Save Complete</span>').appendTo($div).delay(2000).fadeOut(300, function () {
            //                            $(this).remove()
            //                        });
            //                    }
            //                }
            //            });
            //        }
            //    }
            //});
        }
    });
}

$(window).resize(function () {
    adjust_table_by_size();
    adjust_report_table
});

$('#btn-submit-export').click(function () {
    var Crop = $('#txt-crop').val();
    var Packed = $('#dropdown-packed').val();
    var Customer = $('#dropdown-customer').val();
    var Cust_Packed = $('#dropdown-customer-packed').val();
    var value = $('#txtvalue').val();
    var frm_submit = $('<form action="/ExportValue/SubmitExportValue" method="post"/>');
    $('body').append(frm_submit);
    frm_submit.append($('<input name="Crop" type="hidden" value="' + Crop + '" />'));
    frm_submit.append($('<input name="Customer" type="hidden" value="' + Customer + '" />'));
    frm_submit.append($('<input name="PackedGrade" type="hidden" value="' + Packed + '" />'));
    frm_submit.append($('<input name="CustomerPacked" type="hidden" value="' + Cust_Packed + '" />'));
    frm_submit.append($('<input name="ExportValue" type="hidden" value="' + value + '" />'));
    frm_submit.submit();
});

$('#txtvalue').click(function () {
    $('#btn-submit-export').removeClass('disabled');
})


function adjust_table_by_size() {
    var screen_width = $(window).width();
    if (screen_width < 768) {
        //extra small device
        adjust_table_column("xs");
    }
    else if (screen_width < 992) {
        //small device
        adjust_table_column("sm");
    }
    else if (screen_width < 1200) {
        //medium device
        adjust_table_column("md");
    }
}


function adjust_report_table() {
    var height = parseInt($('.hero').css('margin-top')) +
        $('.fixed-table-header').height() +
        $(".filterBox").outerHeight() +
        $('.clearfix').height() +
        $('.navbar-fixed-bottom').height();
    height = $(window).height() - height - 90;
    $('.fixed-table-body').height(height);

    height = $('.hero').outerHeight(true) - ($('.filterBox').outerHeight(true) + $('.navbar-fixed-bottom').outerHeight(true) + $('.nav-tabs-deduction').outerHeight(true)+ 300);
    $('.content-number').height(height);
}

