﻿$(document).ready(function () {
    $('[data-toggle=collapse-side]').each(function () {
        var sel = $(this).attr('data-target');
        var sel2 = $(this).attr('data-target-2');
        $(this).click(function (event) {
            var side_container = $('#side-container');
            var side_footer_container = $('.navbar-fixed-bottom');
            side_container.attr('class', 'side-collapse-container');
            side_footer_container.attr('class', 'navbar-fixed-bottom');
            if ($(this).attr('data-target') == ".side-collapse.left") {
                $('.side-collapse.left').toggleClass('in');
                if (!$('.side-collapse.left').hasClass('in')) {
                    side_container.attr('class', 'side-collapse-container left');
                    side_footer_container.attr('class', 'navbar-fixed-bottom left');
                }
                else {
                    side_container.attr('class', 'side-collapse-container');
                    side_footer_container.attr('class', 'navbar-fixed-bottom');
                }
                if (!$('.side-collapse.right').hasClass('in')) {
                    $('.side-collapse.right').toggleClass('in');
                }
                
            }
            else {
                side_container.attr('class', 'side-collapse-container right');
                side_footer_container.attr('class', 'navbar-fixed-bottom right');
                $('.side-collapse.right').toggleClass('in');
                if (!$('.side-collapse.right').hasClass('in')) {
                    side_container.attr('class', 'side-collapse-container right');
                    side_footer_container.attr('class', 'navbar-fixed-bottom right');
                }
                else {
                    side_container.attr('class', 'side-collapse-container');
                    side_footer_container.attr('class', 'navbar-fixed-bottom');
                }
                if (!$('.side-collapse.left').hasClass('in')) {
                    $('.side-collapse.left').toggleClass('in');
                }
            }
            side_container.toggleClass('out');
            side_footer_container.toggleClass('out');
        });
    });
});